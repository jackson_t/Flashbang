#include <ntddk.h>
#include <bcrypt.h>
#include <ntstrsafe.h>

#include "Common.h"
#include "Utility.h"

void GenerateDeviceName(OUT PUNICODE_STRING DeviceName)
{
	// Generate random GUID for device name.
	GUID DeviceNameGuid{};
	UNICODE_STRING GuidString;
	BCryptGenRandom(NULL, (PUCHAR)&DeviceNameGuid, sizeof(GUID), BCRYPT_USE_SYSTEM_PREFERRED_RNG);
	RtlStringFromGUID(DeviceNameGuid, &GuidString);

	// Concatenate \Device\ + GUID string.
	PWSTR DeviceNameBuffer = (PWSTR)ExAllocatePoolWithTag(PagedPool, BUFSIZ, DRIVER_TAG);
	RtlZeroMemory(DeviceNameBuffer, BUFSIZ);
	RtlStringCbCatW(DeviceNameBuffer, BUFSIZ, LR"(\Device\)");
	RtlStringCbCatW(DeviceNameBuffer, BUFSIZ, GuidString.Buffer);

	// Initialize UNICODE_STRING, then free PWSTR buffer.
	RtlUnicodeStringInit(DeviceName, DeviceNameBuffer);
}