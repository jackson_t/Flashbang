#include <ntifs.h>
#include <ntddk.h>
#include <intrin.h>
#include <aux_klib.h>

#include "Memory.h"
#include "Common.h"

#pragma intrinsic(__readmsr)

NTSTATUS MemorySearch(PCUCHAR StartAddress, PCUCHAR EndAddress, PCUCHAR PatternBuffer, SIZE_T PatternLength, PUCHAR* FoundAddress)
{
	*FoundAddress = (PUCHAR)StartAddress;

	while (*FoundAddress < EndAddress)
	{
		if ((ULONG64)*FoundAddress % 0x10000 == 0)
		{
			KdPrint(("> FlashbangDriver: Searching from address 0x%p...\n", *FoundAddress));
		}

		// Using MmIsAddressValid is usually a bad practice.
		// See: https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/ntddk/nf-ntddk-mmisaddressvalid.
		if (MmIsAddressValid(*FoundAddress))
		{
			if (RtlEqualMemory(PatternBuffer, *FoundAddress, PatternLength))
			{
				return STATUS_SUCCESS;
			}
		}
		else
		{
			KdPrint(("> FlashbangDriver: Invalid address found at 0x%p...\n", *FoundAddress));
			return STATUS_NOT_FOUND;
		}

		*FoundAddress += 1;
	}

	return STATUS_NOT_FOUND;
}

NTSTATUS SetWriteProtect(ULONG Enable)
{
	for (ULONG i = 0; i < KeQueryActiveProcessorCount(0); i++)
	{
		KAFFINITY OldAffinity = KeSetSystemAffinityThreadEx((KAFFINITY)(1i64 << i));

		CR0 cr0;
		cr0.flags = __readcr0();
		cr0.write_protect = (Enable) ? 1 : 0;
		__writecr0(cr0.flags);

		KeRevertToUserAffinityThreadEx(OldAffinity);
	}

	return STATUS_SUCCESS;
}

NTSTATUS WriteVirtualMemory(PVOID Destination, PVOID Source, SIZE_T Length)
{
	PHYSICAL_ADDRESS PhysicalAddress = MmGetPhysicalAddress(Destination);
	PVOID MappedDestination = MmMapIoSpace(PhysicalAddress, Length, MmNonCached);
	RtlCopyMemory(MappedDestination, Source, Length);
	MmUnmapIoSpace(MappedDestination, Length);

	return STATUS_SUCCESS;
}

NTSTATUS PatchModuleMemory(PBUFFER_INFO ModuleBufferInfo)
{
	NTSTATUS Status;

	// Initialise AuxKlib.
	Status = AuxKlibInitialize();
	if (!NT_SUCCESS(Status))
	{
		return Status;
	}

	// Get an array of modules, first by determining the size of the array.
	ULONG BufferSize = 0;
	Status = AuxKlibQueryModuleInformation(&BufferSize, sizeof(AUX_MODULE_EXTENDED_INFO), nullptr);
	if (!NT_SUCCESS(Status) || BufferSize == 0)
	{
		return Status;
	}

	// Calculate the number of modules, and allocate a buffer to receive data.
	ULONG ModuleCount = BufferSize / sizeof(AUX_MODULE_EXTENDED_INFO);
	PAUX_MODULE_EXTENDED_INFO Modules = (PAUX_MODULE_EXTENDED_INFO)ExAllocatePoolWithTag(PagedPool, BufferSize, DRIVER_TAG);
	if (Modules == nullptr)
	{
		return STATUS_INSUFFICIENT_RESOURCES;
	}
	else
	{
		RtlZeroMemory(Modules, BufferSize);
	}

	// Obtain the modules array.
	Status = AuxKlibQueryModuleInformation(&BufferSize, sizeof(AUX_MODULE_EXTENDED_INFO), Modules);
	if (!NT_SUCCESS(Status) || BufferSize == 0)
	{
		return Status;
	}

	// Get address if already specified, otherwise use the provided module search pattern.
	ULONG i = 0;
	PUCHAR TargetAddress = nullptr;
	if (*ModuleBufferInfo->TargetAddress)
	{
		// Loop through modules array to see if the provided address belongs to a module.
		for (i = 0; i < ModuleCount; i++)
		{
			ULONG64 StartAddress = (ULONG64)Modules[i].BasicInfo.ImageBase;
			ULONG64 EndAddress = StartAddress + Modules[i].ImageSize;
			
			if ((StartAddress <= *ModuleBufferInfo->TargetAddress) && (*ModuleBufferInfo->TargetAddress <= EndAddress))
			{
				TargetAddress = (PUCHAR)*ModuleBufferInfo->TargetAddress;
				KdPrint(("> FlashbangDriver: Using provided address 0x%p at %s.\n", TargetAddress, Modules[i].FullPathName + Modules[i].FileNameOffset));
				break;
			}
		}
		
		if (!TargetAddress)
		{
			KdPrint(("> FlashbangDriver: The provided address (0x%p) was not found.\n", *ModuleBufferInfo->TargetAddress));
			return STATUS_NOT_FOUND;
		}
	}
	else
	{
		// Loop through array to find the start address of the target module.
		BOOLEAN IsModuleMatched = FALSE;
		for (i = 0; i < ModuleCount; i++)
		{
			STRING TargetModule{};
			STRING CurrentModule{};
			RtlInitString(&TargetModule, (PCSZ)ModuleBufferInfo->ModuleName);
			RtlInitString(&CurrentModule, (PCSZ)(Modules[i].FullPathName + Modules[i].FileNameOffset));

			if (RtlCompareString(&TargetModule, &CurrentModule, TRUE) == 0)
			{
				KdPrint(("> FlashbangDriver: Matched module %s.\n", TargetModule.Buffer));
				IsModuleMatched = TRUE;
				break;
			}
		}

		// Fail if module was not found.
		if (!IsModuleMatched)
		{
			KdPrint(("> FlashbangDriver: Module %s not found.\n", ModuleBufferInfo->ModuleName));
			return STATUS_NOT_FOUND;
		}

		// Search memory haystack for specified needle.
		PCUCHAR StartAddress = (PCUCHAR)Modules[i].BasicInfo.ImageBase;
		PCUCHAR EndAddress = (PCUCHAR)((ULONG64)Modules[i].BasicInfo.ImageBase + Modules[i].ImageSize);
		KdPrint(("> FlashbangDriver: Searching between [0x%p, 0x%p].\n", StartAddress, EndAddress));
		Status = MemorySearch(StartAddress, EndAddress, ModuleBufferInfo->SearchPattern, ModuleBufferInfo->SearchPatternLength, &TargetAddress);
		if (!NT_SUCCESS(Status)) return Status;

		// If needle is found, apply offset.
		TargetAddress += ModuleBufferInfo->Offset;
		*ModuleBufferInfo->TargetAddress = (ULONG64)TargetAddress;
		KdPrint(("> FlashbangDriver: Matched address at 0x%p.\n", TargetAddress));
	}
	
	// Check if the old (expected) bytes match.
	if (RtlCompareMemory(TargetAddress, ModuleBufferInfo->OldBytes, ModuleBufferInfo->OldBytesLength) != ModuleBufferInfo->OldBytesLength)
	{
		return STATUS_NOT_FOUND;
	}

	// Print old and new bytes.
	KdPrint(("> FlashbangDriver: Old bytes:"));
	for (ULONG j = 0; j < ModuleBufferInfo->OldBytesLength; j++) KdPrint((" %02X", ModuleBufferInfo->OldBytes[j]));
	KdPrint((".\n"));
	KdPrint(("> FlashbangDriver: New bytes:"));
	for (ULONG j = 0; j < ModuleBufferInfo->NewBytesLength; j++) KdPrint((" %02X", ModuleBufferInfo->NewBytes[j]));
	KdPrint((".\n"));

	// Apply patch, since they match at this point.
	KdPrint(("> FlashbangDriver: Old bytes match; patching...\n"));
	WriteVirtualMemory(TargetAddress, ModuleBufferInfo->NewBytes, ModuleBufferInfo->NewBytesLength);

	return STATUS_SUCCESS;
}