#include <ntifs.h>
#include <ntddk.h>
#include <bcrypt.h>
#include <ntstrsafe.h>

#include "FlashbangDriver.h"
#include "Common.h"
#include "Utility.h"
#include "Memory.h"
#include "Modules.h"
#include "Callbacks.h"

UNICODE_STRING DeviceName{};
PVOID CallbackRegistration{};

extern "C" NTSTATUS DriverEntry(_In_ PDRIVER_OBJECT DriverObject, _In_ PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_SUCCESS;

	DriverObject->DriverUnload = UnloadDriver;
	DriverObject->MajorFunction[IRP_MJ_CREATE] = DispatchCreateClose;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = DispatchCreateClose;
	DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DispatchDeviceControl;

	// Delete driver file so security products cannot read it.
	DeleteDriverFile(RegistryPath);

	// Create device.
	PDEVICE_OBJECT DeviceObject;
	GenerateDeviceName(&DeviceName);
	KdPrint(("> FlashbangDriver: %ls.\n", DeviceName.Buffer));
	Status = IoCreateDevice(DriverObject, 0, &DeviceName, FILE_DEVICE_UNKNOWN, 0, FALSE, &DeviceObject);
	if (NT_SUCCESS(Status))
	{
		DeviceObject->Flags |= DO_BUFFERED_IO;
	}
	else
	{
		KdPrint(("> FlashbangDriver: Failed to create device (0x%08X).\n", Status));
		return Status;
	}

	// Call IoRegisterContainerNotification to register handler which will return unique device name.
	IO_SESSION_STATE_NOTIFICATION NotificationInformation = { 0 };
	NotificationInformation.Size = sizeof(IO_SESSION_STATE_NOTIFICATION);
	NotificationInformation.Flags = 0;
	NotificationInformation.IoObject = DriverObject;
	NotificationInformation.EventMask = IO_SESSION_STATE_CREATION_EVENT;
	NotificationInformation.Context = 0;
	Status = IoRegisterContainerNotification(IoSessionStateNotification, (PIO_CONTAINER_NOTIFICATION_FUNCTION)IoSessionNotificationFunction, &NotificationInformation, sizeof(NotificationInformation), &CallbackRegistration);

	KdPrint(("> FlashbangDriver: Compiled on %s %s.\n", __DATE__, __TIME__));
	KdPrint(("> FlashbangDriver: Initialized successfully.\n"));
	return Status;
}

void UnloadDriver(_In_ PDRIVER_OBJECT DriverObject)
{
	// Unregister container notification.
	IoUnregisterContainerNotification(CallbackRegistration);

	// Delete device.
	IoDeleteDevice(DriverObject->DeviceObject);

	// Free resources.
	ExFreePoolWithTag(DeviceName.Buffer, DRIVER_TAG);

	KdPrint(("> FlashbangDriver: Unloaded successfully.\n"));
}

_Use_decl_annotations_
NTSTATUS DispatchCreateClose(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS DeleteDriverFile(PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_SUCCESS;
	OBJECT_ATTRIBUTES ObjectAttributes{};
	InitializeObjectAttributes(&ObjectAttributes, RegistryPath, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);

	HANDLE RegistryKey{};
	Status = ZwOpenKey(&RegistryKey, KEY_QUERY_VALUE, &ObjectAttributes);
	if (!NT_SUCCESS(Status))
	{
		KdPrint(("> FlashbangDriver: Registry key open failed (0x%08X).\n", Status));
		return Status;
	}

	UNICODE_STRING ValueName = RTL_CONSTANT_STRING(L"ImagePath");
	ULONG RequiredSize{};
	Status = ZwQueryValueKey(RegistryKey, &ValueName, KeyValueFullInformation, nullptr, 0, &RequiredSize);

	if ((Status == STATUS_BUFFER_TOO_SMALL) || (Status == STATUS_BUFFER_OVERFLOW))
	{
		PKEY_VALUE_FULL_INFORMATION KeyInfo = (PKEY_VALUE_FULL_INFORMATION)ExAllocatePoolWithTag(NonPagedPool, RequiredSize, DRIVER_TAG);
		
		if (KeyInfo)
		{
			RtlZeroMemory(KeyInfo, RequiredSize);
			Status = ZwQueryValueKey(RegistryKey, &ValueName, KeyValueFullInformation, KeyInfo, RequiredSize, &RequiredSize);

			UNICODE_STRING ImagePath{};
			RtlUnicodeStringInit(&ImagePath, (PCWSTR)((ULONG64)KeyInfo + KeyInfo->DataOffset));
			KdPrint(("> FlashbangDriver: ImagePath: %ls.\n", ImagePath.Buffer));
			OBJECT_ATTRIBUTES ImageAttributes{};
			InitializeObjectAttributes(&ImageAttributes, &ImagePath, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);
			Status = ZwDeleteFile(&ImageAttributes);
			KdPrint(("> FlashbangDriver: Deletion result: 0x%08X.\n", Status));
		}
	}

	return Status;
}

NTSTATUS IoSessionNotificationFunction(PVOID SessionObject,	PVOID IoObject,	ULONG Event, PVOID Context, PVOID NotificationPayload, ULONG PayloadLength)
{
	UNREFERENCED_PARAMETER(SessionObject);
	UNREFERENCED_PARAMETER(IoObject);
	UNREFERENCED_PARAMETER(Event);
	UNREFERENCED_PARAMETER(Context);
	UNREFERENCED_PARAMETER(PayloadLength);

	KdPrint(("> FlashbangDriver: IoSessionNotificationFunction has been called!\n"));
	PDEVICE_NAME_REQUEST Request = (PDEVICE_NAME_REQUEST)NotificationPayload;

	// ToDo: add ProbeForRead and ProbeForWrite calls.

	if (Request->Magic == DRIVER_TAG)
	{
		KdPrint(("> FlashbangDriver: Correct magic sent!\n"));

		if (*Request->DeviceNameLength > 0)
		{
			KdPrint(("> FlashbangDriver: Length provided; copying buffer...\n"));
			RtlCopyMemory(Request->DeviceName, DeviceName.Buffer, (SIZE_T)*Request->DeviceNameLength);
			KdPrint(("> FlashbangDriver: Request->DeviceName: %ls (%d)\n", Request->DeviceName, *Request->DeviceNameLength));

			// Unregister this routine after this function successfully returns, so that subsequent requests cannot be made.
			HANDLE ThreadHandle;
			PsCreateSystemThread(&ThreadHandle, THREAD_ALL_ACCESS, nullptr, nullptr, nullptr, (PKSTART_ROUTINE)IoUnregisterContainerNotificationWithDelay, nullptr);
			ZwClose(ThreadHandle);
		}
		else
		{
			KdPrint(("> FlashbangDriver: Length NOT provided; setting length (%d)...\n", DeviceName.MaximumLength));
			*Request->DeviceNameLength = DeviceName.MaximumLength;
		}
	}

	return STATUS_SUCCESS;
}

void IoUnregisterContainerNotificationWithDelay()
{
	LARGE_INTEGER Interval{};
	Interval.QuadPart = -10'000'000LL; // Wait 1 second.
	KeDelayExecutionThread(KernelMode, FALSE, &Interval);

	// Unregister container notification.
	KdPrint(("> FlashbangDriver: Attempting IoUnregisterContainerNotification...\n"));
	IoUnregisterContainerNotification(CallbackRegistration);
}

_Use_decl_annotations_
NTSTATUS DispatchDeviceControl(PDEVICE_OBJECT, PIRP Irp)
{
	NTSTATUS Status = STATUS_SUCCESS;
	ULONG OutBufferSize = 0;

	PIO_STACK_LOCATION Stack = IoGetCurrentIrpStackLocation(Irp);

	switch ((int)Stack->Parameters.DeviceIoControl.IoControlCode)
	{
	case IOCTL_SANDBOX:
	{
		KdPrint(("> FlashbangDriver: Hello, world!\n"));
		break;
	}
	case IOCTL_GET_MODULES:
	{
		KdPrint(("> FlashbangDriver: Getting modules...\n"));
		OutBufferSize = Stack->Parameters.DeviceIoControl.InputBufferLength;
		MODULE_INFO* Modules = (MODULE_INFO*)Irp->AssociatedIrp.SystemBuffer;
		Status = GetModules(Modules);
		break;
	}
	case IOCTL_GET_CALLBACKS:
	{
		ULONG i = 0;
		ULONG n = 0;
		CALLBACK_INFO* Callbacks = (CALLBACK_INFO*)Irp->AssociatedIrp.SystemBuffer;
		OutBufferSize = Stack->Parameters.DeviceIoControl.InputBufferLength;

		// Get load image notification callbacks and update the output index.
		KdPrint(("> FlashbangDriver: Getting load image notification callbacks...\n"));
		if (NT_SUCCESS(GetLoadImageNotifyCallbacks(&Callbacks[i], &n)))
			i += n;

		// Get create process notification callbacks and update the output index.
		KdPrint(("> FlashbangDriver: Getting create process notification callbacks...\n"));
		if (NT_SUCCESS(GetCreateProcessNotifyCallbacks(&Callbacks[i], &n)))
			i += n;

		// Get create thread notification callbacks and update the output index.
		KdPrint(("> FlashbangDriver: Getting create thread notification callbacks...\n"));
		if (NT_SUCCESS(GetCreateThreadNotifyCallbacks(&Callbacks[i], &n)))
			i += n;

		// Get registry callbacks and and update the output index.
		KdPrint(("> FlashbangDriver: Getting registry notification callbacks...\n"));
		if (NT_SUCCESS(GetRegistryCallbacks(&Callbacks[i], &n)))
			i += n;

		// Get object callbacks and and update the output index.
		KdPrint(("> FlashbangDriver: Getting object notification callbacks...\n"));
		if (NT_SUCCESS(GetObjectCallbacks(&Callbacks[i], &n)))
			i += n;

		// Get minifilter callbacks and and update the output index.
		KdPrint(("> FlashbangDriver: Getting minifilter notification callbacks...\n"));
		if (NT_SUCCESS(GetMinifilterCallbacks(&Callbacks[i], &n)))
			i += n;

		break;
	}
	case IOCTL_GET_QWORD:
	{
		OutBufferSize = Stack->Parameters.DeviceIoControl.InputBufferLength;
		QWORD_INFO* Buffer = (QWORD_INFO*)Irp->AssociatedIrp.SystemBuffer;
		KdPrint(("> FlashbangDriver: Getting QWORD at 0x%p...\n", Buffer->Address));
		Buffer->Value = *Buffer->Address;
		break;
	}
	case IOCTL_SET_QWORD:
	{
		QWORD_INFO* Buffer = (QWORD_INFO*)Irp->AssociatedIrp.SystemBuffer;
		KdPrint(("> FlashbangDriver: Setting QWORD at 0x%p...\n", Buffer->Address));
		WriteVirtualMemory(Buffer->Address, &Buffer->Value, sizeof(ULONG64));
		break;
	}
	case IOCTL_SET_BUFFER:
	{
		BUFFER_INFO* Buffer = (BUFFER_INFO*)Irp->AssociatedIrp.SystemBuffer;
		KdPrint(("> FlashbangDriver: Patching memory in %s module...\n", Buffer->ModuleName));
		Status = PatchModuleMemory(Buffer);
		break;
	}
	default:
	{
		Status = STATUS_INVALID_DEVICE_REQUEST;
		break;
	}
	}

	Irp->IoStatus.Status = Status;
	Irp->IoStatus.Information = OutBufferSize;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return Status;
}
