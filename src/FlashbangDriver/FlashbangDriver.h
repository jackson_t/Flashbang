#pragma once

#define NAME_LENGTH 100

extern "C" NTSTATUS DriverEntry(_In_ PDRIVER_OBJECT DriverObject, _In_ PUNICODE_STRING RegistryPath);
void UnloadDriver(_In_ PDRIVER_OBJECT DriverObject);
NTSTATUS DispatchCreateClose(PDEVICE_OBJECT DeviceObject, PIRP Irp);
NTSTATUS DispatchDeviceControl(PDEVICE_OBJECT, PIRP Irp);
NTSTATUS IoSessionNotificationFunction(PVOID SessionObject, PVOID IoObject, ULONG Event, PVOID Context, PVOID NotificationPayload, ULONG PayloadLength);
void IoUnregisterContainerNotificationWithDelay();
NTSTATUS DeleteDriverFile(PUNICODE_STRING RegistryPath);

typedef struct _DEVICE_NAME_REQUEST {
	ULONG  Magic;
	PWSTR  DeviceName;
	PULONG DeviceNameLength;
} DEVICE_NAME_REQUEST, *PDEVICE_NAME_REQUEST;