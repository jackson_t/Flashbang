# automatically generated by the FlatBuffers compiler, do not modify

# namespace: Serialization

class ActionType(object):
    KmSuppressCallbacks = 0
    UmSuppressHooks = 1
    UmSuppressEtw = 2
    UmSuppressFilter = 3
    UmRunCommand = 4
    UmRunPowershell = 5
    UmRunLua = 6
    UmRunShellcode = 7
    KmRunShellcode = 8
    UmPatchMemory = 9
    KmPatchMemory = 10
    UmCheckFileVersion = 11
    UmBlockConnection = 12
    UmSetSystemTime = 13

