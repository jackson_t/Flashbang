# automatically generated by the FlatBuffers compiler, do not modify

# namespace: Serialization

import flatbuffers
from flatbuffers.compat import import_numpy
np = import_numpy()

class SuppressedFunction(object):
    __slots__ = ['_tab']

    @classmethod
    def GetRootAsSuppressedFunction(cls, buf, offset):
        n = flatbuffers.encode.Get(flatbuffers.packer.uoffset, buf, offset)
        x = SuppressedFunction()
        x.Init(buf, n + offset)
        return x

    # SuppressedFunction
    def Init(self, buf, pos):
        self._tab = flatbuffers.table.Table(buf, pos)

    # SuppressedFunction
    def ModulePath(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(4))
        if o != 0:
            return self._tab.String(o + self._tab.Pos)
        return None

    # SuppressedFunction
    def FunctionName(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(6))
        if o != 0:
            return self._tab.String(o + self._tab.Pos)
        return None

    # SuppressedFunction
    def FunctionAddr(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(8))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Uint64Flags, o + self._tab.Pos)
        return 0

    # SuppressedFunction
    def OriginalBytes(self, j):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(10))
        if o != 0:
            a = self._tab.Vector(o)
            return self._tab.Get(flatbuffers.number_types.Uint8Flags, a + flatbuffers.number_types.UOffsetTFlags.py_type(j * 1))
        return 0

    # SuppressedFunction
    def OriginalBytesAsNumpy(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(10))
        if o != 0:
            return self._tab.GetVectorAsNumpy(flatbuffers.number_types.Uint8Flags, o)
        return 0

    # SuppressedFunction
    def OriginalBytesLength(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(10))
        if o != 0:
            return self._tab.VectorLen(o)
        return 0

    # SuppressedFunction
    def OriginalBytesIsNone(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(10))
        return o == 0

    # SuppressedFunction
    def ModifiedBytes(self, j):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(12))
        if o != 0:
            a = self._tab.Vector(o)
            return self._tab.Get(flatbuffers.number_types.Uint8Flags, a + flatbuffers.number_types.UOffsetTFlags.py_type(j * 1))
        return 0

    # SuppressedFunction
    def ModifiedBytesAsNumpy(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(12))
        if o != 0:
            return self._tab.GetVectorAsNumpy(flatbuffers.number_types.Uint8Flags, o)
        return 0

    # SuppressedFunction
    def ModifiedBytesLength(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(12))
        if o != 0:
            return self._tab.VectorLen(o)
        return 0

    # SuppressedFunction
    def ModifiedBytesIsNone(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(12))
        return o == 0

def SuppressedFunctionStart(builder): builder.StartObject(5)
def SuppressedFunctionAddModulePath(builder, modulePath): builder.PrependUOffsetTRelativeSlot(0, flatbuffers.number_types.UOffsetTFlags.py_type(modulePath), 0)
def SuppressedFunctionAddFunctionName(builder, functionName): builder.PrependUOffsetTRelativeSlot(1, flatbuffers.number_types.UOffsetTFlags.py_type(functionName), 0)
def SuppressedFunctionAddFunctionAddr(builder, functionAddr): builder.PrependUint64Slot(2, functionAddr, 0)
def SuppressedFunctionAddOriginalBytes(builder, originalBytes): builder.PrependUOffsetTRelativeSlot(3, flatbuffers.number_types.UOffsetTFlags.py_type(originalBytes), 0)
def SuppressedFunctionStartOriginalBytesVector(builder, numElems): return builder.StartVector(1, numElems, 1)
def SuppressedFunctionAddModifiedBytes(builder, modifiedBytes): builder.PrependUOffsetTRelativeSlot(4, flatbuffers.number_types.UOffsetTFlags.py_type(modifiedBytes), 0)
def SuppressedFunctionStartModifiedBytesVector(builder, numElems): return builder.StartVector(1, numElems, 1)
def SuppressedFunctionEnd(builder): return builder.EndObject()
