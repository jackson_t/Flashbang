import argparse
import base64
import datetime
import binascii
import json
import ntpath
import sys

import yaml
import jsonschema
import flatbuffers

sys.path.insert(0, 'message_serialization_flatbuffers.zip')
import Flashbang.Serialization.Status as Status
import Flashbang.Serialization.Message as Message
import Flashbang.Serialization.MessageType as MessageType
import Flashbang.Serialization.GeneralSettings as GeneralSettings
import Flashbang.Serialization.DriverSettings as DriverSettings
import Flashbang.Serialization.DriverLoadingMethod as DriverLoadingMethod
import Flashbang.Serialization.ActionType as ActionType
import Flashbang.Serialization.ActionRequest as ActionRequest
import Flashbang.Serialization.ActionRequests as ActionRequests
import Flashbang.Serialization.UmCheckFileVersion as UmCheckFileVersion
import Flashbang.Serialization.UmBlockConnection as UmBlockConnection
import Flashbang.Serialization.UmSetSystemTime as UmSetSystemTime
import Flashbang.Serialization.CallbackType as CallbackType
import Flashbang.Serialization.KmSuppressCallbacks as KmSuppressCallbacks
import Flashbang.Serialization.UmSuppressHooks as UmSuppressHooks
import Flashbang.Serialization.UmSuppressEtw as UmSuppressEtw
import Flashbang.Serialization.UmSuppressFilter as UmSuppressFilter
import Flashbang.Serialization.UmRunCommand as UmRunCommand
import Flashbang.Serialization.UmRunPowershell as UmRunPowershell
import Flashbang.Serialization.UmRunLua as UmRunLua
import Flashbang.Serialization.UmRunShellcode as UmRunShellcode
import Flashbang.Serialization.KmRunShellcode as KmRunShellcode
import Flashbang.Serialization.UmPatchMemory as UmPatchMemory
import Flashbang.Serialization.KmPatchMemory as KmPatchMemory


class MessageSerializer(object):
    def __init__(self, yaml_file=None, flashbang_driver_buffer: bytes = None, vulnerable_driver_buffer: bytes = None):
        self.yaml_file = yaml_file
        self.flashbang_driver_buffer = flashbang_driver_buffer

        if yaml_file:
            try:
                self.yaml_data = yaml.load(self.yaml_file, Loader=yaml.FullLoader)
            except Exception as e:
                print(f'[FB] ERROR → Could not load blindlet file; see details below...\n\n{e}')
                sys.exit(1)

            try:
                self.yaml_schema = json.load(open(r'flashbang_schema.json'))
            except Exception as e:
                print(f'[FB] ERROR → Could not load validation schema; see details below...\n\n{e}')
                sys.exit(1)

    def validate(self):
        if self.yaml_file:
            try:
                jsonschema.validate(self.yaml_data, self.yaml_schema)
            except jsonschema.exceptions.ValidationError as e:
                print(f'[FB] ERROR → Could not serialize blindlet; see details below...\n\n{e}')
                sys.exit(1)
        else:
            raise ValueError('A YAML file must be provided for validation.')

    def serialize_get_heartbeat(self):
        builder = flatbuffers.Builder(128)

        Message.MessageStart(builder)
        Message.MessageAddType(builder, MessageType.MessageType().GET_HEARTBEAT)
        builder.Finish(Message.MessageEnd(builder))

        serialized = builder.Output()
        return bytes(serialized)

    def serialize_load_capability(self):
        builder = flatbuffers.Builder(128)

        if self.flashbang_driver_buffer:
            GeneralSettings.GeneralSettingsStartFlashbangDriverVector(builder, len(self.flashbang_driver_buffer))
            [builder.PrependByte(t) for t in self.flashbang_driver_buffer[::-1]]
            flashbang_driver = builder.EndVector(len(self.flashbang_driver_buffer))
        else:
            flashbang_driver = None

        GeneralSettings.GeneralSettingsStart(builder)
        GeneralSettings.GeneralSettingsAddUseDebugMode(builder, False)

        if flashbang_driver:
            GeneralSettings.GeneralSettingsAddFlashbangDriver(builder, flashbang_driver)

        general_settings = GeneralSettings.GeneralSettingsEnd(builder)

        Message.MessageStart(builder)
        Message.MessageAddType(builder, MessageType.MessageType().LOAD_CAPABILITY)
        Message.MessageAddGeneralSettings(builder, general_settings)
        builder.Finish(Message.MessageEnd(builder))

        serialized = builder.Output()
        return bytes(serialized)

    def serialize_load_blindlet(self):
        builder = flatbuffers.Builder(128)
        payload = self.yaml_data['payload']

        # Serialize driver settings and actions.
        driver_settings = self._serialize_driver_settings(builder) if 'driver_settings' in payload else None
        action_requests = self._serialize_actions(builder)

        # Finish message and serialize buffer.
        Message.MessageStart(builder)
        Message.MessageAddType(builder, MessageType.MessageType().LOAD_BLINDLET)
        Message.MessageAddStatus(builder, Status.Status().STATUS_SUCCESS)
        if 'driver_settings' in payload: Message.MessageAddDriverSettings(builder, driver_settings)
        Message.MessageAddActionRequests(builder, action_requests)
        builder.Finish(Message.MessageEnd(builder))

        serialized = builder.Output()
        return bytes(serialized)

    def serialize_apply_blindlet(self):
        builder = flatbuffers.Builder(128)

        Message.MessageStart(builder)
        Message.MessageAddType(builder, MessageType.MessageType().APPLY_BLINDLET)
        builder.Finish(Message.MessageEnd(builder))

        serialized = builder.Output()
        return bytes(serialized)

    def serialize_revert_blindlet(self):
        builder = flatbuffers.Builder(128)

        Message.MessageStart(builder)
        Message.MessageAddType(builder, MessageType.MessageType().REVERT_BLINDLET)
        builder.Finish(Message.MessageEnd(builder))

        serialized = builder.Output()
        return bytes(serialized)

    def serialize_unload_blindlet(self):
        builder = flatbuffers.Builder(128)

        Message.MessageStart(builder)
        Message.MessageAddType(builder, MessageType.MessageType().UNLOAD_BLINDLET)
        builder.Finish(Message.MessageEnd(builder))

        serialized = builder.Output()
        return bytes(serialized)

    def serialize_unload_capability(self):
        builder = flatbuffers.Builder(128)

        Message.MessageStart(builder)
        Message.MessageAddType(builder, MessageType.MessageType().UNLOAD_CAPABILITY)
        builder.Finish(Message.MessageEnd(builder))

        serialized = builder.Output()
        return bytes(serialized)

    def _serialize_driver_settings(self, builder):
        yaml_driver_settings = self.yaml_data['payload']['driver_settings']

        loading_method_map = {
            'FILE': DriverLoadingMethod.DriverLoadingMethod().FILE,
            'SMB': DriverLoadingMethod.DriverLoadingMethod().SMB,
            'WEBDAV': DriverLoadingMethod.DriverLoadingMethod().WEBDAV
        }

        service_name = builder.CreateString(yaml_driver_settings['service_name'])
        destination_path = builder.CreateString(yaml_driver_settings['destination_path'])
        registry_image_path = builder.CreateString(yaml_driver_settings['registry_image_path'])
        use_random_hash = yaml_driver_settings['use_random_hash']
        use_leading_dot = yaml_driver_settings['use_leading_dot']

        DriverSettings.DriverSettingsStart(builder)
        DriverSettings.DriverSettingsAddLoadingMethod(builder, loading_method_map[yaml_driver_settings['loading_method']])
        DriverSettings.DriverSettingsAddServiceName(builder, service_name)
        DriverSettings.DriverSettingsAddDestinationPath(builder, destination_path)
        DriverSettings.DriverSettingsAddRegistryImagePath(builder, registry_image_path)
        DriverSettings.DriverSettingsAddUseRandomHash(builder, use_random_hash)
        DriverSettings.DriverSettingsAddUseLeadingDot(builder, use_leading_dot)

        return DriverSettings.DriverSettingsEnd(builder)

    def _serialize_actions(self, builder):
        yaml_actions = self.yaml_data['payload']['actions']
        action_offsets = []

        # Create an ActionRequest for each action.
        for yaml_action in yaml_actions:
            conf = yaml_action['conf']

            if yaml_action['type'] == 'UmCheckFileVersion':
                action_offsets.append(self._serialize_action_UmCheckFileVersion(builder, conf))

            elif yaml_action['type'] == 'UmBlockConnection':
                action_offsets.append(self._serialize_action_UmBlockConnection(builder, conf))

            elif yaml_action['type'] == 'KmSuppressCallbacks':
                action_offsets.append(self._serialize_action_KmSuppressCallbacks(builder, conf))

            elif yaml_action['type'] == 'UmSuppressHooks':
                action_offsets.append(self._serialize_action_UmSuppressHooks(builder, conf))

            elif yaml_action['type'] == 'UmSuppressEtw':
                action_offsets.append(self._serialize_action_UmSuppressEtw(builder, conf))

            elif yaml_action['type'] == 'UmSuppressFilter':
                action_offsets.append(self._serialize_action_UmSuppressFilter(builder, conf))

            elif yaml_action['type'] == 'UmRunCommand':
                action_offsets.append(self._serialize_action_UmRunCommand(builder, conf))

            elif yaml_action['type'] == 'UmRunLua':
                action_offsets.append(self._serialize_action_UmRunLua(builder, conf))

            elif yaml_action['type'] == 'KmPatchMemory':
                action_offsets.append(self._serialize_action_KmPatchMemory(builder, conf))

        # Create a vector of ActionRequests.
        Message.MessageStartActionRequestsVector(builder, len(yaml_actions))
        [builder.PrependUOffsetTRelative(action_offset) for action_offset in action_offsets[::-1]]
        action_requests = builder.EndVector(len(yaml_actions))

        return action_requests

    def _serialize_action_UmCheckFileVersion(self, builder, conf):
        # Serialize fields.
        file_path = builder.CreateString(conf['file_path'])
        supported_versions_offsets = [builder.CreateString(v) for v in conf['supported_versions']]
        UmCheckFileVersion.UmCheckFileVersionStartSupportedVersionsVector(builder, len(supported_versions_offsets))
        [builder.PrependUOffsetTRelative(o) for o in supported_versions_offsets]
        supported_versions = builder.EndVector(len(supported_versions_offsets))

        # Encapsulate fields in UmCheckFileVersion object.
        UmCheckFileVersion.UmCheckFileVersionStart(builder)
        UmCheckFileVersion.UmCheckFileVersionAddFilePath(builder, file_path)
        UmCheckFileVersion.UmCheckFileVersionAddSupportedVersions(builder, supported_versions)
        request_offset = UmCheckFileVersion.UmCheckFileVersionEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().UmCheckFileVersion)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_UmBlockConnection(self, builder, conf):
        # Serialize fields.
        remote_host = builder.CreateString(conf['remote_host'])
        process_name = builder.CreateString(conf['process_name'])

        # Encapsulate fields in UmBlockConnection object.
        UmBlockConnection.UmBlockConnectionStart(builder)
        UmBlockConnection.UmBlockConnectionAddProcessId(builder, conf['process_id'])
        UmBlockConnection.UmBlockConnectionAddProcessName(builder, process_name)
        UmBlockConnection.UmBlockConnectionAddRemoteHost(builder, remote_host)
        UmBlockConnection.UmBlockConnectionAddRemotePort(builder, conf['remote_port'])
        UmBlockConnection.UmBlockConnectionAddMaxAttempts(builder, conf['max_attempts'])
        request_offset = UmBlockConnection.UmBlockConnectionEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().UmBlockConnection)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_KmSuppressCallbacks(self, builder, conf):
        # Serialize module name.
        module_name = builder.CreateString(conf['module_name'])

        # Serialize callback types.
        enumerated_types = [getattr(CallbackType.CallbackType(), t) for t in conf['types']]
        KmSuppressCallbacks.KmSuppressCallbacksStartTypesVector(builder, len(enumerated_types))
        [builder.PrependByte(t) for t in enumerated_types[::-1]]
        types = builder.EndVector(len(enumerated_types))

        # Encapsulate fields in KmSuppressCallbacks object.
        KmSuppressCallbacks.KmSuppressCallbacksStart(builder)
        KmSuppressCallbacks.KmSuppressCallbacksAddModuleName(builder, module_name)
        KmSuppressCallbacks.KmSuppressCallbacksAddTypes(builder, types)
        request_offset = KmSuppressCallbacks.KmSuppressCallbacksEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().KmSuppressCallbacks)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_UmSuppressHooks(self, builder, conf):
        # Serialize fields.
        functions_offsets = [builder.CreateString(v) for v in conf['functions']]
        UmSuppressHooks.UmSuppressHooksStartFunctionsVector(builder, len(functions_offsets))
        [builder.PrependUOffsetTRelative(o) for o in functions_offsets]
        functions = builder.EndVector(len(functions_offsets))

        # Encapsulate fields in UmSuppressHooks object.
        UmSuppressHooks.UmSuppressHooksStart(builder)
        UmSuppressHooks.UmSuppressHooksAddFunctions(builder, functions)
        request_offset = UmSuppressHooks.UmSuppressHooksEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().UmSuppressHooks)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_UmSuppressEtw(self, builder, conf):
        # Serialize session name.
        session = builder.CreateString(conf['session'])

        # Serialize providers.
        providers_offsets = [builder.CreateString(v) for v in conf['providers']]
        UmSuppressEtw.UmSuppressEtwStartProvidersVector(builder, len(providers_offsets))
        [builder.PrependUOffsetTRelative(o) for o in providers_offsets[::-1]]
        providers = builder.EndVector(len(providers_offsets))

        # Encapsulate fields in UmSuppressEtw object.
        UmSuppressEtw.UmSuppressEtwStart(builder)
        UmSuppressEtw.UmSuppressEtwAddSession(builder, session)
        UmSuppressEtw.UmSuppressEtwAddProviders(builder, providers)
        request_offset = UmSuppressEtw.UmSuppressEtwEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().UmSuppressEtw)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_UmSuppressFilter(self, builder, conf):
        # Serialize fields.
        filter_name = builder.CreateString(conf['filter_name'])

        # Encapsulate fields in UmSuppressFilter object.
        UmSuppressFilter.UmSuppressFilterStart(builder)
        UmSuppressFilter.UmSuppressFilterAddFilterName(builder, filter_name)
        request_offset = UmSuppressFilter.UmSuppressFilterEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().UmSuppressFilter)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_UmRunCommand(self, builder, conf):
        # Serialize fields.
        command = builder.CreateString(conf['command'])

        # Encapsulate fields in UmRunCommand object.
        UmRunCommand.UmRunCommandStart(builder)
        UmRunCommand.UmRunCommandAddCommand(builder, command)
        request_offset = UmRunCommand.UmRunCommandEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().UmRunCommand)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_UmRunLua(self, builder, conf):
        # Serialize fields.
        code = builder.CreateString(conf['code'])

        # Encapsulate fields in UmRunLua object.
        UmRunLua.UmRunLuaStart(builder)
        UmRunLua.UmRunLuaAddCode(builder, code)
        request_offset = UmRunLua.UmRunLuaEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().UmRunLua)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)

    def _serialize_action_KmPatchMemory(self, builder, conf):
        # Serialize fields.
        module_name = builder.CreateString(conf['module_name'])

        KmPatchMemory.KmPatchMemoryStartSearchPatternVector(builder, len(conf['search_pattern']))
        [builder.PrependByte(b) for b in conf['search_pattern'][::-1]]
        search_pattern = builder.EndVector(len(conf['search_pattern']))

        KmPatchMemory.KmPatchMemoryStartOldBytesVector(builder, len(conf['old_bytes']))
        [builder.PrependByte(b) for b in conf['old_bytes'][::-1]]
        old_bytes = builder.EndVector(len(conf['old_bytes']))

        KmPatchMemory.KmPatchMemoryStartNewBytesVector(builder, len(conf['new_bytes']))
        [builder.PrependByte(b) for b in conf['new_bytes'][::-1]]
        new_bytes = builder.EndVector(len(conf['new_bytes']))

        # Encapsulate fields in KmPatchMemory object.
        KmPatchMemory.KmPatchMemoryStart(builder)
        KmPatchMemory.KmPatchMemoryAddModuleName(builder, module_name)
        KmPatchMemory.KmPatchMemoryAddSearchPattern(builder, search_pattern)
        KmPatchMemory.KmPatchMemoryAddOffset(builder, conf['offset'])
        KmPatchMemory.KmPatchMemoryAddOldBytes(builder, old_bytes)
        KmPatchMemory.KmPatchMemoryAddNewBytes(builder, new_bytes)
        request_offset = KmPatchMemory.KmPatchMemoryEnd(builder)

        # Encapsulate in ActionRequest object.
        ActionRequest.ActionRequestStart(builder)
        ActionRequest.ActionRequestAddRequestType(builder, ActionRequests.ActionRequests().KmPatchMemory)
        ActionRequest.ActionRequestAddRequest(builder, request_offset)

        # Return offset.
        return ActionRequest.ActionRequestEnd(builder)


class MessageDeserializer(object):
    def __init__(self):
        pass

    def deserialize(self, b64_string):
        # Deserialize Message.
        serialized_message = bytearray(base64.b64decode(b64_string))
        message = Message.Message().GetRootAsMessage(serialized_message, 0)

        # Parse Message metadata.
        message_type = self._resolve_enum_name(MessageType.MessageType(), message.Type())
        message_status = self._resolve_enum_name(Status.Status(), message.Status())
        message_date = self._filetime_to_isoformat(message.Timestamp())
        print(f'- {message_type} ({message_status}, {message_date})')

        # Parse ActionResponses.
        if message.ActionResponsesLength():
            print('  - Action Responses:')

            for i in range(message.ActionResponsesLength()):
                action_type = self._resolve_enum_name(ActionType.ActionType(), message.ActionResponses(i).Type())
                action_status = self._resolve_enum_name(Status.Status(), message.ActionResponses(i).Status())
                action_date = self._filetime_to_isoformat(message.ActionResponses(i).Timestamp())
                print(f'    - {action_type} ({action_status})')

                if message.ActionResponses(i).SuppressedCallbacksLength():
                    for j in range(message.ActionResponses(i).SuppressedCallbacksLength()):
                        suppressed_callback = message.ActionResponses(i).SuppressedCallbacks(j)
                        callback_type = self._resolve_enum_name(CallbackType.CallbackType(), suppressed_callback.Type())
                        module_name = suppressed_callback.ModuleName().decode()
                        module_offset = suppressed_callback.ModuleOffset()
                        print(f'      - {module_name}+0x{module_offset:x} ({callback_type})')

                if message.ActionResponses(i).SuppressedFunctionsLength():
                    for j in range(message.ActionResponses(i).SuppressedFunctionsLength()):
                        suppressed_function = message.ActionResponses(i).SuppressedFunctions(j)
                        module_name = ntpath.basename(suppressed_function.ModulePath().decode())
                        function_name = suppressed_function.FunctionName().decode()
                        function_addr = suppressed_function.FunctionAddr()
                        print(f'      - {module_name}!{function_name} @ 0x{function_addr:x}')

                if message.ActionResponses(i).SuppressedSession():
                    suppressed_session = message.ActionResponses(i).SuppressedSession()
                    session = suppressed_session.SessionName().decode()
                    providers = [suppressed_session.ProviderGuids(j).decode() for j in range(suppressed_session.ProviderGuidsLength())]
                    for provider in providers:
                        print(f'      - {session} → {provider}')

    def _resolve_enum_name(self, enum_instance, value):
        for attr in dir(enum_instance):
            if getattr(enum_instance, attr) == value:
                return attr
        return '0x{:08X}'.format(value)

    def _filetime_to_unixtime(self, filetime):
        return (filetime - 116444736000000000) / 10_000_000

    def _filetime_to_isoformat(self, filetime):
        return datetime.datetime.fromtimestamp(self._filetime_to_unixtime(filetime)).isoformat()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', help='[s]erialize or [d]eserialize', required=True)
    parser.add_argument('-t', '--type', help='message type')
    parser.add_argument('-b', '--base64', help='base64-encode output', action='store_true')
    parser.add_argument('-i', '--input', help='input buffer or YAML file')
    parser.add_argument('-d', '--flashbang-driver', help='path to Flashbang driver')
    parser.add_argument('-v', '--vulnerable-driver', help='path to vulnerable driver')
    args = parser.parse_args()

    if args.mode.startswith('s'):
        try:
            input_file = open(args.input) if args.input else None
        except Exception as e:
            print(f'[FB] ERROR → Could not load blindlet file; see details below...\n\n{e}')
            sys.exit(1)

        try:
            flashbang_driver_file = open(args.flashbang_driver, 'rb') if args.flashbang_driver else None
            flashbang_driver_buffer = flashbang_driver_file.read() if flashbang_driver_file else None
        except Exception as e:
            print(f'[FB] ERROR → Could not load Flashbang driver file; see details below...\n\n{e}')
            sys.exit(1)
            
        serializer = MessageSerializer(input_file, flashbang_driver_buffer)

        if args.type == 'GET_HEARTBEAT':
            buffer = serializer.serialize_get_heartbeat()
        elif args.type == 'LOAD_CAPABILITY':
            buffer = serializer.serialize_load_capability()
        elif args.type == 'LOAD_BLINDLET':
            serializer.validate()
            buffer = serializer.serialize_load_blindlet()
        elif args.type == 'APPLY_BLINDLET':
            buffer = serializer.serialize_apply_blindlet()
        elif args.type == 'REVERT_BLINDLET':
            buffer = serializer.serialize_revert_blindlet()
        elif args.type == 'UNLOAD_BLINDLET':
            buffer = serializer.serialize_unload_blindlet()
        elif args.type == 'UNLOAD_CAPABILITY':
            buffer = serializer.serialize_unload_capability()
        else:
            buffer = serializer.serialize_get_heartbeat()

        sys.stdout.buffer.write(base64.b64encode(buffer) if args.base64 else buffer)

        if input_file:
            input_file.close()

    elif args.mode.startswith('d'):
        buffer = args.input.strip() if args.input else sys.stdin.read().strip()
        deserializer = MessageDeserializer()
        deserializer.deserialize(buffer)


def test():
    deserializer = MessageDeserializer()
    deserializer.deserialize('140000000000000000000A0014001300000004000A000000ED3BB77617B5D6010000000000000001')
    deserializer.deserialize('140000000000000000000A0014001300000004000A000000902DA78817B5D6010000000000000002')
    deserializer.deserialize('1C0000000000000014005C0013000000040000000000000000001400140000001686EA8A17B5D601000000000000000404000000020000002C0000001000000000000A000E000D00000004000A0000001686EA8A17B5D60100080A0014001300000004000A000000902DA78817B5D6010000000000000006')
    deserializer.deserialize('140000000000000000000A0014001300000004000A0000008B6EE13C17B5D6010000000000000005')
    deserializer.deserialize('140000000000000000000A0014001300000004000A0000008B6EE13C17B5D6010000000000000003')

    with open('../../schemas/load_blindlet.yaml') as input_file:
        bs = MessageSerializer(input_file)
        bs.validate()

        with open('../../schemas/load_capability.dat', 'wb') as output_file:
            output_file.write(bs.serialize_load_capability())

        with open('../../schemas/load_blindlet.dat', 'wb') as output_file:
            output_file.write(bs.serialize_load_blindlet())

        with open('../../schemas/apply_blindlet.dat', 'wb') as output_file:
            output_file.write(bs.serialize_apply_blindlet())

        with open('../../schemas/revert_blindlet.dat', 'wb') as output_file:
            output_file.write(bs.serialize_revert_blindlet())

        with open('../../schemas/unload_blindlet.dat', 'wb') as output_file:
            output_file.write(bs.serialize_unload_blindlet())


if __name__ == '__main__':
    main()
