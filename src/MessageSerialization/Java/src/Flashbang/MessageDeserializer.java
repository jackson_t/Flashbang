package Flashbang;

import Flashbang.Serialization.*;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

public class MessageDeserializer {
    public MessageDeserializer() {

    }

    public String deserializeMessage(String b64SerializedMessage) throws Exception {
        // Deserialize Message.
        ByteBuffer buffer = ByteBuffer.wrap(Base64.getDecoder().decode(b64SerializedMessage));
        Message message = Message.getRootAsMessage(buffer);

        // Start array of lines.
        String prefix = "[>] [FB]";
        List<String> lines = new ArrayList<String>();

        // Parse Message metadata.
        String messageType = resolveEnumToString(MessageType.class, message.type());
        String messageStatus = resolveEnumToString(Status.class, message.status());
        String messageDate = getReadableDate(message.timestamp());
        lines.add(String.format("%s - %s (%s, %s)", prefix, messageType, messageStatus, messageDate));

        // Parse ActionResponses.
        if (message.actionResponsesLength() > 0) {
            lines.add(String.format("%s   - Action Responses:", prefix));

            for (int i = 0; i < message.actionResponsesLength(); i++) {
                String actionType = resolveEnumToString(ActionType.class, message.actionResponses(i).type());
                String actionStatus = resolveEnumToString(Status.class, message.actionResponses(i).status());
                lines.add(String.format("%s     - %s (%s)", prefix, actionType, actionStatus));

                // Parse SuppressedCallback items.
                if (message.actionResponses(i).suppressedCallbacksLength() > 0) {
                    for (int j = 0; j < message.actionResponses(i).suppressedCallbacksLength(); j++) {
                        SuppressedCallback callback = message.actionResponses(i).suppressedCallbacks(j);
                        String moduleName = callback.moduleName();
                        long moduleOffset = callback.moduleOffset();
                        String callbackType = resolveEnumToString(CallbackType.class, callback.type());
                        lines.add(String.format("%s       - %s+0x%x (%s)", prefix, moduleName, moduleOffset, callbackType));
                    }
                }

                // Parse SuppressedFunction items.
                if (message.actionResponses(i).suppressedFunctionsLength() > 0) {
                    for (int j = 0; j < message.actionResponses(i).suppressedFunctionsLength(); j++) {
                        SuppressedFunction function = message.actionResponses(i).suppressedFunctions(j);
                        String moduleName = Paths.get(function.modulePath()).getFileName().toString();
                        String functionName = function.functionName();
                        long functionAddr = function.functionAddr();
                        lines.add(String.format("%s       - %s!%s @ 0x%x", prefix, moduleName, functionName, functionAddr));
                    }
                }

                // Parse SuppressedSession item.
                if (message.actionResponses(i).suppressedSession() != null)
                {
                    SuppressedSession session = message.actionResponses(i).suppressedSession();
                    String sessionName = session.sessionName();
                    List<String> providerNames = new ArrayList<String>();
                    for (int j = 0; j < session.providerGuidsLength(); j++) { providerNames.add(session.providerGuids(j)); }
                    for (String providerName : providerNames) {
                        lines.add(String.format("%s       - %s: %s", prefix, sessionName, providerName));
                    }
                }
            }
        }

        // Join and return all lines.
        return String.join("\n", lines);
    }

    private String resolveEnumToString(Class clazz, Object value) throws IllegalAccessException {
        // Attempt to get field name where possible.
        for (Field field : clazz.getDeclaredFields()) {
            if (field.get(value).equals(value)) {
                return field.getName();
            }
        }

        // Return a hex string if no field name is available.
        return String.format("0x%08X", value);
    }

    private String getReadableDate(long ldapTime) {
        long unixTime = (ldapTime - 116444736000000000L) / 10_000_000L;
        Date timestamp = new Date(unixTime * 1000);
        LocalDateTime localDateTime = timestamp.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
}
