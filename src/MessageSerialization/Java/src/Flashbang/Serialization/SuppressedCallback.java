// automatically generated by the FlatBuffers compiler, do not modify

package Flashbang.Serialization;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class SuppressedCallback extends Table {
  public static void ValidateVersion() { Constants.FLATBUFFERS_1_12_0(); }
  public static SuppressedCallback getRootAsSuppressedCallback(ByteBuffer _bb) { return getRootAsSuppressedCallback(_bb, new SuppressedCallback()); }
  public static SuppressedCallback getRootAsSuppressedCallback(ByteBuffer _bb, SuppressedCallback obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__assign(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __reset(_i, _bb); }
  public SuppressedCallback __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public byte type() { int o = __offset(4); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateType(byte type) { int o = __offset(4); if (o != 0) { bb.put(o + bb_pos, type); return true; } else { return false; } }
  public String moduleName() { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer moduleNameAsByteBuffer() { return __vector_as_bytebuffer(6, 1); }
  public ByteBuffer moduleNameInByteBuffer(ByteBuffer _bb) { return __vector_in_bytebuffer(_bb, 6, 1); }
  public long moduleOffset() { int o = __offset(8); return o != 0 ? bb.getLong(o + bb_pos) : 0L; }
  public boolean mutateModuleOffset(long module_offset) { int o = __offset(8); if (o != 0) { bb.putLong(o + bb_pos, module_offset); return true; } else { return false; } }
  public long callbackAddress() { int o = __offset(10); return o != 0 ? bb.getLong(o + bb_pos) : 0L; }
  public boolean mutateCallbackAddress(long callback_address) { int o = __offset(10); if (o != 0) { bb.putLong(o + bb_pos, callback_address); return true; } else { return false; } }
  public long originalQword() { int o = __offset(12); return o != 0 ? bb.getLong(o + bb_pos) : 0L; }
  public boolean mutateOriginalQword(long original_qword) { int o = __offset(12); if (o != 0) { bb.putLong(o + bb_pos, original_qword); return true; } else { return false; } }

  public static int createSuppressedCallback(FlatBufferBuilder builder,
      byte type,
      int module_nameOffset,
      long module_offset,
      long callback_address,
      long original_qword) {
    builder.startTable(5);
    SuppressedCallback.addOriginalQword(builder, original_qword);
    SuppressedCallback.addCallbackAddress(builder, callback_address);
    SuppressedCallback.addModuleOffset(builder, module_offset);
    SuppressedCallback.addModuleName(builder, module_nameOffset);
    SuppressedCallback.addType(builder, type);
    return SuppressedCallback.endSuppressedCallback(builder);
  }

  public static void startSuppressedCallback(FlatBufferBuilder builder) { builder.startTable(5); }
  public static void addType(FlatBufferBuilder builder, byte type) { builder.addByte(0, type, 0); }
  public static void addModuleName(FlatBufferBuilder builder, int moduleNameOffset) { builder.addOffset(1, moduleNameOffset, 0); }
  public static void addModuleOffset(FlatBufferBuilder builder, long moduleOffset) { builder.addLong(2, moduleOffset, 0L); }
  public static void addCallbackAddress(FlatBufferBuilder builder, long callbackAddress) { builder.addLong(3, callbackAddress, 0L); }
  public static void addOriginalQword(FlatBufferBuilder builder, long originalQword) { builder.addLong(4, originalQword, 0L); }
  public static int endSuppressedCallback(FlatBufferBuilder builder) {
    int o = builder.endTable();
    return o;
  }

  public static final class Vector extends BaseVector {
    public Vector __assign(int _vector, int _element_size, ByteBuffer _bb) { __reset(_vector, _element_size, _bb); return this; }

    public SuppressedCallback get(int j) { return get(new SuppressedCallback(), j); }
    public SuppressedCallback get(SuppressedCallback obj, int j) {  return obj.__assign(__indirect(__element(j), bb), bb); }
  }
}

