package Flashbang;

import Flashbang.Serialization.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.flatbuffers.FlatBufferBuilder;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import jakarta.xml.bind.DatatypeConverter;

public class MessageSerializer {
    public MessageSerializer() {

    }

    public String serializeGetHeartbeat() {
        FlatBufferBuilder builder = new FlatBufferBuilder(32);

        Message.startMessage(builder);
        Message.addType(builder, MessageType.GET_HEARTBEAT);
        builder.finish(Message.endMessage(builder));

        return DatatypeConverter.printBase64Binary(builder.sizedByteArray());
    }

    public String serializeLoadCapability(boolean useDebugMode, String flashbangDriverPath, String vulnerableDriverPath) {
        FlatBufferBuilder builder = new FlatBufferBuilder(32);

        // Create a vector for the Flashbang driver (if supplied).
        int flashbangDriverOffset = 0;
        if (!flashbangDriverPath.isEmpty()) {
            try {
                byte[] fileContents = getFileContentsAsBytes(flashbangDriverPath);
                flashbangDriverOffset = GeneralSettings.createFlashbangDriverVector(builder, fileContents);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Create a vector for the vulnerable driver (if supplied).
        int vulnerableDriverOffset = 0;
        if (!vulnerableDriverPath.isEmpty()) {
            try {
                byte[] fileContents = getFileContentsAsBytes(vulnerableDriverPath);
                vulnerableDriverOffset = GeneralSettings.createVulnerableDriverVector(builder, fileContents);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Encapsulate supplied settings and file buffers in GeneralSettings object.
        GeneralSettings.startGeneralSettings(builder);
        GeneralSettings.addUseDebugMode(builder, useDebugMode);
        if (flashbangDriverOffset > 0) GeneralSettings.addFlashbangDriver(builder, flashbangDriverOffset);
        if (vulnerableDriverOffset > 0) GeneralSettings.addVulnerableDriver(builder, vulnerableDriverOffset);
        int generalSettingsOffset = GeneralSettings.endGeneralSettings(builder);

        // Encapsulate GeneralSettings object in Message object.
        Message.startMessage(builder);
        Message.addType(builder, MessageType.LOAD_CAPABILITY);
        Message.addGeneralSettings(builder, generalSettingsOffset);
        builder.finish(Message.endMessage(builder));

        return DatatypeConverter.printBase64Binary(builder.sizedByteArray());
    }

    public String serializeLoadBlindlet(String schemaPath, String blindletPath) throws Exception {
        if (validateBlindlet(schemaPath, blindletPath).size() > 0) {
            throw new IllegalArgumentException("Cannot load blindlet due to validation error(s); see Script Console for details.");
        }

        // Create JsonNode for blindlet.
        JsonNode node = new ObjectMapper(new YAMLFactory()).readTree(getFileContentsAsString(blindletPath));

        // Serialize DriverSettings and ActionRequests.
        FlatBufferBuilder builder = new FlatBufferBuilder(32);
        JsonNode driverSettingsNode = node.get("payload").get("driver_settings");
        JsonNode actionRequestsNode = node.get("payload").get("actions");
        int driverSettingsOffset = (driverSettingsNode == null) ? 0 : serializeDriverSettings(builder, driverSettingsNode);
        int actionRequestsOffset = serializeActionRequests(builder, actionRequestsNode);

        Message.startMessage(builder);
        Message.addType(builder, MessageType.LOAD_BLINDLET);
        if (driverSettingsOffset > 0) Message.addDriverSettings(builder, driverSettingsOffset);
        Message.addActionRequests(builder, actionRequestsOffset);
        builder.finish(Message.endMessage(builder));

        return DatatypeConverter.printBase64Binary(builder.sizedByteArray());
    }

    public List<String> validateBlindlet(String schemaPath, String blindletPath) throws IOException {
        // Serialize schema.
        JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V201909);
        JsonSchema schema = factory.getSchema(getFileContentsAsString(schemaPath));

        // Serialize YAML file.
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        JsonNode node = mapper.readTree(getFileContentsAsString(blindletPath));

        // Validate blindlet against schema, and list out all errors (if any).
        List<String> validationErrors = new ArrayList<String>();
        for (ValidationMessage m : schema.validate(node)) { validationErrors.add(m.getMessage()); }

        return validationErrors;
    }

    private int serializeDriverSettings(FlatBufferBuilder builder, JsonNode node) {
        byte loadingMethod = 0;
        switch (node.get("loading_method").asText()) {
            case "FILE":
                loadingMethod = DriverLoadingMethod.FILE;
                break;
            case "SMB":
                loadingMethod = DriverLoadingMethod.SMB;
                break;
            case "WEBDAV":
                loadingMethod = DriverLoadingMethod.WEBDAV;
                break;
            default:
                break;
        }

        int serviceNameOffset = builder.createString(node.get("service_name").asText());
        int destinationPathOffset = builder.createString(node.get("destination_path").asText());
        int registryImagePathOffset = builder.createString(node.get("registry_image_path").asText());
        boolean useRandomHash = node.get("use_random_hash").asBoolean();
        boolean useLeadingDot = node.get("use_leading_dot").asBoolean();

        DriverSettings.startDriverSettings(builder);
        DriverSettings.addLoadingMethod(builder, loadingMethod);
        DriverSettings.addServiceName(builder, serviceNameOffset);
        DriverSettings.addDestinationPath(builder, destinationPathOffset);
        DriverSettings.addRegistryImagePath(builder, registryImagePathOffset);
        DriverSettings.addUseRandomHash(builder, useRandomHash);
        DriverSettings.addUseLeadingDot(builder, useLeadingDot);

        return DriverSettings.endDriverSettings(builder);
    }

    private int serializeActionRequests(FlatBufferBuilder builder, JsonNode node) throws Exception {
        List<Integer> actionRequestOffsets = new ArrayList<Integer>();

        for (JsonNode actionRequest : node) {
            String actionRequestType = actionRequest.get("type").asText();
            JsonNode actionRequestConf = actionRequest.get("conf");

            switch (actionRequestType) {
                case "KmSuppressCallbacks":
                    actionRequestOffsets.add(serializeKmSuppressCallbacks(builder, actionRequestConf));
                    break;
                case "UmSuppressHooks":
                    actionRequestOffsets.add(serializeUmSuppressHooks(builder, actionRequestConf));
                    break;
                case "UmSuppressEtw":
                    actionRequestOffsets.add(serializeUmSuppressEtw(builder, actionRequestConf));
                    break;
                case "UmSuppressFilter":
                    actionRequestOffsets.add(serializeUmSuppressFilter(builder, actionRequestConf));
                    break;
                case "UmRunCommand":
                    actionRequestOffsets.add(serializeUmRunCommandAction(builder, actionRequestConf));
                    break;
                case "UmRunLua":
                    actionRequestOffsets.add(serializeUmRunLuaAction(builder, actionRequestConf));
                    break;
                case "UmRunShellcode":
                    break;
                case "KmRunShellcode":
                    break;
                case "UmPatchMemory":
                    break;
                case "KmPatchMemory":
                    actionRequestOffsets.add(serializeKmPatchMemoryAction(builder, actionRequestConf));
                    break;
                case "UmCheckFileVersion":
                    actionRequestOffsets.add(serializeUmCheckFileVersionAction(builder, actionRequestConf));
                    break;
                case "UmBlockConnection":
                    actionRequestOffsets.add(serializeUmBlockConnectionAction(builder, actionRequestConf));
                    break;
                case "UmSetSystemTime":
                    break;
                default:
                    break;
            }
        }

        int[] offsetsPrimitive = ArrayUtils.toPrimitive(actionRequestOffsets.toArray(new Integer[0]));
        return Message.createActionRequestsVector(builder, offsetsPrimitive);
    }

    private int serializeKmSuppressCallbacks(FlatBufferBuilder builder, JsonNode node) throws NoSuchFieldException, IllegalAccessException {
        // Serialize fields.
        int moduleNameOffset = builder.createString(node.get("module_name").asText());

        // Serialize callback types.
        List<Byte> types = new ArrayList<Byte>();
        for (JsonNode type : node.get("types")) { types.add(CallbackType.class.getField(type.asText()).getByte(null)); }
        byte[] typesPrimitive = ArrayUtils.toPrimitive(types.toArray(new Byte[0]));
        int typesOffset = KmSuppressCallbacks.createTypesVector(builder, typesPrimitive);

        // Encapsulate fields in KmSuppressCallbacks object.
        KmSuppressCallbacks.startKmSuppressCallbacks(builder);
        KmSuppressCallbacks.addModuleName(builder, moduleNameOffset);
        KmSuppressCallbacks.addTypes(builder, typesOffset);
        int requestOffset = KmSuppressCallbacks.endKmSuppressCallbacks(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.KmSuppressCallbacks);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeUmSuppressHooks(FlatBufferBuilder builder, JsonNode node) throws NoSuchFieldException, IllegalAccessException {
        // Serialize fields.
        List<Integer> functionOffsets = new ArrayList<Integer>();
        for (JsonNode functionNode : node.get("functions")) { functionOffsets.add(builder.createString(functionNode.asText())); }
        int[] functionsIntOffsets = functionOffsets.stream().mapToInt(i -> i).toArray();
        int functionsOffset = UmSuppressHooks.createFunctionsVector(builder, functionsIntOffsets);

        // Encapsulate fields in UmSuppressHooks object.
        UmSuppressHooks.startUmSuppressHooks(builder);
        UmSuppressHooks.addFunctions(builder, functionsOffset);
        int requestOffset = UmSuppressHooks.endUmSuppressHooks(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.UmSuppressHooks);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeUmSuppressEtw(FlatBufferBuilder builder, JsonNode node) {
        // Serialize fields.
        int sessionOffset = builder.createString(node.get("session").asText());
        List<Integer> providerOffsets = new ArrayList<Integer>();
        for (JsonNode providerNode : node.get("providers")) { providerOffsets.add(builder.createString(providerNode.asText())); }
        int[] providersIntOffsets = providerOffsets.stream().mapToInt(i -> i).toArray();
        int providersOffset = UmSuppressHooks.createFunctionsVector(builder, providersIntOffsets);

        // Encapsulate fields in UmSuppressEtw object.
        UmSuppressEtw.startUmSuppressEtw(builder);
        UmSuppressEtw.addSession(builder, sessionOffset);
        UmSuppressEtw.addProviders(builder, providersOffset);
        int requestOffset = UmSuppressEtw.endUmSuppressEtw(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.UmSuppressEtw);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeUmSuppressFilter(FlatBufferBuilder builder, JsonNode node) {
        // ToDo: Serialize fields.
        int filterNameOffset = builder.createString(node.get("filter_name").asText());

        // ToDo: Encapsulate fields in UmSuppressFilter object.
        UmSuppressFilter.startUmSuppressFilter(builder);
        UmSuppressFilter.addFilterName(builder, filterNameOffset);
        int requestOffset = UmSuppressFilter.endUmSuppressFilter(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.UmSuppressFilter);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeUmRunCommandAction(FlatBufferBuilder builder, JsonNode node) {
        // Serialize fields.
        int commandOffset = builder.createString(node.get("command").asText());

        // Encapsulate fields in UmRunCommand object.
        UmRunCommand.startUmRunCommand(builder);
        UmRunCommand.addCommand(builder, commandOffset);
        int requestOffset = UmRunCommand.endUmRunCommand(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.UmRunCommand);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeUmRunLuaAction(FlatBufferBuilder builder, JsonNode node) {
        // Serialize fields.
        int codeOffset = builder.createString(node.get("code").asText());

        // Encapsulate fields in UmRunLua object.
        UmRunLua.startUmRunLua(builder);
        UmRunLua.addCode(builder, codeOffset);
        int requestOffset = UmRunLua.endUmRunLua(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.UmRunLua);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeKmPatchMemoryAction(FlatBufferBuilder builder, JsonNode node) {
        // Serialize fields.
        int moduleNameOffset = builder.createString(node.get("module_name").asText());
        byte[] searchPattern = new ObjectMapper().convertValue(node.get("search_pattern"), byte[].class);
        int searchPatternOffset = KmPatchMemory.createSearchPatternVector(builder, searchPattern);
        int offset = node.get("offset").asInt();
        byte[] oldBytes = new ObjectMapper().convertValue(node.get("old_bytes"), byte[].class);
        int oldBytesOffset = KmPatchMemory.createSearchPatternVector(builder, oldBytes);
        byte[] newBytes = new ObjectMapper().convertValue(node.get("new_bytes"), byte[].class);
        int newBytesOffset = KmPatchMemory.createSearchPatternVector(builder, newBytes);

        // Encapsulate fields in KmPatchMemory object.
        KmPatchMemory.startKmPatchMemory(builder);
        KmPatchMemory.addModuleName(builder, moduleNameOffset);
        KmPatchMemory.addSearchPattern(builder, searchPatternOffset);
        KmPatchMemory.addOffset(builder, offset);
        KmPatchMemory.addOldBytes(builder, oldBytesOffset);
        KmPatchMemory.addNewBytes(builder, newBytesOffset);
        int requestOffset = KmPatchMemory.endKmPatchMemory(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.KmPatchMemory);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeUmCheckFileVersionAction(FlatBufferBuilder builder, JsonNode node) {
        // Serialize file path.
        int filePathOffset = builder.createString(node.get("file_path").asText());

        // Serialize supported versions.
        List<Integer> supportedVersionOffsets = new ArrayList<Integer>();
        for (JsonNode supportedVersionNode : node.get("supported_versions")) { supportedVersionOffsets.add(builder.createString(supportedVersionNode.asText())); }
        int[] supportedVersionIntOffsets = supportedVersionOffsets.stream().mapToInt(i -> i).toArray();
        int supportedVersionsOffset = UmCheckFileVersion.createSupportedVersionsVector(builder, supportedVersionIntOffsets);

        // Encapsulate fields in UmCheckFileVersion object.
        UmCheckFileVersion.startUmCheckFileVersion(builder);
        UmCheckFileVersion.addFilePath(builder, filePathOffset);
        UmCheckFileVersion.addSupportedVersions(builder, supportedVersionsOffset);
        int requestOffset = UmCheckFileVersion.endUmCheckFileVersion(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.UmCheckFileVersion);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    private int serializeUmBlockConnectionAction(FlatBufferBuilder builder, JsonNode node) {
        // Serialize fields.
        int processId = node.get("process_id").asInt();
        int processNameOffset = builder.createString(node.get("process_name").asText());
        int remoteHostOffset = builder.createString(node.get("remote_host").asText());
        int remotePort = node.get("remote_port").asInt();
        int maxAttempts = node.get("max_attempts").asInt();

        // Encapsulate fields in UmBlockConnection object.
        UmBlockConnection.startUmBlockConnection(builder);
        UmBlockConnection.addProcessId(builder, processId);
        UmBlockConnection.addProcessName(builder, processNameOffset);
        UmBlockConnection.addRemoteHost(builder, remoteHostOffset);
        UmBlockConnection.addRemotePort(builder, remotePort);
        UmBlockConnection.addMaxAttempts(builder, maxAttempts);
        int requestOffset = UmBlockConnection.endUmBlockConnection(builder);

        // Encapsulate in ActionRequest object.
        ActionRequest.startActionRequest(builder);
        ActionRequest.addRequestType(builder, ActionRequests.UmBlockConnection);
        ActionRequest.addRequest(builder, requestOffset);

        // Return offset.
        return ActionRequest.endActionRequest(builder);
    }

    public String serializeApplyBlindlet() {
        FlatBufferBuilder builder = new FlatBufferBuilder(32);

        Message.startMessage(builder);
        Message.addType(builder, MessageType.APPLY_BLINDLET);
        builder.finish(Message.endMessage(builder));

        return DatatypeConverter.printBase64Binary(builder.sizedByteArray());
    }

    public String serializeRevertBlindlet() {
        FlatBufferBuilder builder = new FlatBufferBuilder(32);

        Message.startMessage(builder);
        Message.addType(builder, MessageType.REVERT_BLINDLET);
        builder.finish(Message.endMessage(builder));

        return DatatypeConverter.printBase64Binary(builder.sizedByteArray());
    }

    public String serializeUnloadBlindlet() {
        FlatBufferBuilder builder = new FlatBufferBuilder(32);

        Message.startMessage(builder);
        Message.addType(builder, MessageType.UNLOAD_BLINDLET);
        builder.finish(Message.endMessage(builder));

        return DatatypeConverter.printBase64Binary(builder.sizedByteArray());
    }

    public String serializeUnloadCapability() {
        FlatBufferBuilder builder = new FlatBufferBuilder(32);

        Message.startMessage(builder);
        Message.addType(builder, MessageType.UNLOAD_CAPABILITY);
        builder.finish(Message.endMessage(builder));

        return DatatypeConverter.printBase64Binary(builder.sizedByteArray());
    }

    private byte[] getFileContentsAsBytes(String path) throws IOException {
        return Files.readAllBytes(Paths.get(path));
    }

    private String getFileContentsAsString(String path) throws IOException {
        return new String(getFileContentsAsBytes(path));
    }
}
