#pragma once

#include "Serialization.h"

using namespace flatbuffers;
using namespace Flashbang::Serialization;

namespace Flashbang
{	
	class Dispatcher
	{
	public:
		static void HandleMessage(IN PVOID InputBuffer, IN SIZE_T InputLength, OUT PVOID* OutputBuffer, OUT SIZE_T* OutputLength);
	private:
		static FlatBufferBuilder GetHeartbeat(const Message* Message);
		static FlatBufferBuilder LoadCapability(const Message* Message);
		static FlatBufferBuilder LoadBlindlet(const Message* Message);
		static FlatBufferBuilder ApplyBlindlet(const Message* Message);
		static FlatBufferBuilder RevertBlindlet(const Message* Message);
		static FlatBufferBuilder UnloadBlindlet();
		static FlatBufferBuilder UnloadCapability();
	};
}