#include "pch.h"
#include <evntrace.h>
#include <tdh.h>
#include <objbase.h>

#include "../Action.h"
#include "../Utility.h"

#pragma comment(lib, "Tdh.lib")

using namespace flatbuffers;
using namespace Flashbang::Action;

#define MAX_SESSIONS 64
#define MAX_SESSION_NAME_LEN 1024
#define MAX_LOGFILE_PATH_LEN 1024

// Struct:    TRACE_PROVIDER
// Describes: An ETW trace provider.
// Members:
// - ProviderId:   GUID of the provider.
// - ProviderName: Name of the provider.
typedef struct _TRACE_PROVIDER
{
	GUID ProviderId = { 0 };
	WCHAR ProviderName[MAX_PATH] = { 0 };
} TRACE_PROVIDER, * PTRACE_PROVIDER;

// Struct:    TRACING_SESSION
// Describes: An ETW tracing session.
// Members:
// - LoggerId:         GUID of the session.
// - InstanceName:     Name of the session.
// - EnabledProviders: List of providers enabled for the session.
typedef struct _TRACING_SESSION
{
	USHORT LoggerId = 0;
	WCHAR  InstanceName[MAX_SESSION_NAME_LEN] = { 0 };
} TRACING_SESSION, * PTRACING_SESSION;

// Function:    PopulateSessionProviders
// Description: Populates a TRACING_SESSION object with its enabled providers.
// Called from: GetSessions
// Remarks:     Adapted from https://docs.microsoft.com/en-us/windows/win32/api/evntrace/nf-evntrace-enumeratetraceguidsex.
std::vector<PTRACE_PROVIDER> GetProviders()
{
	std::vector<PTRACE_PROVIDER> Providers;
	
	PPROVIDER_ENUMERATION_INFO PeiBuffer = nullptr;
	ULONG PeiBufferSize = 0;

	// Get information on each provider (incl. name) used for querying later.
	if (TdhEnumerateProviders(PeiBuffer, &PeiBufferSize) == ERROR_INSUFFICIENT_BUFFER)
	{
		PeiBuffer = (PPROVIDER_ENUMERATION_INFO)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, PeiBufferSize);

		if (!PeiBuffer)
			return Providers;

		if (TdhEnumerateProviders(PeiBuffer, &PeiBufferSize) != ERROR_SUCCESS)
			return Providers;
	}

	// Place the provider names and GUIDs into the TRACE_PROVIDER vector.
	for (unsigned int i = 0; i < PeiBuffer->NumberOfProviders; i++)
	{
		PTRACE_PROVIDER Provider = new TRACE_PROVIDER;

		Provider->ProviderId = PeiBuffer->TraceProviderInfoArray[i].ProviderGuid;

		if (PeiBuffer->TraceProviderInfoArray[i].ProviderNameOffset)
		{
			LPWSTR ProviderName = (LPWSTR)((DWORD_PTR)PeiBuffer + PeiBuffer->TraceProviderInfoArray[i].ProviderNameOffset);
			CopyMemory(Provider->ProviderName, ProviderName, MAX_PATH);
		}

		Providers.push_back(Provider);
	}

	HeapFree(GetProcessHeap(), NULL, PeiBuffer);
	return Providers;
}

// Function:    GetSessions
// Description: Returns a list of ETW tracing sessions.
std::vector<PTRACING_SESSION> GetSessions()
{
	std::vector<PTRACING_SESSION> Sessions;

	// Allocate memory for EVENT_TRACE_PROPERTIES.
	ULONG PropertiesSize = sizeof(EVENT_TRACE_PROPERTIES) + (MAX_SESSION_NAME_LEN * sizeof(WCHAR)) + (MAX_LOGFILE_PATH_LEN * sizeof(WCHAR));
	ULONG BufferSize = PropertiesSize * MAX_SESSIONS;
	PEVENT_TRACE_PROPERTIES EtpBuffer = (PEVENT_TRACE_PROPERTIES)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, BufferSize);
	PEVENT_TRACE_PROPERTIES EtpSessions[MAX_SESSIONS];

	for (USHORT i = 0; i < MAX_SESSIONS; i++)
	{
		EtpSessions[i] = (EVENT_TRACE_PROPERTIES*)((BYTE*)EtpBuffer + (i * PropertiesSize));
		EtpSessions[i]->Wnode.BufferSize = PropertiesSize;
		EtpSessions[i]->LoggerNameOffset = sizeof(EVENT_TRACE_PROPERTIES);
		EtpSessions[i]->LogFileNameOffset = sizeof(EVENT_TRACE_PROPERTIES) + (MAX_SESSION_NAME_LEN * sizeof(WCHAR));
	}

	ULONG SessionCount = 0;
	QueryAllTraces(EtpSessions, (ULONG)MAX_SESSIONS, &SessionCount);

	// Create TRACING_SESSION objects.
	for (USHORT i = 0; i < SessionCount; i++)
	{
		PTRACING_SESSION Session = new TRACING_SESSION;

		Session->LoggerId = (USHORT)EtpSessions[i]->Wnode.HistoricalContext;
		CopyMemory(Session->InstanceName, (LPWSTR)((char*)EtpSessions[i] + EtpSessions[i]->LoggerNameOffset), MAX_SESSION_NAME_LEN);

		Sessions.push_back(Session);
	}

	HeapFree(GetProcessHeap(), NULL, EtpBuffer);
	return Sessions;
}

// Function:    DisableProvider
// Description: Disables the provider for a session, given its logger ID and the provider's GUID.
// Called from: UmeDisableSelectedProvider
DWORD EnableProvider(USHORT LoggerId, LPCGUID ProviderGuid, BOOLEAN Enable)
{
	return EnableTraceEx2(
		(TRACEHANDLE)LoggerId,
		ProviderGuid,
		(Enable) ? EVENT_CONTROL_CODE_ENABLE_PROVIDER : EVENT_CONTROL_CODE_DISABLE_PROVIDER,
		TRACE_LEVEL_VERBOSE,
		NULL,
		NULL,
		NULL,
		nullptr);
}

Response UmSuppressEtw::Apply(FlatBufferBuilder* Builder, const Serialization::UmSuppressEtw* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_UNSUCCESSFUL);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	std::vector<PTRACING_SESSION> TraceSessions = GetSessions();
	std::vector<PTRACE_PROVIDER> TraceProviders = GetProviders();
	flatbuffers::Offset<Serialization::SuppressedSession> SuppressedSession;
	std::vector<std::string> RequestedProviderNames;

	for (PTRACING_SESSION Session : TraceSessions)
	{
		std::string CurrentSessionName = Utility::WStringToString(std::wstring(Session->InstanceName));
		std::string RequestedSessionName = std::string(ActionRequest->session()->c_str());

		if (CurrentSessionName == RequestedSessionName)
		{
			for (unsigned int i = 0; i < ActionRequest->providers()->size(); i++)
			{
				RequestedProviderNames.push_back(std::string(ActionRequest->providers()->Get(i)->c_str()));
				GUID RequestedProviderGuid = { 0 };

				// If provider name is actually a GUID:
				if (CLSIDFromString(Utility::StringToWString(RequestedProviderNames.at(i)).c_str(), &RequestedProviderGuid) == NOERROR)
				{
					DWORD EnableStatus = EnableProvider(Session->LoggerId, &RequestedProviderGuid, FALSE);

					if (EnableStatus == ERROR_SUCCESS)
					{
						SuppressedSession = Serialization::CreateSuppressedSession(*Builder,
							Builder->CreateString(ActionRequest->session()),
							Builder->CreateVectorOfStrings(RequestedProviderNames));
						Status = Serialization::Status_STATUS_SUCCESS;
					}
					else if (EnableStatus == ERROR_ACCESS_DENIED)
					{
						Status = Serialization::Status_STATUS_PRIVILEGE_NOT_HELD;
					}
					else
					{
						Status = Serialization::Status_STATUS_UNSUCCESSFUL;
					}
				}
				else
				{
					for (PTRACE_PROVIDER Provider : TraceProviders)
					{
						std::string CurrentProviderName = Utility::WStringToString(std::wstring(Provider->ProviderName));

						// Match for provider name:
						if (CurrentProviderName == RequestedProviderNames.at(i))
						{
							DWORD EnableStatus = EnableProvider(Session->LoggerId, &Provider->ProviderId, FALSE);

							if (EnableStatus == ERROR_SUCCESS)
							{
								SuppressedSession = Serialization::CreateSuppressedSession(*Builder,
									Builder->CreateString(ActionRequest->session()),
									Builder->CreateVectorOfStrings(RequestedProviderNames));
								Status = Serialization::Status_STATUS_SUCCESS;
							}
							else if (EnableStatus == ERROR_ACCESS_DENIED)
							{
								Status = Serialization::Status_STATUS_PRIVILEGE_NOT_HELD;
							}
							else
							{
								Status = Serialization::Status_STATUS_UNSUCCESSFUL;
							}

							break;
						}
					}
				}
			}

			break;
		}
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSuppressEtw);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	SubBuilder.add_suppressed_session(SuppressedSession);
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Response UmSuppressEtw::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	if (!ActionResponse->suppressed_session())
	{
		Status = Serialization::Status_STATUS_UNSUCCESSFUL;

		SubBuilder.add_type(Serialization::ActionType::ActionType_UmSuppressEtw);
		SubBuilder.add_status((Serialization::Status)Status);
		SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
		Action::Response Response({ Status, SubBuilder.Finish() });
		return Response;
	}

	std::vector<PTRACING_SESSION> TraceSessions = GetSessions();
	std::vector<PTRACE_PROVIDER> TraceProviders = GetProviders();
	std::string TargetSessionName = std::string(ActionResponse->suppressed_session()->session_name()->c_str());

	for (PTRACING_SESSION Session : TraceSessions)
	{
		std::string CurrentSessionName = Utility::WStringToString(std::wstring(Session->InstanceName));

		if (CurrentSessionName != TargetSessionName)
		{
			continue;
		}
		else
		{
			for (unsigned int i = 0; i < ActionResponse->suppressed_session()->provider_guids()->size(); i++)
			{
				GUID TargetProviderGuid = { 0 };
				std::string TargetProviderName = std::string(ActionResponse->suppressed_session()->provider_guids()->Get(i)->c_str());

				if (CLSIDFromString(Utility::StringToWString(TargetProviderName).c_str(), &TargetProviderGuid) == NOERROR)
				{
					EnableProvider(Session->LoggerId, &TargetProviderGuid, TRUE);
				}
				else
				{
					for (PTRACE_PROVIDER Provider : TraceProviders)
					{
						std::string CurrentProviderName = Utility::WStringToString(std::wstring(Provider->ProviderName));

						if (CurrentProviderName != TargetProviderName)
						{
							continue;
						}
						else
						{
							EnableProvider(Session->LoggerId, &Provider->ProviderId, TRUE);
						}
					}
				}
			}
		}
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSuppressEtw);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}