#include "pch.h"

#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS 1

#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <TlHelp32.h>

#include "../Core.h"
#include "../Action.h"
#include "../Utility.h"

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")

using namespace flatbuffers;
using namespace Flashbang;

typedef struct _CONNECTION_PARAMS
{
	DWORD ProcessId   = 0;
	PCSTR ProcessName = nullptr;
	PCSTR RemoteHost  = nullptr;
	DWORD RemotePort  = 0;
	DWORD MaxAttempts = 0;
} CONNECTION_PARAMS, *PCONNECTION_PARAMS;

DWORD BlockConnection(PCONNECTION_PARAMS ConnectionParams)
{
	DWORD Status(Flashbang::Serialization::Status_STATUS_UNSUCCESSFUL);
	DWORD NumAttempts = 0;

	Core::g_ThreadStatuses->insert(std::make_pair(GetCurrentThreadId(), TRUE));

	// Run loop as long as Core::g_ThreadStatuses[GetCurrentThreadId()] is set to true.
	// This will be set to false in the revert function for this action.
	while (Core::g_ThreadStatuses->find(GetCurrentThreadId())->second)
	{
		// GetTcpTable2 variables.
		PMIB_TCPTABLE2 TcpTable = (MIB_TCPTABLE2*)HeapAlloc(GetProcessHeap(), 0, sizeof(MIB_TCPTABLE2));
		ULONG Size = sizeof(MIB_TCPTABLE2);

		// Make an initial call to GetTcpTable2 to get the necessary size into the Size variable.
		if (GetTcpTable2(TcpTable, &Size, TRUE) == ERROR_INSUFFICIENT_BUFFER)
		{
			HeapFree(GetProcessHeap(), NULL, TcpTable);
			TcpTable = (MIB_TCPTABLE2*)HeapAlloc(GetProcessHeap(), 0, Size);

			if (TcpTable)
			{
				// Make a second call to GetTcpTable2 to get the actual data we require.
				if (GetTcpTable2(TcpTable, &Size, TRUE) == NO_ERROR)
				{
					for (unsigned int i = 0; i < TcpTable->dwNumEntries; i++)
					{
						// Declare initial fixed constraints.
						BOOL IsRemoteAddr = TcpTable->table[i].dwRemoteAddr != 0;
						BOOL IsNonListening = TcpTable->table[i].dwState != MIB_TCP_STATE_LISTEN;

						// Skip connections that don't satisfy the constraints above.
						if (!(IsRemoteAddr && IsNonListening))
						{
							continue;
						}

						// Start collecting block-eligibility points.
						DWORD NumMatchPoints = 0;
						DWORD TotalMatchPoints = 0;

						// Validate PID (specific PID overrides process name).
						if (ConnectionParams->ProcessId > 0)
						{
							TotalMatchPoints++;
							NumMatchPoints += (TcpTable->table[i].dwOwningPid == ConnectionParams->ProcessId) ? 1 : 0;
						}
						else if (ConnectionParams->ProcessName)
						{
							if (strlen(ConnectionParams->ProcessName) > 0)
							{
								TotalMatchPoints++;

								if (Flashbang::Utility::IsProcessNameMatchedWithPid(TcpTable->table[i].dwOwningPid, ConnectionParams->ProcessName))
								{
									NumMatchPoints += 1;
								}
							}
						}

						// Validate remote host.
						if (ConnectionParams->RemoteHost)
						{
							if (strlen(ConnectionParams->RemoteHost) > 0)
							{
								TotalMatchPoints++;

								// Initialise Winsock.
								WSADATA WsaData{};
								if (WSAStartup(MAKEWORD(2, 2), &WsaData) == 0)
								{
									struct addrinfo* AddrInfoFirstResult = nullptr;
									struct addrinfo* AddrInfoNextResult = nullptr;

									// Get addrinfo for provided RemoteHost value.
									if (GetAddrInfoA(ConnectionParams->RemoteHost, nullptr, nullptr, &AddrInfoFirstResult) == 0)
									{
										for (AddrInfoNextResult = AddrInfoFirstResult; AddrInfoNextResult != nullptr; AddrInfoNextResult = AddrInfoNextResult->ai_next)
										{
											switch (AddrInfoNextResult->ai_family)
											{
											case AF_INET:
											{
												struct sockaddr_in* Ipv4Address = (struct sockaddr_in*)AddrInfoNextResult->ai_addr;

												// Declare match if first IPv4 address matches.
												if (TcpTable->table[i].dwRemoteAddr == Ipv4Address->sin_addr.S_un.S_addr)
												{
													NumMatchPoints++;
													break;
												}

												break;
											}
											default:
											{
												break;
											}
											}
										}

										// Cleanup.
										FreeAddrInfoA(AddrInfoFirstResult);
									}

									// More cleanup.
									WSACleanup();
								}
							}
						}

						// Validate remote port.
						if (ConnectionParams->RemotePort > 0)
						{
							TotalMatchPoints++;
							NumMatchPoints += (TcpTable->table[i].dwRemotePort == htons((WORD)ConnectionParams->RemotePort)) ? 1 : 0;
						}

						// Close suspected connections from the kernel driver to the remote address.
						if (NumMatchPoints == TotalMatchPoints)
						{
							TcpTable->table[i].dwState = MIB_TCP_STATE_DELETE_TCB;
							SetTcpEntry((PMIB_TCPROW)&TcpTable->table[i]);
							Status = Flashbang::Serialization::Status_STATUS_SUCCESS;
							NumAttempts++;

							break;
						}
					}
				}
				else
				{
					// Free buffers and return error.
					HeapFree(GetProcessHeap(), NULL, TcpTable);
					return Flashbang::Serialization::Status_STATUS_UNSUCCESSFUL;
				}

				// Free TcpTable buffer.
				HeapFree(GetProcessHeap(), NULL, TcpTable);

				// Break loop if max attempts have been reached.
				if (ConnectionParams->MaxAttempts > 0)
				{
					if (NumAttempts >= ConnectionParams->MaxAttempts)
					{
						// Free buffers first before breaking.
						HeapFree(GetProcessHeap(), NULL, (PVOID)ConnectionParams->ProcessName);
						HeapFree(GetProcessHeap(), NULL, (PVOID)ConnectionParams->RemoteHost);
						HeapFree(GetProcessHeap(), NULL, (PVOID)ConnectionParams);

						break;
					}
				}
			}
		}
	}

	return Status;
}

Action::Response Flashbang::Action::UmBlockConnection::Apply(FlatBufferBuilder* Builder, const Serialization::UmBlockConnection* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	if (PCONNECTION_PARAMS ConnectionParams = (PCONNECTION_PARAMS)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(CONNECTION_PARAMS)))
	{
		// Reserialize request into CONNECTION_PARAMS structure.
		ConnectionParams->ProcessId = ActionRequest->process_id();
		ConnectionParams->RemotePort = ActionRequest->remote_port();
		ConnectionParams->MaxAttempts = ActionRequest->max_attempts();

		// Allocate buffer for process name string.
		if (ActionRequest->process_name()->size() > 0)
		{
			if (ConnectionParams->ProcessName = (PCSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, (SIZE_T)ActionRequest->process_name()->size() + 1))
			{
				CopyMemory((PVOID)ConnectionParams->ProcessName, ActionRequest->process_name()->c_str(), ActionRequest->process_name()->size());
			}
		}

		// Allocate buffer for remote host string.
		if (ActionRequest->remote_host()->size() > 0)
		{
			if (ConnectionParams->RemoteHost = (PCSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, (SIZE_T)ActionRequest->remote_host()->size() + 1))
			{
				CopyMemory((PVOID)ConnectionParams->RemoteHost, ActionRequest->remote_host()->c_str(), ActionRequest->remote_host()->size());
			}
		}

		// Start block connection thread.
		DWORD ThreadId{};
		HANDLE ThreadHandle = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)BlockConnection, ConnectionParams, 0, &ThreadId);

		// Save thread ID and state.
		SubBuilder.add_thread_id(ThreadId);

		// Wait before next action.
		Sleep(1000);
	}
	else
	{
		Status = Serialization::Status_STATUS_INSUFFICIENT_RESOURCES;
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmBlockConnection);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Action::Response Flashbang::Action::UmBlockConnection::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	// Set the block connection thread status to false to stop the while loop.
	if (DWORD ThreadId = ActionResponse->thread_id())
	{
		map<DWORD, BOOL>::iterator Iterator = Core::g_ThreadStatuses->find(ThreadId);
		if (Iterator != Core::g_ThreadStatuses->end())
		{
			Iterator->second = FALSE;
		}
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmBlockConnection);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}