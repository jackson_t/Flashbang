#include "pch.h"

#include "luajit/lua.hpp"

#include "../Action.h"
#include "../Utility.h"

using namespace flatbuffers;
using namespace Flashbang::Action;

Response UmRunLua::Apply(FlatBufferBuilder* Builder, const Serialization::UmRunLua* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	lua_State* L = luaL_newstate();
	luaL_openlibs(L);

	if (luaL_dostring(L, ActionRequest->code()->c_str()))
		Status = Serialization::Status_STATUS_UNSUCCESSFUL;
	else
		Status = Serialization::Status_STATUS_SUCCESS;

	lua_close(L);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmRunLua);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Response UmRunLua::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmRunLua);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}