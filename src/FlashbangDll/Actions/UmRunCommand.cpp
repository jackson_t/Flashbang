#include "pch.h"

#include "../Action.h"
#include "../Utility.h"

using namespace flatbuffers;
using namespace Flashbang::Action;

Response UmRunCommand::Apply(FlatBufferBuilder* Builder, const Serialization::UmRunCommand* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	PROCESS_INFORMATION ProcessInformation = { 0 };
	STARTUPINFOA StartupInfo = { 0 };
	StartupInfo.cb = sizeof(STARTUPINFOA);
	BOOL IsProcessCreated = CreateProcessA(
		nullptr,
		(LPSTR)ActionRequest->command()->c_str(), // command
		nullptr, nullptr, false, 0, nullptr, nullptr, &StartupInfo, &ProcessInformation);
	CloseHandle(ProcessInformation.hProcess);
	CloseHandle(ProcessInformation.hThread);

	Status = (IsProcessCreated) ? Serialization::Status_STATUS_SUCCESS : Serialization::Status_STATUS_UNSUCCESSFUL;

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmRunCommand);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Response UmRunCommand::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmRunCommand);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}