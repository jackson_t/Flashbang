#include "pch.h"

#include "luajit/lua.hpp"

#include "../Action.h"
#include "../Utility.h"

using namespace flatbuffers;
using namespace Flashbang::Action;

// ToDo: use SysWhispers2 instead.
typedef DWORD(NTAPI* FnNtQuerySystemTime)(IN PLARGE_INTEGER SystemTime);
typedef DWORD(NTAPI* FnNtSetSystemTime)(IN PLARGE_INTEGER SystemTime, OUT PLARGE_INTEGER PreviousTime OPTIONAL);
static FnNtQuerySystemTime NtQuerySystemTime = (FnNtQuerySystemTime)::GetProcAddress(::GetModuleHandle(L"ntdll.dll"), "NtQuerySystemTime");
static FnNtSetSystemTime NtSetSystemTime = (FnNtSetSystemTime)::GetProcAddress(::GetModuleHandle(L"ntdll.dll"), "NtSetSystemTime");

Response UmSetSystemTime::Apply(FlatBufferBuilder* Builder, const Serialization::UmSetSystemTime* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	// Get original time.
	LARGE_INTEGER OriginalSystemTime, ModifiedSystemTime;
	::NtQuerySystemTime(&OriginalSystemTime);

	// Determine the modified time based on request type.
	switch (ActionRequest->type())
	{
	case Serialization::TimeType::TimeType_ABSOLUTE_TIME:
	{
		ModifiedSystemTime.QuadPart = ActionRequest->value();  // ToDo: accept Unix time.
		break;
	}
	case Serialization::TimeType::TimeType_RELATIVE_TIME:
	{
		ModifiedSystemTime.QuadPart = OriginalSystemTime.QuadPart + (ActionRequest->value() * 10'000'000);
		break;
	}
	case Serialization::TimeType::TimeType_BOOT_TIME:
	{
		// Take current date, then rewind to boot time in order to blend in with other driver loads.
		// Add 30-60s so it doesn't look like the first driver to load.
		srand((unsigned int)GetTickCount64());
		unsigned long long OffsetSec = (unsigned long long)rand() % 30 + 30;
		ModifiedSystemTime.QuadPart = OriginalSystemTime.QuadPart - (GetTickCount64() * 10'000) + (OffsetSec * 10'000'000);
		break;
	}
	default:
	{
		ModifiedSystemTime.QuadPart = OriginalSystemTime.QuadPart;
		break;
	}
	}

	// Set the modified time.
	::NtSetSystemTime(&ModifiedSystemTime, &OriginalSystemTime);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSetSystemTime);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Response UmSetSystemTime::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSetSystemTime);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}