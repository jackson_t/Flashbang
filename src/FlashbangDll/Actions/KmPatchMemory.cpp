#include "pch.h"

#include "../Core.h"
#include "../Action.h"
#include "../Utility.h"
#include "../Syscalls.h"
#include "../../FlashbangDriver/Common.h"

#define CTL_CODE(DeviceType, Function, Method, Access) (((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method))
#define METHOD_BUFFERED 0
#define FILE_ANY_ACCESS 0
#define STATUS_SUCCESS  0
#define STATUS_INSUFFICIENT_RESOURCES 0xC000009A
#define STATUS_MEMORY_NOT_ALLOCATED 0xC00000A0
#define STATUS_NOT_FOUND 0xC0000225

using namespace flatbuffers;
using namespace Flashbang;

DWORD SendPatchRequest(PBUFFER_INFO BufferInfo)
{
	DWORD Status(Serialization::Status::Status_STATUS_SUCCESS);

	// Create handle to driver's device.
	HANDLE DeviceHandle = CreateFileW(
		Core::g_FlashbangDriver.DeviceName,
		GENERIC_WRITE,
		FILE_SHARE_WRITE,
		nullptr,
		OPEN_EXISTING,
		0,
		nullptr);

	// Fail if handle is invalid.
	if (DeviceHandle == (HANDLE)ERROR_INVALID_HANDLE)
	{
		return Serialization::Status::Status_STATUS_UNSUCCESSFUL;
	}

	// Call driver function.
	IO_STATUS_BLOCK IoStatusBlock{};
	NTSTATUS NtStatus = NtDeviceIoControlFile(DeviceHandle, NULL, nullptr, nullptr, &IoStatusBlock, IOCTL_SET_BUFFER, BufferInfo, sizeof(BUFFER_INFO), nullptr, NULL);
	switch (NtStatus)
	{
	case STATUS_SUCCESS:
		Status = Serialization::Status::Status_STATUS_SUCCESS;
		break;
	case STATUS_NOT_FOUND:
		Status = Serialization::Status::Status_STATUS_NOT_FOUND;
		break;
	case STATUS_INSUFFICIENT_RESOURCES:
		Status = Serialization::Status::Status_STATUS_INSUFFICIENT_RESOURCES;
		break;
	default:
		Status = Serialization::Status::Status_STATUS_UNSUCCESSFUL;
		break;
	}

	// Close handle and return.
	CloseHandle(DeviceHandle);
	return Status;
}

Action::Response Action::KmPatchMemory::Apply(FlatBufferBuilder* Builder, const Serialization::KmPatchMemory* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	// Load Flashbang driver if it has not been loaded yet.
	if (!Core::g_FlashbangDriver.Loaded)
	{
		if (Status = DriverLoader::LoadDriver(&Core::g_FlashbangDriver))
		{
			SubBuilder.add_type(Serialization::ActionType::ActionType_KmPatchMemory);
			SubBuilder.add_status((Serialization::Status)Status);
			SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
			Action::Response Response({ Status, SubBuilder.Finish() });
			return Response;
		}
	}

	// Reserialize request into BUFFER_INFO structure.
	BUFFER_INFO BufferInfo{};
	BufferInfo.ModuleName = (PUCHAR)ActionRequest->module_name()->c_str();
	BufferInfo.SearchPattern = (PUCHAR)ActionRequest->search_pattern()->data();
	BufferInfo.SearchPatternLength = ActionRequest->search_pattern()->size();
	BufferInfo.Offset = ActionRequest->offset();
	BufferInfo.OldBytes = (PUCHAR)ActionRequest->old_bytes()->data();
	BufferInfo.OldBytesLength = ActionRequest->old_bytes()->size();
	BufferInfo.NewBytes = (PUCHAR)ActionRequest->new_bytes()->data();
	BufferInfo.NewBytesLength = ActionRequest->new_bytes()->size();

	// Supply a pointer to receive the matched address.
	ULONG64 TargetAddress = 0;
	BufferInfo.TargetAddress = &TargetAddress;

	// Send request to driver.
	Status = SendPatchRequest(&BufferInfo);

	// Create PatchedMemory object to be used for reversion.
	flatbuffers::Offset<Serialization::PatchedMemory> PatchedMemoryOffset = Serialization::CreatePatchedMemory(
		*Builder,
		4,  // PID is constant for system process (4).
		TargetAddress,
		Builder->CreateVector(BufferInfo.OldBytes, BufferInfo.OldBytesLength),
		Builder->CreateVector(BufferInfo.NewBytes, BufferInfo.NewBytesLength));

	SubBuilder.add_type(Serialization::ActionType::ActionType_KmPatchMemory);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	SubBuilder.add_patched_memory(PatchedMemoryOffset);
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Action::Response Action::KmPatchMemory::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	// Reserialize response into BUFFER_INFO structure.
	ULONG64 TargetAddress = ActionResponse->patched_memory()->address();
	BUFFER_INFO BufferInfo{};
	BufferInfo.TargetAddress = &TargetAddress;
	BufferInfo.OldBytes = (PUCHAR)ActionResponse->patched_memory()->new_bytes()->data(); // Swapped to revert.
	BufferInfo.OldBytesLength = ActionResponse->patched_memory()->new_bytes()->size();
	BufferInfo.NewBytes = (PUCHAR)ActionResponse->patched_memory()->old_bytes()->data();
	BufferInfo.NewBytesLength = ActionResponse->patched_memory()->old_bytes()->size();

	// Send request to driver.
	Status = SendPatchRequest(&BufferInfo);

	SubBuilder.add_type(Serialization::ActionType::ActionType_KmPatchMemory);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}