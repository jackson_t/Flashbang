#include "pch.h"

#include "luajit/lua.hpp"

#include "../Action.h"
#include "../Utility.h"

#pragma comment(lib, "Version.lib")

using namespace flatbuffers;
using namespace Flashbang::Action;

Response UmCheckFileVersion::Apply(FlatBufferBuilder* Builder, const Serialization::UmCheckFileVersion* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	// Expand environment variables (if any).
	CHAR ExpandedFilePath[MAX_PATH]{};
	PCCH FilePath(ActionRequest->file_path()->c_str());
	ExpandEnvironmentStringsA(FilePath, ExpandedFilePath, MAX_PATH);

	DWORD SizeHandle;
	DWORD FileVersionInfoSize = GetFileVersionInfoSizeA(ExpandedFilePath, &SizeHandle);

	if (FileVersionInfoSize)
	{
		PVOID FileVersionInfoData = (PVOID)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, FileVersionInfoSize);
		if (FileVersionInfoData)
		{
			if (GetFileVersionInfoA(ExpandedFilePath, NULL, FileVersionInfoSize, FileVersionInfoData))
			{
				VS_FIXEDFILEINFO* VersionInfo = { 0 };
				FileVersionInfoSize = sizeof(VS_FIXEDFILEINFO);
				if (VerQueryValueW(FileVersionInfoData, L"\\", (LPVOID*)&VersionInfo, (PUINT)&FileVersionInfoSize))
				{
					char ActualFileVersion[MAX_PATH] = { 0 };
					StringCchPrintfA(
						ActualFileVersion,
						MAX_PATH,
						"%d.%d.%d.%d",
						VersionInfo->dwFileVersionMS >> 16,
						VersionInfo->dwFileVersionMS & 0xFFFF,
						VersionInfo->dwFileVersionLS >> 16,
						VersionInfo->dwFileVersionLS & 0xFFFF);

					auto SupportedVersions = ActionRequest->supported_versions();
					for (auto ExpectedFileVersion : *SupportedVersions)
					{
						if (std::string(ExpectedFileVersion->c_str()) == std::string(ActualFileVersion))
						{
							Status = Serialization::Status_STATUS_SUCCESS;
							break;
						}
						else
						{
							Status = Serialization::Status_STATUS_UNSUCCESSFUL;
						}
					}
				}
				else
				{
					Status = Serialization::Status_STATUS_UNSUCCESSFUL;
				}
			}
			else
			{
				Status = Serialization::Status_STATUS_UNSUCCESSFUL;
			}
		}
		else
		{
			Status = Serialization::Status_STATUS_UNSUCCESSFUL;
		}
	}
	else
	{
		Status = Serialization::Status_STATUS_UNSUCCESSFUL;
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmCheckFileVersion);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Response UmCheckFileVersion::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmCheckFileVersion);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}