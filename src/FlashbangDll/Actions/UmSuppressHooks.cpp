#include "pch.h"

#include <Psapi.h>
#include <Shlwapi.h>
#include <filesystem>

#include "boost/algorithm/string.hpp"

#include "../Action.h"
#include "../Utility.h"

using namespace boost::algorithm;
using namespace flatbuffers;
using namespace Flashbang::Action;

// Struct:    HOOKED_FUNCTION
// Describes: A hooked function.
// Members:
// - ModuleHandle: Handle to the function's module.
// - Ordinal:      The ordinal number of the function.
// - Address:      The address of the function.
// - Name:         Name of the function, if it exists.
typedef struct _HOOKED_FUNCTION {
	HMODULE ModuleHandle = 0;
	DWORD   Ordinal = 0;
	LPVOID  Address = nullptr;
	WCHAR   Name[MAX_PATH] = { 0 };
	UCHAR   OriginalBytes[16] = { 0 };
	UCHAR   ModifiedBytes[16] = { 0 };
} HOOKED_FUNCTION, *PHOOKED_FUNCTION;

// Struct:    LOADED_MODULE
// Describes: A loaded module.
// Members:
// - Handle:          Handle to the function's module.
// - Path:            File path of module.
// - HookedFunctions: Array of hooked functions.
typedef struct _LOADED_MODULE {
	HMODULE Handle = 0;
	WCHAR Path[MAX_PATH] = { 0 };
	std::vector<PHOOKED_FUNCTION> HookedFunctions;
} LOADED_MODULE, *PLOADED_MODULE;

// Function:    GetModules
// Description: Returns a list of all loaded modules for this process.
// Called from: CheckAllModulesForHooks
std::vector<PLOADED_MODULE> GetModules()
{
	std::vector<PLOADED_MODULE> Modules;

	DWORD RequredBytes = 0;
	DWORD ModuleCount = 0;
	HMODULE* ModuleHandles = (HMODULE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(HMODULE) * 1024);
	DWORD ModuleHandlesSize = sizeof(HMODULE) * 1024;
	BOOL Status = EnumProcessModulesEx(GetCurrentProcess(), ModuleHandles, ModuleHandlesSize, &RequredBytes, LIST_MODULES_DEFAULT);

	// Fail silently if the call was not successful or enough memory wasn't allocated.
	if ((Status == FALSE) || (RequredBytes > ModuleHandlesSize))
	{
		return Modules;
	}

	ModuleCount = RequredBytes / sizeof(HMODULE);
	for (unsigned int i = 0; i < ModuleCount; i++)
	{
		PLOADED_MODULE Module = new LOADED_MODULE;

		Module->Handle = ModuleHandles[i];
		GetModuleFileNameExW(GetCurrentProcess(), Module->Handle, Module->Path, MAX_PATH);

		Modules.push_back(Module);
	}

	HeapFree(GetProcessHeap(), NULL, ModuleHandles);
	return Modules;
}

// Function:    CheckModuleForHooks
// Description: Checks a given module for hooked functions by comparing against a fresh copy.
// Called from: CheckAllModulesForHooks
// Remarks:     Adapted from https://github.com/NtRaiseHardError/Antimalware-Research.
VOID CheckModuleForHooks(PLOADED_MODULE Module)
{
	// Load a fresh copy in memory.
	HANDLE FmFileHandle = CreateFileW(Module->Path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	HANDLE FmMappingHandle = CreateFileMappingW(FmFileHandle, NULL, PAGE_READONLY | SEC_IMAGE, 0, 0, NULL);
	HMODULE FmHandle = (HMODULE)MapViewOfFile(FmMappingHandle, FILE_MAP_READ, 0, 0, 0);
	HMODULE LmHandle = Module->Handle;

	// Parse the original module's PE headers.
	PIMAGE_DOS_HEADER LmDosHeader = (PIMAGE_DOS_HEADER)LmHandle;
	PIMAGE_NT_HEADERS LmNtHeaders = (PIMAGE_NT_HEADERS)((DWORD_PTR)LmHandle + LmDosHeader->e_lfanew);

	// Parse the fresh module's PE headers.
	PIMAGE_DOS_HEADER FmDosHeader = (PIMAGE_DOS_HEADER)FmHandle;
	PIMAGE_NT_HEADERS FmNtHeaders = (PIMAGE_NT_HEADERS)((DWORD_PTR)FmHandle + FmDosHeader->e_lfanew);

	// Check if the export table exists.
	if (LmNtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress != 0) {
		// Get the export table for the loaded module.
		PIMAGE_EXPORT_DIRECTORY LmExportDirectory = (PIMAGE_EXPORT_DIRECTORY)((DWORD_PTR)LmHandle + LmNtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
		LPDWORD LmFunctionTable = (LPDWORD)((DWORD_PTR)LmHandle + LmExportDirectory->AddressOfFunctions);
		LPDWORD LmNameTable = (LPDWORD)((DWORD_PTR)LmHandle + LmExportDirectory->AddressOfNames);
		LPWORD LmOrdinalTable = (LPWORD)((DWORD_PTR)LmHandle + LmExportDirectory->AddressOfNameOrdinals);

		// Get the export table for the fresh module.
		PIMAGE_EXPORT_DIRECTORY FmExportDirectory = (PIMAGE_EXPORT_DIRECTORY)((DWORD_PTR)FmHandle + FmNtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
		LPDWORD FmFunctionTable = (LPDWORD)((DWORD_PTR)FmHandle + FmExportDirectory->AddressOfFunctions);
		LPDWORD FmNameTable = (LPDWORD)((DWORD_PTR)FmHandle + FmExportDirectory->AddressOfNames);
		LPWORD FmOrdinalTable = (LPWORD)((DWORD_PTR)FmHandle + FmExportDirectory->AddressOfNameOrdinals);

		// Walk the export table.
		for (DWORD i = 0; i < LmExportDirectory->NumberOfNames; i++) {
			// Get the address of the export (loaded + fresh).
			FARPROC LmFunction = (FARPROC)((DWORD_PTR)LmHandle + LmFunctionTable[LmOrdinalTable[i]]);
			FARPROC FmFunction = (FARPROC)((DWORD_PTR)FmHandle + FmFunctionTable[FmOrdinalTable[i]]);

			// Check if the address of the loaded export is executable. Skip if not.
			MEMORY_BASIC_INFORMATION mbi = { 0 };
			VirtualQuery(LmFunction, &mbi, sizeof(mbi));
			if ((mbi.Protect & PAGE_EXECUTE_READ) == 0)
				continue;

			// Check if the function is hooked by comparing memory between the loaded module and the fresh copy.
			if (memcmp(LmFunction, FmFunction, 16))
			{
				PHOOKED_FUNCTION HookedFunction = new HOOKED_FUNCTION;

				HookedFunction->ModuleHandle = Module->Handle;
				HookedFunction->Ordinal = LmOrdinalTable[i];
				HookedFunction->Address = LmFunction;
				MultiByteToWideChar(CP_UTF8, MB_PRECOMPOSED, (LPCCH)((DWORD_PTR)LmHandle + LmNameTable[i]), -1, (LPWSTR)HookedFunction->Name, MAX_PATH - 1);
				CopyMemory(HookedFunction->OriginalBytes, FmFunction, 16);
				CopyMemory(HookedFunction->ModifiedBytes, LmFunction, 16);

				Module->HookedFunctions.push_back(HookedFunction);
			}
		}
	}

	// Unmap fresh module.
	UnmapViewOfFile(FmHandle);
	CloseHandle(FmMappingHandle);
	CloseHandle(FmFileHandle);
}

// Function:    CheckAllModulesForHooks
// Description: Returns a list of all loaded modules with any hooks.
// Called from: Apply
std::vector<PLOADED_MODULE> CheckAllModulesForHooks()
{
	// Get all modules.
	std::vector<PLOADED_MODULE> Modules = GetModules();

	// Check each module for hooks.
	for (PLOADED_MODULE Module : Modules)
		CheckModuleForHooks(Module);

	return Modules;
}

// Function:    PatchFunction
// Description: Patches a function to original or modified version.
// Called from: Apply
BOOL PatchFunction(LPVOID Address, UCHAR Bytes[16])
{
	DWORD OldProtection = 0;

	if (!VirtualProtect(Address, 16, PAGE_EXECUTE_READWRITE, &OldProtection))
		return FALSE;

	CopyMemory(Address, Bytes, 16);

	if (!VirtualProtect(Address, 16, OldProtection, &OldProtection))
		return FALSE;

	return TRUE;
}

Response UmSuppressHooks::Apply(FlatBufferBuilder* Builder, const Serialization::UmSuppressHooks* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	std::vector<PLOADED_MODULE> LoadedModules = CheckAllModulesForHooks();
	std::vector<flatbuffers::Offset<Serialization::SuppressedFunction>> SuppressedFunctions;
	for (PLOADED_MODULE Module : LoadedModules)
	{
		for (PHOOKED_FUNCTION Function : Module->HookedFunctions)
		{
			std::string ModulePath = Utility::WStringToString(std::wstring(Module->Path));
			std::string ModuleName = std::filesystem::path(ModulePath).filename().string();
			std::string FunctionName = Utility::WStringToString(std::wstring(Function->Name));

			if (ActionRequest->functions()->size())
			{
				for (unsigned int i = 0; i < ActionRequest->functions()->size(); i++)
				{
					std::string AllowlistedName = std::string(ActionRequest->functions()->Get(i)->c_str());

					if ((istarts_with(AllowlistedName, ModuleName)) && (iends_with(AllowlistedName, FunctionName)))
					{
						if (PatchFunction(Function->Address, Function->OriginalBytes))
						{
							SuppressedFunctions.push_back(Serialization::CreateSuppressedFunction(*Builder,
								Builder->CreateString(ModulePath.c_str()),
								Builder->CreateString(FunctionName.c_str()),
								(ULONG64)Function->Address,
								Builder->CreateVector(Function->OriginalBytes, 16),
								Builder->CreateVector(Function->ModifiedBytes, 16)));
						}
					}
				}
			}
			else
			{				
				if (PatchFunction(Function->Address, Function->OriginalBytes))
				{
					SuppressedFunctions.push_back(Serialization::CreateSuppressedFunction(*Builder,
						Builder->CreateString(ModulePath.c_str()),
						Builder->CreateString(FunctionName.c_str()),
						(ULONG64)Function->Address,
						Builder->CreateVector(Function->OriginalBytes, 16),
						Builder->CreateVector(Function->ModifiedBytes, 16)));
				}
			}
		}
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSuppressHooks);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	SubBuilder.add_suppressed_functions(Builder->CreateVector(SuppressedFunctions));
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Response UmSuppressHooks::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	const Vector<Offset<Serialization::SuppressedFunction>>* HookedFunctions = ActionResponse->suppressed_functions();
	for (uoffset_t i = 0; i < HookedFunctions->size(); i++)
	{
		// Is the array copy happening in the right order? Is there a more terse way to write this?
		PVOID Address = (PVOID)HookedFunctions->Get(i)->function_addr();
		UCHAR ModifiedBytes[16] = { 0 };
		for (uoffset_t j = 0; j < 16; j++) ModifiedBytes[j] = HookedFunctions->Get(i)->modified_bytes()->Get(j);

		if (!PatchFunction(Address, ModifiedBytes))
			Status = Serialization::Status_STATUS_UNSUCCESSFUL;
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_UmSuppressHooks);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}