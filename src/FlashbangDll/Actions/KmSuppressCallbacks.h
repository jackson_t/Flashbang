#pragma once

#include "../../FlashbangDriver/Common.h"

#define CTL_CODE(DeviceType, Function, Method, Access) (((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method))
#define METHOD_BUFFERED 0
#define FILE_ANY_ACCESS 0

// Struct:    CALLBACK_ENTRY
// Describes: A kernel-mode callback.
// Members:
// - Type:          The type of callback.
// - Address:       The address of the callback.
// - ModuleName:    The name of the module hosting the callback.
// - ModuleOffset:  The offset of the callback from the module's base address.
// - OriginalQword: The first 8 bytes of the callback.
// - Suppressed:    Whether the callback has been suppressed.
// - Notable:       Whether the callback should be highlighted.
typedef struct _CALLBACK_ENTRY
{
	CALLBACK_TYPE Type = CALLBACK_TYPE::Unknown;
	PVOID         Address = nullptr;
	WCHAR         ModuleName[MAX_PATH] = { 0 };
	SIZE_T        ModuleOffset = 0;
	ULONG64       OriginalQword = 0;
} CALLBACK_ENTRY, *PCALLBACK_ENTRY;

HANDLE GetDeviceHandle();
PMODULE_INFO GetCallbackModule(PMODULE_INFO Modules, PVOID CallbackAddress);
ULONG64 GetQword(PVOID Address);
BOOL SetQword(PVOID Address, ULONG64 Value);
BYTE GetParentType(CALLBACK_TYPE CallbackType);
ULONG64 GetSuppressionValue(CALLBACK_TYPE CallbackType);
std::vector<PCALLBACK_ENTRY> GetCallbacks();
BOOL SuppressCallback(PCALLBACK_ENTRY Callback);
BOOL RevertCallback(PCALLBACK_ENTRY Callback);
BOOL RevertCallback(PCALLBACK_ENTRY Callback);