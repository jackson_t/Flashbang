#include "pch.h"

#include <string>
#include "flatbuffers/flatbuffers.h"

#include "Messaging.h"
#include "Serialization.h"
#include "Core.h"
#include "Dispatcher.h"
#include "Utility.h"

using namespace Flashbang;
using namespace Flashbang::Serialization;

DWORD Messaging::GetPseudorandomClassName(IN DWORD Seed, OUT LPWSTR GuidString)
{
	struct GUID_BUFFER {
		DWORD Data1;
		DWORD Data2;
		DWORD Data3;
		DWORD Data4;
	};

	srand(Seed + SALT);

	GUID_BUFFER GuidBuffer{};
	GuidBuffer.Data1 = rand() * rand();
	GuidBuffer.Data2 = rand() * rand();
	GuidBuffer.Data3 = rand() * rand();
	GuidBuffer.Data4 = rand() * rand();

	GUID* Guid = (GUID*)&GuidBuffer;
	return StringFromGUID2(*Guid, (LPOLESTR)GuidString, 64);
}

LRESULT CALLBACK Messaging::MainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PCOPYDATASTRUCT RequestCopyData = (PCOPYDATASTRUCT)lParam;

	switch (message)
	{
	case WM_COPYDATA:
	{
		if (RequestCopyData->dwData == (ULONG_PTR)HANDLER_TYPE::FlashbangRequest)
		{
			// Process message.
			PVOID ResponseBuffer = nullptr;
			SIZE_T ResponseLength = 0;
			Dispatcher::HandleMessage(RequestCopyData->lpData, RequestCopyData->cbData, &ResponseBuffer, &ResponseLength);

			// Get class name and window handle of target.
			WCHAR GuidString[64]{};
			DWORD Seed = (DWORD)wParam + (DWORD)HANDLER_TYPE::FlashbangResponse; // Seed = Sender PID + Handler Type
			GetPseudorandomClassName(Seed, (LPWSTR)&GuidString);
			LPCWSTR ClassName = (LPCWSTR)&GuidString;
			HWND MessageWindow = FindWindowW(ClassName, nullptr);

			// Construct and send the message.
			if (MessageWindow)
			{
				COPYDATASTRUCT ResponseCopyData = { 0 };
				ResponseCopyData.dwData = (ULONG_PTR)HANDLER_TYPE::FlashbangResponse;
				ResponseCopyData.cbData = (DWORD)ResponseLength;
				ResponseCopyData.lpData = ResponseBuffer;
				SendMessage(MessageWindow, WM_COPYDATA, (WPARAM)GetProcessId(GetCurrentProcess()), (LPARAM)&ResponseCopyData);
			}

			HeapFree(GetProcessHeap(), HEAP_ZERO_MEMORY, ResponseBuffer);
		}

		break;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

	return 0;
}

void Messaging::RegisterHandler()
{
	WCHAR GuidString[64]{};
	DWORD Seed = GetProcessId(GetCurrentProcess()) + (DWORD)HANDLER_TYPE::FlashbangRequest;
	GetPseudorandomClassName(Seed, (LPWSTR)&GuidString);
	LPCWSTR ClassName = (LPCWSTR)&GuidString;

	WNDCLASSEX WindowClass = { 0 };
	WindowClass.cbSize = sizeof(WNDCLASSEX);
	WindowClass.lpfnWndProc = MainWndProc;
	WindowClass.hInstance = 0;
	WindowClass.lpszClassName = ClassName;

	RegisterClassExW(&WindowClass);
	CreateWindowExW(0, ClassName, ClassName, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);
}
