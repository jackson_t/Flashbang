#pragma once

#include <map>
#include "Messaging.h"
#include "DriverLoader.h"

using namespace std;
using namespace flatbuffers;

namespace Flashbang
{
	class __declspec(dllexport) Core
	{
		Flashbang::Messaging FbMessaging;

	public:
		Core();

		static DWORD g_State;
		static map<DWORD, BOOL>* g_ThreadStatuses;
		static PVOID g_LastMessage;
		static Driver g_VulnerableDriver;
		static Driver g_FlashbangDriver;
		static DriverSettings* g_DriverSettings;
		static Vector<Offset<ActionRequest>>* g_ActionRequests;
		static Vector<Offset<ActionResponse>>* g_ActionResponses;
	};
}