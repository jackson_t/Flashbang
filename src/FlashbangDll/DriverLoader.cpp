#include "pch.h"
#include <VersionHelpers.h>
#include <strsafe.h>
#include <ShlObj.h>
#include <filesystem>

#include "Core.h"
#include "DriverLoader.h"
#include "Utility.h"
#include "Syscalls.h"

using namespace flatbuffers;
using namespace Flashbang;
using namespace Flashbang::Serialization;

/// <summary>
/// Validates DriverSettings object for compatibility with target environment.
/// </summary>
/// <remarks>
/// This should be called before LoadDriver() is called.
/// </remarks>
/// <param name="Settings"></param>
/// <returns></returns>
DWORD DriverLoader::ValidateSettings(const DriverSettings* Settings)
{
	// Bit-flipping for random hashes only works on Windows 10+.
	if (Settings->use_random_hash())
	{
		PBYTE PEB = (PBYTE)__readgsqword(0x60);
		DWORD OsMajorVersion = (DWORD)(*(PEB + 0x118));
		if (OsMajorVersion < 10)
			return Status::Status_STATUS_INCOMPATIBLE_OS_VERSION;
	}

	// Cannot choose an existing service that has already started.
	const char* ServiceName = Settings->service_name()->c_str();
	DWORD ServiceStatus = IsServiceRegisteredAndStarted(ServiceName);
	switch (ServiceStatus)
	{
	case (DWORD)Status::Status_STATUS_SERVICE_NOT_DRIVER:
	case (DWORD)Status::Status_STATUS_SERVICE_NOT_STOPPED:
	case (DWORD)Status::Status_STATUS_UNSUCCESSFUL:
		return ServiceStatus;
	default:
		break;
	}

	// RegistryImagePath, if supplied, must start with a '\'.
	if (Settings->registry_image_path()->size())
	{
		if (Settings->registry_image_path()->c_str()[0] != '\\')
		{
			return Status::Status_STATUS_OBJECT_PATH_SYNTAX_BAD;
		}
	}

	return Status::Status_STATUS_SUCCESS;
}

/// <summary>
/// Loads a specified driver as per configuration defined in DriverSettings.
/// </summary>
/// <param name="TargetDriver">Pointer to Flashbang::Driver object.</param>
/// <returns>
/// <para>- STATUS_SUCCESS if successful.</para>
/// <para>- STATUS_BUFFER_TOO_SMALL if driver buffer not in memory.</para>
/// <para>- STATUS_UNSUCCESSFUL if a Windows API function raised an unhandled error.</para>
/// </returns>
DWORD DriverLoader::LoadDriver(Driver* TargetDriver)
{
	// Return success if driver is already loaded.
	if (TargetDriver->Loaded)
	{
		return Status::Status_STATUS_SUCCESS;
	}
	
	// Fail fast if the driver buffer is not stored in memory.
	if ((!TargetDriver->Length) || (!TargetDriver->Buffer))
	{
		return Status::Status_STATUS_BUFFER_TOO_SMALL;
	}

	// Get the status of the target service.
	const char* ServiceName = Core::g_DriverSettings->service_name()->c_str();
	DWORD ServiceStatus = IsServiceRegisteredAndStarted(ServiceName);

	// Declare variables used later on.
	const char* RegistryImagePath = nullptr;
	TargetDriver->DestinationPath = (PSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MAX_PATH);
	PSTR RegistryImagePathWin32 = (PSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MAX_PATH);
	LPQUERY_SERVICE_CONFIGA ServiceConfig = nullptr;

	// Modify the checksum of the driver PE if use_random_hash is enabled.
	if (Core::g_DriverSettings->use_random_hash())
	{
		IMAGE_DOS_HEADER* DriverDosHeader = (IMAGE_DOS_HEADER*)TargetDriver->Buffer;
		IMAGE_NT_HEADERS* DriverNtHeaders = (IMAGE_NT_HEADERS*)((ULONG64)DriverDosHeader + DriverDosHeader->e_lfanew);
		DriverNtHeaders->OptionalHeader.CheckSum = rand(); // Previously seeded in Messaging::GetPseudorandomClassName.
	}

	// Process next steps based on loading method (FILE, SMB, WEBDAV).
	switch (Core::g_DriverSettings->loading_method())
	{
	case DriverLoadingMethod::DriverLoadingMethod_FILE:
	{
		// Get destination path (if specified), otherwise set it to a temporary path.
		if (Core::g_DriverSettings->destination_path()->size())
		{
			ExpandEnvironmentStringsA(Core::g_DriverSettings->destination_path()->c_str(), TargetDriver->DestinationPath, MAX_PATH);
			std::filesystem::path ParentPath = std::filesystem::path(std::string(TargetDriver->DestinationPath)).parent_path();
			
			// Create parent directories if they do not exist.
			if (!std::filesystem::exists(ParentPath))
			{
				if (DWORD DirectoryStatus = SHCreateDirectoryExA(NULL, ParentPath.string().c_str(), NULL))
				{
					switch (DirectoryStatus)
					{
					case ERROR_CANCELLED:
					case ERROR_FILE_EXISTS:
					case ERROR_ALREADY_EXISTS:
						break;
					default:
						return Status::Status_STATUS_UNSUCCESSFUL;
					}
				}
			}
		}
		else
		{
			CHAR TempPath[MAX_PATH]{};
			CHAR TempFileName[MAX_PATH]{};
			GetTempPathA(MAX_PATH, (LPSTR)&TempPath);
			GetTempFileNameA((LPCSTR)&TempPath, ServiceName, 0, (LPSTR)TargetDriver->DestinationPath);
		}

		// Drop the file to disk.
		DWORD BytesWritten = 0;
		if (HANDLE DriverFile = CreateFileA(TargetDriver->DestinationPath, GENERIC_WRITE, NULL, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL))
		{
			WriteFile(DriverFile, TargetDriver->Buffer, TargetDriver->Length, &BytesWritten, nullptr);
			CloseHandle(DriverFile);
		}

		break;
	}
	default:
		break;
	}

	// Create symbolic link if RegistryImagePath is defined, otherwise set it to the DestinationPath.
	if (Core::g_DriverSettings->registry_image_path()->size())
	{
		RegistryImagePath = Core::g_DriverSettings->registry_image_path()->c_str();

		std::string RegistryImagePathString = std::string(AY_OBFUSCATE(R"(\\.\GlobalRoot)")) + std::string(RegistryImagePath);
		RegistryImagePathString.copy(RegistryImagePathWin32, RegistryImagePathString.size(), 0);

		if (TargetDriver->DestinationPath)
		{
			if (!CreateSymbolicLinkA(RegistryImagePathWin32, TargetDriver->DestinationPath, NULL))
			{
				DeleteFileA(TargetDriver->DestinationPath);
				return Status::Status_STATUS_UNSUCCESSFUL; // ToDo (more descriptive status code).
			}
		}
	}
	else
	{
		RegistryImagePath = TargetDriver->DestinationPath;
	}

	// Set the image path for the service (new or existing).
	if (ServiceStatus == Status::Status_STATUS_SERVICE_FOUND)
	{
		// Modify existing service.
		if (ModifyDriverService(ServiceName, RegistryImagePath, &ServiceConfig))
		{
			// Delete driver file and/or symbolic link before returning.
			if (RegistryImagePathWin32) DeleteFileA(RegistryImagePathWin32);
			if (TargetDriver->DestinationPath) DeleteFileA(TargetDriver->DestinationPath);
			return Status::Status_STATUS_UNSUCCESSFUL;
		}
	}
	else if (ServiceStatus == Status::Status_STATUS_SERVICE_NOT_FOUND)
	{
		// Create a new service, then delete it after loading.
		if (CreateDriverService(ServiceName, RegistryImagePath))
		{
			// Delete driver file and/or symbolic link before returning.
			if (RegistryImagePathWin32) DeleteFileA(RegistryImagePathWin32);
			if (TargetDriver->DestinationPath) DeleteFileA(TargetDriver->DestinationPath);
			return Status::Status_STATUS_UNSUCCESSFUL;
		}
	}

	// Start driver service.
	if (ToggleService(ServiceName, TRUE))
	{
		return Status::Status_STATUS_UNSUCCESSFUL;
	}
	else
	{
		// Remove the symbolic link, if it was created.
		if (RegistryImagePathWin32)
		{
			DeleteFileA(RegistryImagePathWin32);
		}

		// If the target service was modified, restore the original image path.
		if ((ServiceStatus == Status::Status_STATUS_SERVICE_FOUND) && (ServiceConfig))
		{
			ModifyDriverService(ServiceName, ServiceConfig->lpBinaryPathName, nullptr);
			HeapFree(GetProcessHeap(), NULL, ServiceConfig);
		}
		// If the target service was just created, mark it for deletion.
		else if (ServiceStatus == Status::Status_STATUS_SERVICE_NOT_FOUND)
		{
			DeleteDriverService(ServiceName);
		}
	}

	// Get device name from Flashbang driver.
	if (TargetDriver->Type == FlashbangDriver)
	{
		if (GetFlashbangDeviceName())
		{
			return Status::Status_STATUS_UNSUCCESSFUL;
		}
	}

	// Set flag to true upon successful driver load.
	TargetDriver->Loaded = TRUE;

	return Status::Status_STATUS_SUCCESS;
}

/// <summary>
/// Unloads and deletes a specified driver.
/// </summary>
/// <param name="TargetDriver">Pointer to Flashbang::Driver object.</param>
/// <returns>
/// <para>- STATUS_SUCCESS if successful.</para>
/// <para>- STATUS_BUFFER_TOO_SMALL if driver buffer not in memory.</para>
/// <para>- STATUS_UNSUCCESSFUL if a Windows API function raised an unhandled error.</para>
/// </returns>
DWORD DriverLoader::UnloadDriver(Driver* TargetDriver)
{
	DWORD Status = Status::Status_STATUS_SUCCESS;

	// Get the status of the target service.
	const char* ServiceName = Core::g_DriverSettings->service_name()->c_str();
	DWORD ServiceStatus = IsServiceRegisteredAndStarted(ServiceName);

	// Stop the service.
	if (ServiceStatus == Status::Status_STATUS_SERVICE_NOT_STOPPED)
	{
		if (ToggleService(ServiceName, FALSE))
		{
			Status = Status::Status_STATUS_UNSUCCESSFUL;
		}
		else
		{
			Status = Status::Status_STATUS_SUCCESS;
		}
	}
	
	// Delete the driver file.
	if (TargetDriver->DestinationPath)
	{
		DeleteFileA(TargetDriver->DestinationPath);
	}

	return Status;
}

/// <summary>
/// Checks if the given service has already been registered.
/// </summary>
/// <param name="ServiceName">Name of the target service.</param>
/// <returns>
/// <para>- STATUS_SERVICE_NOT_FOUND if the service does not exist.</para>
/// <para>- STATUS_SERVICE_NOT_DRIVER if the service exists but is not a driver.</para>
/// <para>- STATUS_SERVICE_NOT_STOPPED if the service exists and has already started.</para>
/// <para>- STATUS_SERVICE_FOUND if the service exists and has *not* started.</para>
/// <para>- STATUS_UNSUCCESSFUL if a Windows API function raised an unhandled error.</para>
/// </returns>
DWORD DriverLoader::IsServiceRegisteredAndStarted(const char* ServiceName)
{
	DWORD Status = Status::Status_STATUS_UNSUCCESSFUL;

	if (SC_HANDLE ScmHandle = OpenSCManagerW(nullptr, nullptr, GENERIC_READ))
	{
		if (SC_HANDLE SvcHandle = OpenServiceA(ScmHandle, ServiceName, SERVICE_QUERY_STATUS))
		{
			SERVICE_STATUS ServiceStatus{};
			if (QueryServiceStatus(SvcHandle, &ServiceStatus))
			{
				if (ServiceStatus.dwServiceType != SERVICE_KERNEL_DRIVER)
				{
					Status = Status::Status_STATUS_SERVICE_NOT_DRIVER;
				}
				else if (ServiceStatus.dwCurrentState != SERVICE_STOPPED)
				{
					Status = Status::Status_STATUS_SERVICE_NOT_STOPPED;
				}
				else
				{
					Status = Status::Status_STATUS_SERVICE_FOUND;
				}
			}
			else
			{
				Status = Status::Status_STATUS_UNSUCCESSFUL;
			}

			CloseHandle(SvcHandle);
		}
		else
		{
			if (GetLastError() == ERROR_SERVICE_DOES_NOT_EXIST)
			{
				Status = Status::Status_STATUS_SERVICE_NOT_FOUND;
			}
			else
			{
				Status = Status::Status_STATUS_UNSUCCESSFUL;
			}
		}

		CloseHandle(ScmHandle);
	}
	else
	{
		Status = Status::Status_STATUS_UNSUCCESSFUL;
	}

	return Status;
}

/// <summary>
/// Creates a service for a specified driver.
/// </summary>
/// <param name="ServiceName">Name of the service</param>
/// <param name="DriverPath">Path of the driver file</param>
/// <returns>
/// <para>- STATUS_SUCCESS if service was created.</para>
/// <para>- STATUS_UNSUCCESSFUL if service was not created.</para>
/// </returns>
DWORD DriverLoader::CreateDriverService(const char* ServiceName, const char* DriverPath)
{
	DWORD Status = Status::Status_STATUS_UNSUCCESSFUL;

	if (SC_HANDLE ScmHandle = OpenSCManagerW(nullptr, nullptr, SC_MANAGER_CREATE_SERVICE))
	{
		if (SC_HANDLE SvcHandle = CreateServiceA(ScmHandle, ServiceName, ServiceName, SERVICE_ALL_ACCESS, SERVICE_KERNEL_DRIVER, SERVICE_DEMAND_START, SERVICE_ERROR_IGNORE, DriverPath, nullptr, nullptr, nullptr, nullptr, nullptr))
		{
			CloseHandle(SvcHandle);
			Status = Status::Status_STATUS_SUCCESS;
		}
		else
		{
			Status = Status::Status_STATUS_UNSUCCESSFUL;
		}

		CloseHandle(ScmHandle);
	}
	else
	{
		Status = Status::Status_STATUS_UNSUCCESSFUL;
	}

	return Status;
}

/// <summary>
/// Modifies an existing service to point to a specified driver.
/// </summary>
/// <param name="ServiceName">Name of the service</param>
/// <param name="DriverPath">Path of the new driver file</param>
/// <param name="ServiceConfig">Service config containing original image path (can be null)</param>
/// <returns>
/// <para>- STATUS_SUCCESS if service was modified.</para>
/// <para>- STATUS_UNSUCCESSFUL if service was not modified.</para>
/// </returns>
DWORD DriverLoader::ModifyDriverService(const char* ServiceName, const char* DriverPath, OUT LPQUERY_SERVICE_CONFIGA* ServiceConfig)
{
	DWORD Status = Status::Status_STATUS_UNSUCCESSFUL;

	if (SC_HANDLE ScmHandle = OpenSCManagerA(nullptr, nullptr, SC_MANAGER_ALL_ACCESS))
	{
		if (SC_HANDLE SvcHandle = OpenServiceA(ScmHandle, ServiceName, SERVICE_QUERY_CONFIG | SERVICE_CHANGE_CONFIG))
		{
			// Retrieve ServiceConfig object containing old image path, so the service can be reverted back after.
			if (ServiceConfig)
			{
				DWORD BytesNeeded = 0;
				if (!QueryServiceConfigA(SvcHandle, nullptr, 0, &BytesNeeded))
				{
					*ServiceConfig = (LPQUERY_SERVICE_CONFIGA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, BytesNeeded);
					if (!QueryServiceConfigA(SvcHandle, *ServiceConfig, BytesNeeded, &BytesNeeded))
					{
						CloseHandle(SvcHandle);
						CloseHandle(ScmHandle);
						return Status::Status_STATUS_UNSUCCESSFUL;
					}
				}
			}

			// Change service config to use new driver path.
			if (ChangeServiceConfigA(SvcHandle, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE, DriverPath, NULL, NULL, NULL, NULL, NULL, NULL))
			{
				Status = Status::Status_STATUS_SUCCESS;
			}
			else
			{
				CloseHandle(SvcHandle);
				CloseHandle(ScmHandle);
				return Status::Status_STATUS_UNSUCCESSFUL;
			}

			CloseHandle(SvcHandle);
		}
		else
		{
			Status = Status::Status_STATUS_UNSUCCESSFUL;
		}

		CloseHandle(ScmHandle);
	}
	else
	{
		Status = Status::Status_STATUS_UNSUCCESSFUL;
	}

	return Status;
}

/// <summary>
/// Deletes an existing service.
/// </summary>
/// <param name="ServiceName">Name of the service</param>
/// <returns>
/// <para>- STATUS_SUCCESS if service was deleted.</para>
/// <para>- STATUS_UNSUCCESSFUL if service was not deleted.</para>
/// </returns>
DWORD DriverLoader::DeleteDriverService(const char* ServiceName)
{
	DWORD Status = Status::Status_STATUS_UNSUCCESSFUL;

	if (SC_HANDLE ScmHandle = OpenSCManagerA(nullptr, nullptr, DELETE))
	{
		if (SC_HANDLE SvcHandle = OpenServiceA(ScmHandle, ServiceName, DELETE))
		{
			if (DeleteService(SvcHandle))
			{
				Status = Status::Status_STATUS_SUCCESS;
			}
			else
			{
				Status = Status::Status_STATUS_UNSUCCESSFUL;
			}

			CloseHandle(SvcHandle);
		}
		else
		{
			Status = Status::Status_STATUS_UNSUCCESSFUL;
		}

		CloseHandle(ScmHandle);
	}
	else
	{
		Status = Status::Status_STATUS_UNSUCCESSFUL;
	}

	return Status;
}

/// <summary>
/// Starts or stops a service.
/// </summary>
/// <param name="ServiceName">Name of the service</param>
/// <param name="ShouldEnable">Desired state (TRUE to start, FALSE to stop)</param>
/// <returns>
/// <para>- STATUS_SUCCESS if service state was toggled.</para>
/// <para>- STATUS_UNSUCCESSFUL if service state was not toggled.</para>
/// </returns>
DWORD DriverLoader::ToggleService(const char* ServiceName, bool ShouldEnable)
{
	DWORD Status = Status::Status_STATUS_UNSUCCESSFUL;

	if (SC_HANDLE ScmHandle = OpenSCManagerW(nullptr, nullptr, SC_MANAGER_ALL_ACCESS))
	{
		if (SC_HANDLE SvcHandle = OpenServiceA(ScmHandle, ServiceName, SERVICE_START | SERVICE_STOP))
		{
			if (ShouldEnable)
			{
				if (StartServiceW(SvcHandle, NULL, nullptr))
				{
					Status = Status::Status_STATUS_SUCCESS;
				}
				else
				{
					Status = Status::Status_STATUS_UNSUCCESSFUL;
				}
			}
			else
			{
				SERVICE_STATUS ServiceStatus{};
				if (ControlService(SvcHandle, SERVICE_CONTROL_STOP, &ServiceStatus))
				{
					Status = Status::Status_STATUS_SUCCESS;
				}
				else
				{
					Status = Status::Status_STATUS_UNSUCCESSFUL;
				}
			}

			CloseHandle(SvcHandle);
		}
		else
		{
			Status = Status::Status_STATUS_UNSUCCESSFUL;
		}

		CloseHandle(ScmHandle);
	}
	else
	{
		Status = Status::Status_STATUS_UNSUCCESSFUL;
	}

	return Status;
}

/// <summary>
/// Get the device name of the Flashbang driver so it can be used for future calls.
/// </summary>
/// <returns>
/// <para>- STATUS_SUCCESS if device name was retrieved.</para>
/// <para>- STATUS_UNSUCCESSFUL if device name was not retrieved.</para>
/// </returns>
DWORD DriverLoader::GetFlashbangDeviceName()
{
	DWORD Status = Status::Status_STATUS_UNSUCCESSFUL;

	HANDLE SessionHandle{};
	UNICODE_STRING SessionName{};
	SessionName.Buffer = (PWSTR)ObfuscateStringW(R"(\KernelObjects\Session0)");
	SessionName.Length = 46;
	SessionName.MaximumLength = SessionName.Length + 2;
	OBJECT_ATTRIBUTES ObjectAttributes{};
	InitializeObjectAttributes(&ObjectAttributes, &SessionName, 0, NULL, NULL);
	
	if (!NtOpenSession(&SessionHandle, 2, &ObjectAttributes))
	{
		DWORD DeviceNameLength = 0;
		DeviceNameRequest Request{ DRIVER_TAG, nullptr, &DeviceNameLength };

		// Get the size of the device name first.
		if (!NtNotifyChangeSession(SessionHandle, 0, nullptr, IoSessionEventCreated, IoSessionStateDisconnected, IoSessionStateDisconnected, (PVOID)&Request, sizeof(DeviceNameRequest)))
		{
			Request.DeviceName = (PWSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, DeviceNameLength);

			// Get the actual device name now that its buffer has been allocated.
			if (!NtNotifyChangeSession(SessionHandle, 0, nullptr, IoSessionEventCreated, IoSessionStateDisconnected, IoSessionStateDisconnected, (PVOID)&Request, sizeof(DeviceNameRequest)))
			{
				if (Core::g_FlashbangDriver.DeviceName = (PWSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MAX_PATH))
				{
					std::wstring DeviceNameWin32 = std::wstring(ObfuscateStringW(R"(\\.\GlobalRoot)")) + std::wstring(Request.DeviceName);
					DeviceNameWin32.copy(Core::g_FlashbangDriver.DeviceName, DeviceNameWin32.size(), 0);
					Core::g_FlashbangDriver.DeviceName[DeviceNameWin32.size()] = '\0';
					Status = Status::Status_STATUS_SUCCESS;
				}
				else
				{
					Status = Status::Status_STATUS_UNSUCCESSFUL;
				}
			}
			else
			{
				Status = Status::Status_STATUS_UNSUCCESSFUL;
			}
		}
		else
		{
			Status = Status::Status_STATUS_UNSUCCESSFUL;
		}

		CloseHandle(SessionHandle);
	}
	else
	{
		Status = Status::Status_STATUS_UNSUCCESSFUL;
	}

	return Status;
}