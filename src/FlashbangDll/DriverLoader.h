#pragma once

#include "Serialization.h"
#include "../FlashbangDriver/Common.h"

using namespace flatbuffers;
using namespace Flashbang::Serialization;

namespace Flashbang
{
	enum DriverType {
		FlashbangDriver,
		VulnerableDriver
	};

	struct Driver
	{
		DriverType      Type;
		DriverSettings* Settings;
		PVOID           Buffer = nullptr;
		DWORD           Length = 0;
		BOOL            Loaded = FALSE;
		PWSTR           DeviceName = nullptr;
		PSTR            DestinationPath = nullptr;
	};

	struct DeviceNameRequest
	{
		ULONG  Magic;
		PWSTR  DeviceName;
		PULONG DeviceNameLength;
	};

	class DriverLoader
	{
	public:
		static DWORD ValidateSettings(const DriverSettings* Settings);
		static DWORD LoadDriver(Driver* TargetDriver);
		static DWORD UnloadDriver(Driver* TargetDriver);
	private:
		static DWORD IsServiceRegisteredAndStarted(const char* ServiceName);
		static DWORD CreateDriverService(const char* ServiceName, const char* DriverPath);
		static DWORD ModifyDriverService(const char* ServiceName, const char* DriverPath, OUT LPQUERY_SERVICE_CONFIGA* ServiceConfig);
		static DWORD DeleteDriverService(const char* ServiceName);
		static DWORD ToggleService(const char* ServiceName, bool ShouldEnable);
		static DWORD GetFlashbangDeviceName();
	};
}