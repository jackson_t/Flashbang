#pragma once

#include <Shlwapi.h>

#include "Serialization.h"

#define SALT 'FBCN'

using namespace Flashbang::Serialization;

namespace Flashbang
{
	class Messaging
	{
	public:
		void RegisterHandler();
	private:
		static DWORD GetPseudorandomClassName(IN DWORD Seed, OUT LPWSTR GuidString);
		static LRESULT CALLBACK MainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	
	enum class HANDLER_TYPE
	{
		FlashbangRequest = 0,
		FlashbangResponse
	};
	};
}