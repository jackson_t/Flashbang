.code

EXTERN SW2_GetSyscallNumber: PROC

NtOpenProcessToken PROC
	push rcx                   ; Save registers.
	push rdx
	push r8
	push r9
	mov ecx, 0C79AC50Ah        ; Load function hash into ECX.
	call SW2_GetSyscallNumber  ; Resolve function hash into syscall number.
	pop r9                     ; Restore registers.
	pop r8
	pop rdx
	pop rcx
	mov r10, rcx
	syscall                    ; Invoke system call.
	ret
NtOpenProcessToken ENDP

NtAdjustPrivilegesToken PROC
	push rcx                   ; Save registers.
	push rdx
	push r8
	push r9
	mov ecx, 0EBCEF546h        ; Load function hash into ECX.
	call SW2_GetSyscallNumber  ; Resolve function hash into syscall number.
	pop r9                     ; Restore registers.
	pop r8
	pop rdx
	pop rcx
	mov r10, rcx
	syscall                    ; Invoke system call.
	ret
NtAdjustPrivilegesToken ENDP

NtDeviceIoControlFile PROC
	push rcx                   ; Save registers.
	push rdx
	push r8
	push r9
	mov ecx, 0B2B9DA2Bh        ; Load function hash into ECX.
	call SW2_GetSyscallNumber  ; Resolve function hash into syscall number.
	pop r9                     ; Restore registers.
	pop r8
	pop rdx
	pop rcx
	mov r10, rcx
	syscall                    ; Invoke system call.
	ret
NtDeviceIoControlFile ENDP

NtOpenSession PROC
	push rcx                   ; Save registers.
	push rdx
	push r8
	push r9
	mov ecx, 009A50D32h        ; Load function hash into ECX.
	call SW2_GetSyscallNumber  ; Resolve function hash into syscall number.
	pop r9                     ; Restore registers.
	pop r8
	pop rdx
	pop rcx
	mov r10, rcx
	syscall                    ; Invoke system call.
	ret
NtOpenSession ENDP

NtNotifyChangeSession PROC
	push rcx                   ; Save registers.
	push rdx
	push r8
	push r9
	mov ecx, 03B935B00h        ; Load function hash into ECX.
	call SW2_GetSyscallNumber  ; Resolve function hash into syscall number.
	pop r9                     ; Restore registers.
	pop r8
	pop rdx
	pop rcx
	mov r10, rcx
	syscall                    ; Invoke system call.
	ret
NtNotifyChangeSession ENDP

end