#include "pch.h"

#include "Core.h"
#include "Messaging.h"

using namespace Flashbang;

DWORD Core::g_State = 0;
Driver Core::g_FlashbangDriver{ FlashbangDriver, nullptr, nullptr, 0, FALSE, nullptr, nullptr };
Driver Core::g_VulnerableDriver{ VulnerableDriver, nullptr, nullptr, 0, FALSE, nullptr, nullptr };
map<DWORD, BOOL>* Core::g_ThreadStatuses = new std::map<DWORD, BOOL>();

/// <summary>
/// Initializes Flashbang module.
/// </summary>
Core::Core()
{
    FbMessaging.RegisterHandler();
    Core::g_ThreadStatuses->insert(std::make_pair(GetCurrentThreadId(), TRUE));
}