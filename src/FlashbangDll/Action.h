#pragma once

#include "Serialization.h"

using namespace flatbuffers;

namespace Flashbang
{
	namespace Action
	{
		struct Response
		{
			unsigned long Status;
			Offset<Serialization::ActionResponse> Offset;
		};

		class KmSuppressCallbacks
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::KmSuppressCallbacks* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmSuppressHooks
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmSuppressHooks* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmSuppressEtw
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmSuppressEtw* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmSuppressFilter
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmSuppressFilter* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmRunCommand
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmRunCommand* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmRunLua
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmRunLua* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class KmPatchMemory
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::KmPatchMemory* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmCheckFileVersion
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmCheckFileVersion* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmBlockConnection
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmBlockConnection* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};

		class UmSetSystemTime
		{
		public:
			static Response Apply(FlatBufferBuilder* Builder, const Serialization::UmSetSystemTime* ActionRequest);
			static Response Revert(FlatBufferBuilder* Builder, const Serialization::ActionResponse* ActionResponse);
		};
	}
}