#include "pch.h"

#include "Utility.h"
#include "Syscalls.h"

using namespace Flashbang;

__int64 Utility::GetSystemTimeAsInt64()
{
    FILETIME ft;
    GetSystemTimeAsFileTime(&ft);

    LARGE_INTEGER li;
    li.LowPart = ft.dwLowDateTime;
    li.HighPart = ft.dwHighDateTime;

    return li.QuadPart;
}

// Function:    IsProcessElevated
// Description: Returns true if process is running elevated or is running as SYSTEM.
bool Utility::IsProcessElevated()
{
	HANDLE TokenHandle;
	DWORD ReturnLength;
	TOKEN_ELEVATION_TYPE ElevationType;
	OpenProcessToken(GetCurrentProcess(), TOKEN_READ, &TokenHandle);
	GetTokenInformation(TokenHandle, TokenElevationType, &ElevationType, sizeof(TOKEN_ELEVATION_TYPE), &ReturnLength);

	if (ElevationType == TokenElevationTypeFull)
		return TRUE;

	BOOL IsSystem = FALSE;
	PSID SystemSid = nullptr;
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	if (AllocateAndInitializeSid(&NtAuthority, 1, SECURITY_LOCAL_SYSTEM_RID, 0, 0, 0, 0, 0, 0, 0, &SystemSid))
		if (CheckTokenMembership(NULL, SystemSid, &IsSystem))
			if (IsSystem)
				return TRUE;

	return FALSE;
}

bool Utility::TogglePrivilege(DWORD Privilege, BOOL ShouldEnable)
{
	DWORD Status = 0;
	HANDLE TokenHandle;
	TOKEN_PRIVILEGES TokenPrivileges;

	if (NtOpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &TokenHandle))
		return FALSE;

	TokenPrivileges.PrivilegeCount = 1;
	TokenPrivileges.Privileges[0].Luid.LowPart = Privilege;
	TokenPrivileges.Privileges[0].Luid.HighPart = 0L;
	TokenPrivileges.Privileges[0].Attributes = (ShouldEnable) ? SE_PRIVILEGE_ENABLED : NULL;

	if (Status = NtAdjustPrivilegesToken(TokenHandle, FALSE, &TokenPrivileges, sizeof(TOKEN_PRIVILEGES), (PTOKEN_PRIVILEGES)NULL, NULL))
		return FALSE;

	CloseHandle(TokenHandle);

	return TRUE;
}

unsigned long Utility::GetPidForProcessName(const char* ProcessName)
{
	PROCESSENTRY32 ProcessEntry;
	ProcessEntry.dwSize = sizeof(PROCESSENTRY32);
	std::wstring ProcessNameW = Utility::StringToWString(std::string(ProcessName));

	// Take a snapshot of all processes in the system.
	HANDLE SnapshotHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == SnapshotHandle)
	{
		return 0;
	}

	// Retrieve information about the first process, and exit if unsuccessful.
	if (!Process32First(SnapshotHandle, &ProcessEntry))
	{
		CloseHandle(SnapshotHandle);
		return 0;
	}

	do
	{
		if (!wcscmp(ProcessEntry.szExeFile, ProcessNameW.c_str()))
		{
			return ProcessEntry.th32ProcessID;
		}
	} while (Process32Next(SnapshotHandle, &ProcessEntry));

	CloseHandle(SnapshotHandle);
	return 0;
}

bool Utility::IsProcessNameMatchedWithPid(unsigned long ProcessId, const char* ProcessName)
{
	PROCESSENTRY32 ProcessEntry;
	ProcessEntry.dwSize = sizeof(PROCESSENTRY32);
	std::wstring ProcessNameW = Utility::StringToWString(std::string(ProcessName));

	// Take a snapshot of all processes in the system.
	HANDLE SnapshotHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == SnapshotHandle)
	{
		return FALSE;
	}

	// Retrieve information about the first process, and exit if unsuccessful.
	if (!Process32First(SnapshotHandle, &ProcessEntry))
	{
		CloseHandle(SnapshotHandle);
		return FALSE;
	}

	do
	{
		if (!wcscmp(ProcessEntry.szExeFile, ProcessNameW.c_str()))
		{
			if (ProcessEntry.th32ProcessID == ProcessId)
			{
				return TRUE;
			}
		}
	} while (Process32Next(SnapshotHandle, &ProcessEntry));

	CloseHandle(SnapshotHandle);
	return FALSE;
}

std::string Utility::WStringToString(const std::wstring& WString)
{
	if (WString.empty())
	{
		return std::string();
	}
	else
	{
		int SizeNeeded = WideCharToMultiByte(CP_UTF8, 0, &WString[0], (int)WString.size(), NULL, 0, NULL, NULL);
		std::string OutputString(SizeNeeded, 0);
		WideCharToMultiByte(CP_UTF8, 0, &WString[0], (int)WString.size(), &OutputString[0], SizeNeeded, NULL, NULL);
		return OutputString;
	}
}

std::wstring Utility::StringToWString(const std::string& String)
{
	if (String.empty())
	{
		return std::wstring();
	}
	else
	{
		int SizeNeeded = MultiByteToWideChar(CP_UTF8, 0, &String[0], (int)String.size(), NULL, 0);
		std::wstring OutputString(SizeNeeded, 0);
		MultiByteToWideChar(CP_UTF8, 0, &String[0], (int)String.size(), &OutputString[0], SizeNeeded);
		return OutputString;
	}
}