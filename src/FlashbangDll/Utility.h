#pragma once

#include <string>
#include <TlHelp32.h>
#include "Obfuscate.h"

#define PopupCurrentLine MessageBoxA(NULL, (std::string(__FUNCTION__) + std::string(": ") + std::to_string(__LINE__)).c_str(), std::string(__FILE__).c_str(), MB_OK);
#define ObfuscateString(s) AY_OBFUSCATE(s)
#define ObfuscateStringW(s) Flashbang::Utility::StringToWString(std::string(ObfuscateString(s))).c_str()

typedef ULONG NTSTATUS;

namespace Flashbang
{
	class Utility
	{
	public:
		static __int64 GetSystemTimeAsInt64();
		static bool IsProcessElevated();
		static bool TogglePrivilege(DWORD Privilege, BOOL ShouldEnable);
		static unsigned long GetPidForProcessName(const char* ProcessName);
		static bool IsProcessNameMatchedWithPid(unsigned long ProcessId, const char* ProcessName);
		static std::string WStringToString(const std::wstring& WString);
		static std::wstring StringToWString(const std::string& String);
	};
}