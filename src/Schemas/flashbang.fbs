namespace Flashbang.Serialization;

enum Status:ulong {
  // NTSTATUS values.
  STATUS_SUCCESS = 0x00000000,
  STATUS_UNSUCCESSFUL = 0xC0000001,
  STATUS_BUFFER_TOO_SMALL = 0xC0000023,
  STATUS_OBJECT_PATH_SYNTAX_BAD = 0xC000003B,
  STATUS_PRIVILEGE_NOT_HELD = 0xC0000061,
  STATUS_INSUFFICIENT_RESOURCES = 0xC000009A,
  STATUS_MEMORY_NOT_ALLOCATED = 0xC00000A0,
  STATUS_IMAGE_ALREADY_LOADED = 0xC000010E,
  STATUS_NOT_FOUND = 0xC0000225,
  STATUS_INVALID_IMAGE_WIN_32 = 0xC0000359,
  STATUS_REQUEST_OUT_OF_SEQUENCE = 0xC000042A,
  STATUS_FLT_DO_NOT_DETACH = 0xC01C0010,
  STATUS_FLT_FILTER_NOT_FOUND = 0xC01C0013,
  
  // Custom values.
  STATUS_INCOMPATIBLE_OS_VERSION = 0xC4000001,
  STATUS_SERVICE_FOUND = 0xC4000002,
  STATUS_SERVICE_NOT_FOUND = 0xC4000003,
  STATUS_SERVICE_NOT_DRIVER = 0xC4000004,
  STATUS_SERVICE_NOT_STOPPED = 0xC4000005
}

enum MessageType:byte {
  GET_HEARTBEAT,
  LOAD_CAPABILITY,
  LOAD_BLINDLET,
  UNLOAD_BLINDLET,
  APPLY_BLINDLET,
  REVERT_BLINDLET,
  UNLOAD_CAPABILITY
}

table Message {
  type:MessageType;                  // The type of message to send/receive.
  status:Status;                     // Status code for receiving.
  timestamp:uint64;                  // Message timestamp (FILETIME).
  general_settings:GeneralSettings;  // Settings for loading the capability (used for LOAD_CAPABILITY messages).
  driver_settings:DriverSettings;    // Settings for loading the driver (used for LOAD_BLINDLET messages).
  action_requests:[ActionRequest];   // List of action requests (used for LOAD_BLINDLET messages).
  action_responses:[ActionResponse]; // List of action responses (used for APPLY_BLINDLET messages).
}

table GeneralSettings {
  use_debug_mode:bool;       // Enable debug mode.
  vulnerable_driver:[ubyte]; // Buffer for a vulnerable driver to load (only required if the Flashbang driver is not signed).
  flashbang_driver:[ubyte];  // Buffer for the Flashbang driver used for kernel-mode (Km*) actions.
}

enum DriverLoadingMethod:byte {
  FILE,
  SMB,
  WEBDAV
}

table DriverSettings {
  loading_method:DriverLoadingMethod;
  service_name:string;
  destination_path:string;
  registry_image_path:string;
  use_random_hash:bool;
  use_leading_dot:bool;
  onload_lua_callback:string;
}

enum ActionType:byte {
  KmSuppressCallbacks, // Suppress kernel-mode callbacks.
  UmSuppressHooks,     // Suppress user-mode function hooks for the host process.
  UmSuppressEtw,       // Suppress ETW sessions or providers.
  UmSuppressFilter,    // Suppress a filter driver by unloading it via Filter Manager.
  UmRunCommand,        // Run a command.
  UmRunPowershell,     // Run PowerShell code.
  UmRunLua,            // Run Lua code.
  UmRunShellcode,      // Run shellcode from user-mode.
  KmRunShellcode,      // Run shellcode from kernel-mode.
  UmPatchMemory,       // Patch memory from user-mode.
  KmPatchMemory,       // Patch memory from kernel-mode.
  UmCheckFileVersion,  // Check file version against allowlist; exit sequence upon failure.
  UmBlockConnection,   // Block network connections.
  UmSetSystemTime      // Set system time.
}

union ActionRequests {
  KmSuppressCallbacks, // Suppress kernel-mode callbacks.
  UmSuppressHooks,     // Suppress user-mode function hooks for the host process.
  UmSuppressEtw,       // Suppress ETW sessions or providers.
  UmSuppressFilter,    // Suppress a filter driver by unloading it via Filter Manager.
  UmRunCommand,        // Run a command.
  UmRunPowershell,     // Run PowerShell code.
  UmRunLua,            // Run Lua code.
  UmRunShellcode,      // Run shellcode from user-mode.
  KmRunShellcode,      // Run shellcode from kernel-mode.
  UmPatchMemory,       // Patch memory from user-mode.
  KmPatchMemory,       // Patch memory from kernel-mode.
  UmCheckFileVersion,  // Check file version against allowlist; exit sequence upon failure.
  UmBlockConnection,   // Block network connections.
  UmSetSystemTime      // Set system time.
}

table ActionRequest {
  request:ActionRequests;
  should_abort_on_error:bool;
}

table UmCheckFileVersion {
  file_path:string;
  supported_versions:[string];
}

table UmBlockConnection {
  process_id:uint32;
  process_name:string;
  remote_host:string;
  remote_port:uint32;
  max_attempts:uint32;
}

enum TimeType:byte {
  ABSOLUTE_TIME,
  RELATIVE_TIME,
  BOOT_TIME
}

table UmSetSystemTime {
  type:TimeType;
  value:int64;
}

enum CallbackType:byte {
  IMAGE,
  PROCESS,
  THREAD,
  REGISTRY,
  OBJECT,
  FILE
}

table KmSuppressCallbacks {
  module_name:string;
  types:[CallbackType];
}

table SuppressedCallback {
  type:CallbackType;
  module_name:string;
  module_offset:uint64;
  callback_address:uint64;
  original_qword:uint64;
}

// UmSuppressHooks

table UmSuppressHooks {
  functions:[string];
}

table SuppressedFunction {
  module_path:string;
  function_name:string;
  function_addr:uint64;
  original_bytes:[ubyte];
  modified_bytes:[ubyte];
}

table UmSuppressEtw {
  session:string;
  providers:[string];
}

table UmSuppressFilter {
  filter_name:string;
  filter_altitude:uint32;
}

table SuppressedSession {
  session_name:string;
  provider_guids:[string];
}

table UmRunCommand {
  command:string;
}

table UmRunPowershell {
  code:string;
}

table UmRunLua {
  code:string;
}

table UmRunShellcode {
  shellcode:string;
}

table KmRunShellcode {
  shellcode:string;
}

table UmPatchMemory {
  process_name:string;
  module_name:string;
  search_pattern:[ubyte];
  offset:int32;
  old_bytes:[ubyte];
  new_bytes:[ubyte];
}

table KmPatchMemory {
  module_name:string;
  search_pattern:[ubyte];
  offset:int32;
  old_bytes:[ubyte];
  new_bytes:[ubyte];
}

table PatchedMemory {
  process_id:uint32;
  address:uint64;
  old_bytes:[ubyte];
  new_bytes:[ubyte];
}

table ActionResponse {
  type:ActionType;
  status:Status;
  timestamp:uint64;
  
  // UmBlockConnection
  thread_id:uint32;
  
  // UmSetSystemTime
  old_system_time:uint64;
  
  // KmSuppressCallbacks
  suppressed_callbacks:[SuppressedCallback];
  
  // UmSuppressHooks
  suppressed_functions:[SuppressedFunction];
  
  // UmSuppressEtw
  suppressed_session:SuppressedSession;
  
  // UmSuppressFilter
  filter_name:string;
  
  // UmPatchMemory, KmPatchMemory
  patched_memory:PatchedMemory;
}

root_type Message;