#pragma once
#include <windows.h>

#define FB_REQUEST 0
#define FB_RESPONSE 1
#define FB_SALT 'FBCN' // Change salt value for better opsec. This makes GUIDs used for IPC less predictable.

// Declare Windows API function prototypes for Dynamic Function Resolution (DFR).
DECLSPEC_IMPORT HANDLE  WINAPI KERNEL32$GetProcessHeap(VOID);
DECLSPEC_IMPORT HANDLE  WINAPI KERNEL32$GetCurrentProcess(VOID);
DECLSPEC_IMPORT DWORD   WINAPI KERNEL32$GetProcessId(HANDLE);
DECLSPEC_IMPORT LPVOID  WINAPI KERNEL32$HeapAlloc(HANDLE, DWORD, SIZE_T);
DECLSPEC_IMPORT BOOL    WINAPI KERNEL32$VirtualFree(LPVOID, SIZE_T, DWORD);
DECLSPEC_IMPORT LRESULT WINAPI USER32$DefWindowProcW(HWND, UINT, WPARAM, LPARAM);
DECLSPEC_IMPORT ATOM    WINAPI USER32$RegisterClassExW(CONST WNDCLASSEXW*);
DECLSPEC_IMPORT HWND    WINAPI USER32$CreateWindowExW(DWORD, LPCWSTR, LPCWSTR, DWORD, INT, INT, INT, INT, HWND, HMENU, HINSTANCE, LPVOID);
DECLSPEC_IMPORT HWND    WINAPI USER32$FindWindowW(LPCWSTR, LPCWSTR);
DECLSPEC_IMPORT LRESULT WINAPI USER32$SendMessageW(HWND, UINT, WPARAM, LPARAM);
DECLSPEC_IMPORT BOOL    WINAPI CRYPT32$CryptBinaryToStringA(CONST BYTE*, DWORD, DWORD, LPSTR, DWORD*);
DECLSPEC_IMPORT INT     WINAPI OLE32$StringFromGUID2(REFGUID, LPOLESTR, INT);

// Declare Flashbang function prototypes.
DWORD            FB_Random(PDWORD Seed);
DWORD            FB_GetPseudorandomClassName(IN DWORD Seed, OUT LPWSTR GuidString);
VOID             FB_ResponseCallback(PVOID ResponseBuffer, DWORD ResponseLength);
LRESULT CALLBACK FB_MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
VOID             FB_RegisterHandler();
VOID             FB_SendBuffer(PVOID Payload, DWORD Length);
PCHAR            FB_GetB64String(PCHAR Buffer, SIZE_T Length);

// Operator hash is used to prevent multiple users from deserializing the same output.
DWORD OperatorHash = 1;

// Modify this function to implement platform-specific handling of Flashbang responses.
VOID FB_ResponseCallback(PVOID ResponseBuffer, DWORD ResponseLength)
{
	// Return raw response as base64-encoded string.
	PCHAR B64String = FB_GetB64String((PCHAR)ResponseBuffer, (SIZE_T)ResponseLength);
	BeaconPrintf(CALLBACK_OUTPUT, "[>] [FB] Raw response (tasked by operator #%d):\r\n%s", OperatorHash, B64String);
	KERNEL32$VirtualFree((LPVOID)B64String, 0, MEM_RELEASE);
}

DWORD FB_Random(PDWORD Seed)
{
	// Generate a random number with the algorithm used by MSVC. This is manually
	// implemented (instead of using srand() and random()) because Cobalt Strike
	// BOFs do not link to MSVC runtime.
	//
	// Reference: https://doxygen.reactos.org/d6/df4/rand_8c_source.html#l00010
	
	*Seed = *Seed * 214013 + 2531011;
	return (*Seed >> 16) & RAND_MAX;
}

DWORD FB_GetPseudorandomClassName(IN DWORD Seed, OUT LPWSTR GuidString)
{
	struct GUID_BUFFER
	{
		DWORD Data1;
		DWORD Data2;
		DWORD Data3;
		DWORD Data4;
	};

	DWORD RandomSeed = Seed + FB_SALT;

	struct GUID_BUFFER GuidBuffer;
	GuidBuffer.Data1 = FB_Random(&RandomSeed) * FB_Random(&RandomSeed);
	GuidBuffer.Data2 = FB_Random(&RandomSeed) * FB_Random(&RandomSeed);
	GuidBuffer.Data3 = FB_Random(&RandomSeed) * FB_Random(&RandomSeed);
	GuidBuffer.Data4 = FB_Random(&RandomSeed) * FB_Random(&RandomSeed);

	GUID* Guid = (GUID*)&GuidBuffer;
	return OLE32$StringFromGUID2((REFGUID)Guid, (LPOLESTR)GuidString, 64);
}

LRESULT CALLBACK FB_MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PCOPYDATASTRUCT CopyData = (PCOPYDATASTRUCT)lParam;

	switch (message)
	{
	case WM_COPYDATA:
	{
		if (CopyData->dwData == (ULONG_PTR)FB_RESPONSE)
		{
			PVOID ResponseBuffer = CopyData->lpData;
			DWORD ResponseLength = CopyData->cbData;

			// Invoke platform-specific callback to handle Flashbang response.
			FB_ResponseCallback(ResponseBuffer, ResponseLength);
		}

		break;
	}
	default:
		// Use default callback if WM_COPYDATA message not provided.
		return USER32$DefWindowProcW(hWnd, message, wParam, lParam);
		break;
	}

	return 0;
}

VOID FB_RegisterHandler()
{
	// Generate ClassName GUID for the handler.
	WCHAR GuidString[64];
	DWORD Seed = KERNEL32$GetProcessId(KERNEL32$GetCurrentProcess()) + (DWORD)FB_RESPONSE;
	FB_GetPseudorandomClassName(Seed, (LPWSTR)&GuidString);
	LPCWSTR ClassName = (LPCWSTR)&GuidString;

	// Populate WindowClass object.
	WNDCLASSEXW WindowClass = { 0 };
	WindowClass.cbSize = sizeof(WNDCLASSEX);
	WindowClass.lpfnWndProc = FB_MessageHandler;
	WindowClass.hInstance = 0;
	WindowClass.lpszClassName = ClassName;

	// Register class and create "window".
	USER32$RegisterClassExW(&WindowClass);
	USER32$CreateWindowExW(0, ClassName, ClassName, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);
}

VOID FB_SendBuffer(PVOID Payload, DWORD Length)
{
	// Get class name and window handle of target.
	WCHAR GuidString[64];
	DWORD Seed = KERNEL32$GetProcessId(KERNEL32$GetCurrentProcess()) + (DWORD)FB_REQUEST; // Seed = Sender PID + Handler Type
	FB_GetPseudorandomClassName(Seed, (LPWSTR)&GuidString);
	LPCWSTR ClassName = (LPCWSTR)&GuidString;
	HWND MessageWindow = USER32$CreateWindowExW(0, ClassName, ClassName, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);

	// Pass the payload and length to the COPYDATASTRUCT used in the SendMessage call.
	COPYDATASTRUCT RequestCopyData = { 0 };
	RequestCopyData.dwData = (DWORD)FB_REQUEST;
	RequestCopyData.cbData = Length;
	RequestCopyData.lpData = Payload;

	// Send the payload.
	USER32$SendMessageW(MessageWindow, WM_COPYDATA, (WPARAM)KERNEL32$GetProcessId(KERNEL32$GetCurrentProcess()), (LPARAM)&RequestCopyData);
}

PCHAR FB_GetB64String(PCHAR Buffer, SIZE_T Length)
{
	// Get the expected encoded length first.
	DWORD EncodedLength = 0;
	CRYPT32$CryptBinaryToStringA((const PBYTE)Buffer, Length, CRYPT_STRING_BASE64, NULL, &EncodedLength);
	
	// Allocate and get the base64-encoded string.
	PCHAR B64String = (PCHAR)KERNEL32$HeapAlloc(KERNEL32$GetProcessHeap(), HEAP_ZERO_MEMORY, EncodedLength * 2);
	CRYPT32$CryptBinaryToStringA((const PBYTE)Buffer, Length, CRYPT_STRING_BASE64, (LPSTR)B64String, &EncodedLength);
	
	// Remove trailing CRLF.
	B64String[EncodedLength - 2] = '\0';
	B64String[EncodedLength - 1] = '\0';
	
	return B64String;
}
