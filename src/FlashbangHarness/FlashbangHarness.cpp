#include <Windows.h>
#include <iostream>
#include <sstream>

#include "../FlashbangDll/Core.h"
#include "FbBinding.h"
#include "Utility.h"

#define MAX_MESSAGE_SIZE 0x100000

DWORD ResponseCount;

void GetSerializedMessageBuffer(const char* MessageType, const char* BlindletPath, OUT PBYTE Message, OUT PDWORD MessageSize)
{
    // Get the path of the Python file.
    CHAR ModuleDirectory[MAX_PATH]{};
    GetModuleFileNameA(NULL, (LPSTR)&ModuleDirectory, MAX_PATH);
    PathRemoveFileSpecA((LPSTR)&ModuleDirectory);

    // Construct the command line.
    std::stringstream CommandLine;
    if (BlindletPath)
    {
        CommandLine << "python \"" << ModuleDirectory << "\\message_serialization.py\" --mode serialize --type " << MessageType << " --input \"" << BlindletPath << "\"";
    }
    else
    {
        const char* DriverPath = "FlashbangDriver.sys";
        CommandLine << "python \"" << ModuleDirectory << "\\message_serialization.py\" --mode serialize --type " << MessageType << " --flashbang-driver \"" << DriverPath << "\"";
    }

    // Create pipes.
    HANDLE PipeReadHandle, PipeWriteHandle;
    SECURITY_ATTRIBUTES SecurityAttributes{};
    SecurityAttributes.nLength = sizeof(SECURITY_ATTRIBUTES);
    SecurityAttributes.bInheritHandle = TRUE;
    SecurityAttributes.lpSecurityDescriptor = NULL;
    CreatePipe(&PipeReadHandle, &PipeWriteHandle, &SecurityAttributes, MAX_MESSAGE_SIZE);
    SetHandleInformation(PipeReadHandle, HANDLE_FLAG_INHERIT, 0);

    // Setup CreateProcess call to use pipes.
    PROCESS_INFORMATION ProcessInformation{};
    STARTUPINFOA StartupInfo{};
    StartupInfo.cb = sizeof(STARTUPINFOA);
    StartupInfo.hStdError = GetStdHandle(STD_ERROR_HANDLE);
    StartupInfo.hStdOutput = PipeWriteHandle;
    StartupInfo.dwFlags |= STARTF_USESTDHANDLES;

    // ToDo: fix current directory path.
    BOOL IsProcessCreated = CreateProcessA(nullptr, (LPSTR)CommandLine.str().c_str(), nullptr, nullptr, true, 0, nullptr, (LPCSTR)&ModuleDirectory, &StartupInfo, &ProcessInformation);
    WaitForSingleObject(ProcessInformation.hProcess, INFINITE);

    // Read response.
    if (!ReadFile(PipeReadHandle, Message, MAX_MESSAGE_SIZE, MessageSize, NULL))
    {
        *Message = 0;
        *MessageSize = 0;
    }

    // Clean up.
    CloseHandle(PipeReadHandle);
    CloseHandle(ProcessInformation.hProcess);
    CloseHandle(ProcessInformation.hThread);
}

void GetDeserializedResponseText(const char* Base64Response)
{
    // Get the path of the Python file.
    CHAR ModuleDirectory[MAX_PATH]{};
    GetModuleFileNameA(NULL, (LPSTR)&ModuleDirectory, MAX_PATH);
    PathRemoveFileSpecA((LPSTR)&ModuleDirectory);

    std::stringstream CommandLine;
    CommandLine << "python \"" << ModuleDirectory << "\\message_serialization.py\" --mode deserialize --input \"" << Base64Response << "\"";

    PROCESS_INFORMATION ProcessInformation{};
    STARTUPINFOA StartupInfo{};
    StartupInfo.cb = sizeof(STARTUPINFOA);
    BOOL IsProcessCreated = CreateProcessA(nullptr, (LPSTR)CommandLine.str().c_str(), nullptr, nullptr, true, 0, nullptr, (LPCSTR)&ModuleDirectory, &StartupInfo, &ProcessInformation);
    WaitForSingleObject(ProcessInformation.hProcess, INFINITE);

    // Clean up.
    CloseHandle(ProcessInformation.hProcess);
    CloseHandle(ProcessInformation.hThread);
}

void SendFbMessage(const char* MessageType, const char* BlindletPath)
{
    DWORD MessageSize = 0;
    PBYTE MessageBuffer = (PBYTE)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MAX_MESSAGE_SIZE);
    GetSerializedMessageBuffer(MessageType, BlindletPath, MessageBuffer, &MessageSize);
    std::cout << "> Sending " << MessageType << " message (" << MessageSize << " bytes)...\n";
    FB_SendBuffer((char*)MessageBuffer, MessageSize);
}

int main()
{
	// Register handler for Flashbang responses.
	FB_RegisterHandler();

	// Load Flashbang.
	//Flashbang::Core FbCore;
    LoadShellcodeFromFile(L"flashbang_core.bin");

	// Check if process is elevated.
	if (!IsProcessElevated())
	{
		MessageBoxW(NULL, L"Process must be elevated to run Flashbang.", L"Flashbang", MB_OK | MB_ICONERROR);
		TerminateProcess(GetCurrentProcess(), 1);
	}

    // Check if Python is on the system.
    if (!IsPythonInstalled())
    {
        MessageBoxW(NULL, L"Could not find Python on the system.", L"Flashbang", MB_OK | MB_ICONERROR);
        TerminateProcess(GetCurrentProcess(), 1);
    }

    // Check if handlers are registered before sending more messages.
	while (ResponseCount < 1)
	{
		SendFbMessage("GET_HEARTBEAT", nullptr);
	}

    SendFbMessage("LOAD_CAPABILITY", nullptr);

	// Load blindlet from file.
    std::cout << "> Select a blindlet file to load...\n";
    CHAR BlindletPath[MAX_PATH] = { 0 };
    OPENFILENAMEA OpenFileName = { 0 };
    OpenFileName.lStructSize = sizeof(OpenFileName);
    OpenFileName.hwndOwner = GetActiveWindow();
    OpenFileName.lpstrFilter = "Blindlet Files (*.yaml)\0*.YAML\0";
    OpenFileName.lpstrFile = BlindletPath;
    OpenFileName.nMaxFile = MAX_PATH;
	if (!GetOpenFileNameA(&OpenFileName))
	{
		MessageBoxW(NULL, L"A blindlet file was not supplied.", L"Flashbang", MB_OK | MB_ICONERROR);
		TerminateProcess(GetCurrentProcess(), 1);
	}

	SendFbMessage("LOAD_BLINDLET", (const char*)&BlindletPath);
	SendFbMessage("APPLY_BLINDLET", nullptr);
	
	MessageBoxW(NULL, L"Press OK to revert and unload blindlet.", L"Flashbang", MB_OK | MB_ICONINFORMATION);
	
	SendFbMessage("REVERT_BLINDLET", nullptr);
	SendFbMessage("UNLOAD_BLINDLET", nullptr);
	SendFbMessage("UNLOAD_CAPABILITY", nullptr);
}