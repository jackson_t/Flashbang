#pragma once
#include <Windows.h>

#include "FlashbangHarness.h"
#include "Utility.h"

#define FB_REQUEST 0
#define FB_RESPONSE 1
#define SALT 'FBCN'

// Declare Flashbang function prototypes.
VOID             FB_ResponseCallback(PVOID ResponseBuffer, DWORD ResponseLength);
DWORD            FB_GetPseudorandomClassName(IN DWORD Seed, OUT LPWSTR GuidString);
LRESULT CALLBACK FB_MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
VOID             FB_RegisterHandler();
VOID             FB_SendBuffer(PVOID Payload, DWORD Length);

// ResponseCount is specific to the FlashbangHarness implementation.
extern DWORD ResponseCount;

// Modify this function to implement platform-specific handling of Flashbang responses.
VOID FB_ResponseCallback(PVOID ResponseBuffer, DWORD ResponseLength)
{
    PCHAR B64String = GetB64String((const unsigned char*)ResponseBuffer, ResponseLength);
    GetDeserializedResponseText(B64String);
    ResponseCount += 1;
}

DWORD FB_GetPseudorandomClassName(IN DWORD Seed, OUT LPWSTR GuidString)
{
    struct GUID_BUFFER
    {
        DWORD Data1;
        DWORD Data2;
        DWORD Data3;
        DWORD Data4;
    };

    srand(Seed + SALT);

    GUID_BUFFER GuidBuffer{};
    GuidBuffer.Data1 = rand() * rand();
    GuidBuffer.Data2 = rand() * rand();
    GuidBuffer.Data3 = rand() * rand();
    GuidBuffer.Data4 = rand() * rand();

    GUID* Guid = (GUID*)&GuidBuffer;
    return StringFromGUID2(*Guid, (LPOLESTR)GuidString, 64);
}

LRESULT CALLBACK FB_MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PCOPYDATASTRUCT CopyData = (PCOPYDATASTRUCT)lParam;

    switch (message)
    {
    case WM_COPYDATA:
    {
        if (CopyData->dwData == (ULONG_PTR)FB_RESPONSE)
        {
            PVOID ResponseBuffer = CopyData->lpData;
            DWORD ResponseLength = CopyData->cbData;

            // Invoke platform-specific callback to handle Flashbang response.
            FB_ResponseCallback(ResponseBuffer, ResponseLength);
        }

        break;
    }
    default:
        // Use default callback if WM_COPYDATA message not provided.
        return DefWindowProc(hWnd, message, wParam, lParam);
        break;
    }

    return 0;
}

VOID FB_RegisterHandler()
{
    // Generate ClassName GUID for the handler.
    WCHAR GuidString[64]{};
    DWORD Seed = GetProcessId(GetCurrentProcess()) + (DWORD)FB_RESPONSE;
    FB_GetPseudorandomClassName(Seed, (LPWSTR)&GuidString);
    LPCWSTR ClassName = (LPCWSTR)&GuidString;

    // Populate WindowClass object.
    WNDCLASSEX WindowClass = { 0 };
    WindowClass.cbSize = sizeof(WNDCLASSEX);
    WindowClass.lpfnWndProc = FB_MessageHandler;
    WindowClass.hInstance = 0;
    WindowClass.lpszClassName = ClassName;

    // Register class and create "window".
    RegisterClassExW(&WindowClass);
    CreateWindowExW(0, ClassName, ClassName, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);
}

VOID FB_SendBuffer(PVOID Payload, DWORD Length)
{
    // Get class name and window handle of target.
    WCHAR GuidString[64]{};
    DWORD Seed = GetProcessId(GetCurrentProcess()) + (DWORD)FB_REQUEST; // Seed = Sender PID + Handler Type
    FB_GetPseudorandomClassName(Seed, (LPWSTR)&GuidString);
    LPCWSTR ClassName = (LPCWSTR)&GuidString;
    HWND MessageWindow = CreateWindowExW(0, ClassName, ClassName, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);

    // Pass the payload and length to the COPYDATASTRUCT used in the SendMessage call.
    COPYDATASTRUCT RequestCopyData = { 0 };
    RequestCopyData.dwData = (DWORD)FB_REQUEST;
    RequestCopyData.cbData = Length;
    RequestCopyData.lpData = Payload;

    // Send the payload.
    SendMessageW(MessageWindow, WM_COPYDATA, (WPARAM)GetProcessId(GetCurrentProcess()), (LPARAM)&RequestCopyData);
}