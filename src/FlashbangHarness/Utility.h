#pragma once
#include <Windows.h>
#include <wincrypt.h>
#include <Shlwapi.h>

#include <iostream>
#include <sstream>

#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "Shlwapi.lib")

#define PopupCurrentLine MessageBoxA(NULL, (std::string(__FUNCTION__) + std::string(": ") + std::to_string(__LINE__)).c_str(), std::string(__FILE__).c_str(), MB_OK);

typedef LPVOID(WINAPI* FnEtwpCreateEtwThread)(LPVOID, LPVOID);

bool IsProcessElevated();
bool IsPythonInstalled();
int SearchMemory(PBYTE Haystack, DWORD HaystackSize, PBYTE Needle, DWORD NeedleSize);
char* GetB64String(const unsigned char* Buffer, size_t Length);
void LoadShellcodeFromFile(LPCWSTR FilePath);