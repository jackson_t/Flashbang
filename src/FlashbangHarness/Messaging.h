#pragma once
#include <Windows.h>
#include <Shlwapi.h>

#pragma comment(lib, "Shlwapi.lib")

#define SALT 'FBCN'
#define MAX_MESSAGE_SIZE 0x100000

extern DWORD ResponseCount;

enum class HANDLER_TYPE
{
	FlashbangRequest = 0,
	FlashbangResponse = 1
};

DWORD GetPseudorandomClassName(IN DWORD Seed, OUT LPWSTR GuidString);
LRESULT CALLBACK MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void RegisterHandler();

void SendBuffer(char* Payload, int Length);

void GetSerializedMessageBuffer(const char* MessageType, const char* BlindletPath, OUT PBYTE Message, OUT PDWORD MessageSize);
void GetDeserializedResponseText(const char* Base64Response);
