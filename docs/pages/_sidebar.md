<style type="text/css">
  hr {
    border: none;
    height: 1px;
    background-color: #eee;
  }
</style>

* [**Introduction**](README.md)

<hr>

- **Operators**
  - [Cobalt Strike Usage](operators/cobalt-strike-usage.md)
  - [<s>Covenant Usage</s> 🚧](operators/covenant-usage.md)
  - [<s>Tweaking Blindlets</s> 🚧](operators/tweaking.md)
- **Developers**
  - [Building Flashbang](developers/building.md)
  - [Blindlet Tutorials](developers/tutorial.md)
  - [Blindlet Reference](developers/reference.md)
  - [OPSEC Considerations](developers/opsec.md)
  - [Custom C2 Integration](developers/integration.md)
  - [Flashbang Internals](developers/internals.md)
- **Defenders**
  - [<s>Tool-specific Artifacts</s> 🚧](defenders/flashbang-artifacts.md)
  - [<s>Technique-specific Artifacts</s> 🚧](defenders/technique-artifacts.md)
  - [Prevention](defenders/prevention.md)

<hr>

- **Blindlets**
  - [<s>OpenEDR</s> 🚧](blindlets/openedr.md)
  - [<s>Sysmon</s> 🚧](blindlets/sysmon.md)
  - [<s>Defender Antivirus</s> 🚧](blindlets/mdav.md)