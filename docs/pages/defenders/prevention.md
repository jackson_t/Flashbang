# Prevention

Microsoft provides some options to interrupt some of the utility Flashbang can provide to offensive teams.

## Hypervisor-protected Code Integrity (HVCI)

?> _TBD_

### References

- Microsoft: [Enable virtualization-based protection of code integrity](https://docs.microsoft.com/en-us/windows/security/threat-protection/device-guard/enable-virtualization-based-protection-of-code-integrity)

## Windows Defender Application Control (WDAC)

?> _TBD: make sure policy is signed._

- https://webapp-wdac-wizard.azurewebsites.net/

![](https://i.imgur.com/bvf0Gh1.png)

### References

- Microsoft: [Windows Defender Application Control and AppLocker Overview](https://docs.microsoft.com/en-us/windows/security/threat-protection/windows-defender-application-control/wdac-and-applocker-overview)

## Credential Guard

?> _TBD_