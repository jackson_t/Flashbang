<style type="text/css">
  h3#practice-1 + p + table > tbody > tr:nth-child(1) {
    background-color: #deffdd77 !important;
  }
  h3#practice-1 + p + table > tbody > tr:nth-child(2) {
    background-color: #deffdd77 !important;
  }
  
  h3#practice-1 + p + table > tbody > tr:nth-child(3) {
    background-color: #fffbdd77 !important;
  }
  
  h3#practice-1 + p + table > tbody > tr:nth-child(4) {
    background-color: #fffbdd77 !important;
  }
  
  h3#practice-1 + p + table > tbody > tr:nth-child(5) {
    background-color: #ffecdd77 !important;
  }
</style>

# Blindlet Tutorials

This page has a few tutorials to help you get started with writing "blindlets", which are the YAML-based configuration files used to suppress security products. Each blindlet executes a sequence of configurable actions.

We're going to use [OpenEDR](https://www.openedr.com/) as a case study. They start easy and go into some advanced scenarios you may want to consider if applicable for the products you're up against. If you are more of a hands-on learner and prefer to dive into it, then feel free to skip the "theory" sections and circle back to them later if needed.

## Basic: Suppressing Telemetry

### Theory

?> _ToDo: introduction of common sources: kernel callbacks, function hooks, ETW, AMSI; implementations in OpenEDR._

<!--
- kernel callbacks, user-mode hooks, etw, amsi
  - how does an edr know i'm creating a process if i'm making direct syscalls?
  - how is an edr knowing i'm using queueuserapc if i'm removing the user-mode hooks
- locations of code where this is implemented in openedr
-->

Flashbang provides key actions you can use to suppress telemetry for the products you're up against:

- [**KmSuppressCallbacks**](developers/reference#KmSuppressCallbacks): Suppress kernel-mode callbacks.
- [**UmSuppressHooks**](developers/reference#UmSuppressHooks): Suppress user-mode function hooks for the host process.
- [**UmSuppressEtw**](developers/reference#UmSuppressEtw): Suppress ETW providers within a given session.
- [**UmSuppressFilter**](developers/reference#UmSuppressFilter): Suppress a filter driver by unloading it via Filter Manager.


### Practice

You can use the [Telemetry Sourcerer](https://github.com/jthuraisamy/TelemetrySourcerer) tool in your lab environment to help you determine how your target product collects telemetry and what to suppress.

#### Kernel-mode Callbacks

In the _Kernel-mode Callbacks_ tab, there are various callbacks for `edrdrv.sys` which is the driver component of OpenEDR.

<p align="center">
  <img src="https://i.imgur.com/aVH90QD.png" />
</p>

To suppress these, you can specify a [KmSuppressCallbacks](developers/reference#KmSuppressCallbacks) action in your blindlet:

```yaml
payload:
  actions:
    - type: KmSuppressCallbacks
      note: Suppress OpenEDR driver callbacks.
      conf:
        module_name: edrdrv.sys
        types: [IMAGE, REGISTRY, OBJECT, FILE] # Maps to the "Collection Type" column in Telemetry Sourcerer.
```

#### User-mode Hooks

In the _User-mode Hooks_ tab, there is a list of 37 API functions with inline hooks. This aligns with the [code here](https://github.com/ComodoSecurity/openedr/blob/main/edrav2/iprj/edrpm/src/winapi.h#L63).

<p align="center">
  <img src="https://i.imgur.com/kb9EBix.png" />
</p>

They can be suppressed using the [UmSuppressHooks](developers/reference#UmSuppressHooks) action, specifying some functions or an empty list to denote all:

```yaml
payload:
  actions:
    - type: UmSuppressHooks
      note: Suppress hooks on some ntdll functions (this process only).
      conf:
        functions:
		  - ntdll.dll!NtCreateSymbolicLinkObject
		  - ntdll.dll!NtWriteVirtualMemory
```

```yaml
payload:
  actions:
    - type: UmSuppressHooks
      note: Suppress all detected API function hooks (this process only).
      conf:
        functions: []
```

!> Flashbang only suppresses function hooks in its own process. Other processes are unaffected.

#### Putting it Together

OpenEDR doesn't appear to use ETW for telemetry, so suppressing the kernel callbacks and hooked functions should be sufficient:

```yaml
payload:
  actions:
    - type: UmSuppressHooks
      note: Suppress all detected API function hooks (this process only).
      conf:
        functions: []
		
    - type: KmSuppressCallbacks
      note: Suppress OpenEDR driver callbacks.
      conf:
        module_name: edrdrv.sys
        types: [IMAGE, REGISTRY, OBJECT, FILE]
```

We can validate whether this will work with _Flashbang Harness_, a standalone tool that can be used for testing scenarios like this:

<video autoplay controls loop width="100%">
  <source src="pages/developers/tutorial-basic_validation.mp4" type="video/mp4">
</video>

In the video above, we have the blindlet file open on the top-right and Telemetry Sourcerer on the bottom-right. When the harness tool is launched, we provide the blindlet file and can see that it was able to unhook the functions listed in the previous screenshot as well as suppress kernel callbacks. We can further validate the latter by refreshing the list in Telemetry Sourcerer. After that, the OK button is pressed to revert the effects of the blindlet.

A configuration like this is usually sufficient for many personal AV products. Though seeing that this is an EDR product, we need to be mindful of any telemetry sent by the product and minimize any footprint created by the tool or techniques.

## Intermediate: Stealthy Driver Loading

### Theory

If any kernel-mode actions are specified in a blindlet (e.g. [KmSuppressCallbacks](developers/reference#KmSuppressCallbacks), [KmPatchMemory](developers/reference#KmPatchMemory)), Flashbang will attempt to load its driver to facilitate execution. Being able to run kernel-mode code to bypass EDRs can be powerful, but the act of loading the driver can be noisy and leave conspicuous traces. It's important to load it as stealthily as possible to avoid getting caught. How much a product can see and how verbose it is in reporting can vary from product to product, so there isn't a one-size-fits-all way to quietly load the driver.

Here is a generic list of relevant events EDR agents may report when drivers are loaded:

| Event Type | Possible Trigger | Observations |
|:-----------|:--------|:-------------|
| SERVICE_CREATE | CmRegisterCallbackEx callback or CreateService hook | Driver services are backed by the registry. The registry must be modified to point to the Flashbang driver. This can be done by creating a new service or by updating an existing service. Service creation is also a persistence technique, so this could look anomalous. |
| SERVICE_UPDATE | CmRegisterCallbackEx callback or ChangeServiceConfig hook | Some products may report service creations but not image path updates. | 
| EXECUTABLE_WRITE | IRP_MJ_WRITE callback or WriteFile hook | To reduce noise, most products will not report every file write but only interesting files (e.g. executables). Some will determine file type by extension, while others may look for magic bytes. |
| MODULE_LOAD | PsSetLoadImageNotifyRoutine callback | Some products may not report this event if the driver is deleted upon loading because the image path cannot be resolved. |
| MODULE_INFO | PsSetLoadImageNotifyRoutine or IRP_MJ_WRITE callback | This event type usually contains hashes and digital signature information. Some products may omit this event or partially report it if the driver is deleted upon loading because file cannot be read. Other products may collect this information prior to driver load, when the file is written to disk. |

Flashbang offers a number of [configuration options](developers/reference#driver-settings) to alter the reporting of these events:

| Option | Short Description |
|:-------|:------------------|
| loading_method | Sets the method for loading the driver (FILE or SMB). |
| service_name | Sets the service name of the driver. Can be a new service or an existing driver service that has not started. |
| destination_path | The absolute path of the driver file. If a local path is specified, the driver will be dropped to disk here. |
| registry_image_path | Sets the service's ImagePath value in the Registry. A symlink will be created if it's not the destination_path. |
| use_random_hash | Modifies the PE checksum so the computed hash will be different each time it drops to disk (Win10 ONLY). |
| use_leading_dot | Uses a leading dot (U+2024) for the filename extension separator instead of a full stop (U+002E). |

?> OPSEC guidance for each of these options is discussed in the [Blindlet Reference](developers/reference#driver-settings) page.

### Practice

To apply this to OpenEDR, you can review the logs stored in `%ProgramData%\edrsvc\log\output_events\*.log` as well as the [taxonomy of event types](https://github.com/ComodoSecurity/openedr/blob/a0d8c7080ddb81243d3d112075a2d5befd121cf8/edrav2/iprj/edrdata/common.src#L19). Below is what we observe when we try to match them against the generic types above:


| Generic Event Type | OpenEDR Event Type | Remarks |
|:-------------------|:--------------------|:--------|
| MODULE_LOAD | N/A | Events are not generated for driver loads. |
| MODULE_INFO | N/A | Events are not generated for module metadata (hashes, signatures, etc.). |
| SERVICE_CREATE | LLE_REGISTRY_KEY_CREATE | Events are generated only if originating from [whitelisted processes](https://github.com/ComodoSecurity/openedr/blob/a0d8c7080ddb81243d3d112075a2d5befd121cf8/edrav2/iprj/edrdata/source_policy.json#L11). |
| SERVICE_UPDATE | LLE_REGISTRY_VALUE_SET | Events are generated only if originating from [whitelisted processes](https://github.com/ComodoSecurity/openedr/blob/a0d8c7080ddb81243d3d112075a2d5befd121cf8/edrav2/iprj/edrdata/source_policy.json#L11). |
| **EXECUTABLE_WRITE** | **LLE_FILE_DATA_WRITE_FULL** | **Events are generated for new file writes; event includes file hash.** |

?> For proprietary products, getting non-console access to event logs and taxonomies can be more challenging. While this is outside the scope of this tutorial, more guidance can be found in this article: [Diverting EDR Telemetry to Private Infrastructure](http://www.jackson-t.ca/edr-reversing-evading-03.html).

At the time of writing, there aren't as many potential incriminating events that can come from OpenEDR. Driver loads and module metadata do not get logged, and the default policy only logs registry changes when they occur from whitelisted processes like PowerShell or the Registry Editor. So we're good there as long as our implant isn't hosted in one of those processes.

The only potential concern here is when the driver file drops to disk which triggers the `LLE_FILE_DATA_WRITE_FULL` event.

```javascript
{
	"baseEventType": 7, // LLE_FILE_DATA_WRITE_FULL
	"baseType": 6,
	"deviceName": "DESKTOP-TESTING",
	"file": 
	{
		"hash": "211810f22bef7ce0ca5fe0f75ed807fde0b3dff2",
		"path": "C:\\Users\\User\\AppData\\Local\\Temp\\TEM6C97.tmp",
		"type": "OTHER"
	},
	"processes":  [ ... ],
	"sessionUser": "User@DESKTOP-TESTING",
	"time": 1606577405853,
}
```

By default, Flashbang will drop the driver to the Temporary Files directory if a `destination_path` value is not specified in the `driver_settings` payload object. File writes like this are common and aren't as suspicious on their own, but the hash could be suspicious if an unmodified version of the Flashbang driver is dropped to disk.

Here are a few workarounds to consider for this scenario:

1. **Dropping the driver file with a random hash.** As pointed out by [@FuzzySec](https://twitter.com/FuzzySec/status/1298704369165180929), the `CheckSum` field in driver PEs is not used when computing the Authenticode hash. So it's possible to modify this field, and the driver will still load&mdash;despite producing a different MD5/SHA hash every time it is dropped to disk. This technique is known to only work on Windows 10 targets and can be enabled by setting `use_random_hash` to true in the driver settings object.

2. **Remote driver loading via SMB.** If your target is on an older OS version (e.g. Windows 7) where `use_random_hash` will not work, and you are also in a position to host the driver remotely, then you can consider this option which will not drop a file to disk, and therefore not produce this event. While this could be a reasonable choice for this EDR, a remote path may look peculiar in other products where module loads are logged&mdash;a reminder that telemetry suppression should be tailored per product or environment.

3. **Exclusion paths in EDR configuration.** Last but not least, many EDR agents have configurable exclusion filters which can help reduce noise, but also provides attackers an opportunity to abuse blind spots. In OpenEDR, the default policy excludes file writes coming from processes named `git.exe`, `GoogleUpdate.exe`, `ngen.exe`, and [more](https://github.com/ComodoSecurity/openedr/blob/a0d8c7080ddb81243d3d112075a2d5befd121cf8/edrav2/iprj/edrdata/source_policy.json#L26). While this is outside the scope of what's configurable in `driver_settings`, it's certainly worth looking into the exclusion paths configured in the EDR you're up against.

> Ultimately, events like this `LLE_FILE_DATA_WRITE_FULL` one might not be a concern in your target environment if relevant detection queries are not implemented, or if defensive analysts are not sufficiently trained.

## Advanced: Manipulating Event Caches

### Theory

Most of the sophisticated EDR products include "tampering detection" features, where certain events produced by the sensor will trigger critical alerts after phoning home. Some products also build logic into the sensors to detect tampering behaviour. When up against situations like these&mdash;where efforts to avoid tampering detection have been unsuccessfully exhausted&mdash;a mitigating step could be to manipulate event caches, so even when suspicious events are inevitably collected, they are not reported.

Here is the gist of how this technique works:

1. Block network connections to the product's server.
2. Execute steps to disable telemetry.
3. Execute script to purge or selectively edit the sensor's event buffer.
4. Resume network connections.

In Flashbang, these additional steps can be implemented using the `UmBlockConnection` and `UmRunLua` actions. The former allows you to specify which process and/or remote hosts should have network connections terminated, as well as for how long. The latter allows you to run arbitrary Lua scripts (with the ability to call Windows API functions) so that event caches can be manipulated.

### Practice

At the time of writing, OpenEDR does not have sensor-based tampering detection and the only potentially suspicious event (`LLE_FILE_DATA_WRITE_FULL`) can be influenced to appear more benign. So let's suspend our disbelief for a moment and assume that this event is suspicious and its generation is inevasible.

?> ToDo.