# Custom C2 Integration

<style type="text/css">
  .markdown-section td > code { background-color: #ebebeb !important; }
</style>

## Requirements

Flashbang should be able to support any C2 platform that satisfies the following requirements:

| Requirement | Why | Cobalt Strike Example |
|:------------|:----|:----------------------|
| Arbitrary code execution within the agent process. | Required to load the module. | C code via BOF. |
| Receive response buffers from modules. | Required to process responses. | `BeaconPrintf()` BOF function. |
| Process response buffers off-target. | Required to process responses. | Local deserialization via Aggressor script. |

## Instructions

The project comes with a header-only library written in C that you can port or adapt to integrate into your own implant. It includes three relevant functions, and the steps below will go over how to use them with examples from the Cobalt Strike implementation:

```c
VOID FB_RegisterHandler();
VOID FB_SendBuffer(PVOID Buffer, DWORD Length);
VOID FB_ResponseCallback(PVOID ResponseBuffer, DWORD ResponseLength);
```

1. Prior to any implementation work, read both the [Implant ⥄ Module Communications](developers/internals?id=implant-⥄-module-communications) and [States and Message Types](developers/internals?id=states-and-message-types) write-ups first, then come back to this page when you're done. These will provide some necessary context on how the implant and module communicate, as well as the order in which messages should be sent to the module.

2. From the implant, call the **FB_RegisterHandler()** function so that the implant can receive messages from Flashbang.

3. After the handler is registered, you can invoke the shellcode from **flashbang_core.bin**. This is the Flashbang DLL converted to shellcode using [Donut](https://github.com/TheWover/donut). It's produced during the build process and can be found in the **dist/Standalone** directory. How the agent receives and executes the shellcode can vary per C2 platform and is left as an exercise for the developer. In the Cobalt Strike implementation, the shellcode is sent via the **bof_pack()** function and it's executed by calling **EtwpCreateEtwThread()**.

4. Modify the **FB_ResponseCallback()** function in the header so that the serialized response buffer can be sent back to the C2 platform. In Cobalt Strike, this is implemented by Base64-encoding the buffer and sending it back to operators via the **BeaconPrintf()** function:

```c
// Modify this function to implement platform-specific handling of Flashbang responses.
VOID FB_ResponseCallback(PVOID ResponseBuffer, DWORD ResponseLength)
{
	// Return raw response as base64-encoded string.
	PCHAR B64String = FB_GetB64String((PCHAR)ResponseBuffer, (SIZE_T)ResponseLength);
	BeaconPrintf(CALLBACK_OUTPUT, "[>] [FB] Raw response:\r\n%s", B64String);
	KERNEL32$VirtualFree((LPVOID)B64String, 0, MEM_RELEASE);
}
```

5. When the response buffer is received, it should be deserialized for human-readable consumption. For OPSEC reasons, this process should be done _off-target_, such as on the team server or locally on the operator's console. Flashbang comes with a couple libraries written in Python and Java that handle (de)serialization. These libraries can be ported to other languages FlatBuffers supports (C#, Go, Rust, etc.). Cobalt Strike uses the Java library, and the Base64-encoded response buffer from the beacon is passed to the **deserializeMessage()** function. This returns output for the Aggressor script to print on operator consoles.

6. The last part to implement is sending messages from the implant to the Flashbang module using the **FB_SendBuffer()** function. This function will accept a serialized buffer and its length. There are different [message types](developers/internals?id=states-and-message-types) that can be sent, and each type is tied to a state which must be executed in a particular order. Messages should be serialized off-target (e.g. on team server or operator console).<br><br>In Cobalt Strike, the Aggressor script serializes messages using the Java library, then sends them to the beacon using the **bof_pack()** function. The BOF unpacks each buffer, and uses the **FB_SendBuffer()** function to send them over to Flashbang. To simplify user input, the details of the state machine are abstracted away from operators.<br><br>In post-exploitation contexts, messages are serialized on-demand. Flashbang can also be integrated into stage 0 payloads (droppers or loaders) where messages are pre-serialized and executed in order. There are different ways to embed those buffers and this is left as an exercise for the developer.