<style type="text/css">
  code.lang-example {
    margin: -2em 0em 0em 0em !important;
  }
</style>

# Building Flashbang

Building Flashbang requires several build tools to be installed, including the Visual C++ build tools, both the Windows SDK and WDK, vcpkg with project dependencies installed, and more. So to simplify the build process, a Docker build image is created and used to ensure that the build process is consistent between developers.

## Build Steps

1. Install [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/).
2. Switch to Windows containers (via tray icon context menu).
3. Enter the lines below in a Command Prompt window (not PowerShell):

```batch
git clone https://github.com/jthuraisamy/Flashbang.git
cd Flashbang\
docker build -t flashbang-buildtools:latest -m 2GB build\
:: Building the Docker image takes 30-45 min; consider taking a coffee break.
docker run --rm -v "%CD%:C:\Flashbang" flashbang-buildtools "C:\Flashbang\build\build.ps1"
```

?> The image building process requires an Internet connection; ensure that firewall rules are set appropriately.

## Expected Output

Output should generally look like this:

```
**********************************************************************
** Visual Studio 2019 Developer Command Prompt v16.8.3
** Copyright (c) 2020 Microsoft Corporation
**********************************************************************
[vcvarsall.bat] Environment initialized for: 'x64'
Could Not Find C:\BuildTools\donut\mmap-windows.obj
Could Not Find C:\BuildTools\donut\lib\libdonut.lib

                  .                         ,--.
,-. . . ,-. . , , |-. o ,-. ,-. ,-. ,-. ,-.    /
`-. | | `-. |/|/  | | | `-. | | |-' |   `-. ,-'
`-' `-| `-' ' '   ' ' ' `-' |-' `-' '   `-' `---
     /|                     |  @Jackson_T
    `-'                     '  @modexpblog, 2021

SysWhispers2: Why call the kernel when you can whisper?

Complete! Files written to:
        C:\Flashbang\src\FlashbangDll\Syscalls.h
        C:\Flashbang\src\FlashbangDll\Syscalls.c
        C:\Flashbang\src\FlashbangDll\SyscallsStubs.asm

Cleaning solution...          [… snip …]
Building FlashbangDll...      [… snip …]
Building FlashbangDriver...   [… snip …]
Building FlashbangHarness...  [… snip …]
Building Cobalt Strike BOF... [… snip …]
Packaging FlashbangDll as compressed shellcode...

  [ Donut shellcode generator v0.9.3
  [ Copyright (c) 2019 TheWover, Odzhan

  [ Instance type : Embedded
  [ Module file   : "C:\Flashbang\src\x64\Release\FlashbangDll.dll"
  [ Entropy       : Random names + Encryption
  [ Compressed    : aPLib (Reduced by 50%)
  [ File type     : DLL
  [ Function      : Initialize
  [ Target CPU    : amd64
  [ AMSI/WDLP     : none
  [ Shellcode     : "C:\Flashbang\dist\CobaltStrike\flashbang_core.bin"

Build complete! Duration: 00:00:37.1288479.
```

Two directories should now be present in `dist\` containing the build artifacts:

```
C:\Flashbang\dist>dir

12/27/2020  06:13 AM    <DIR>          .
12/27/2020  06:13 AM    <DIR>          ..
12/27/2020  06:13 AM    <DIR>          CobaltStrike
12/27/2020  06:13 AM    <DIR>          Standalone
```