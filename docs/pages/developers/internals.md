# Flashbang Internals

This document describes Flashbang's internal workings and related design decisions. Reading this is not required to use it or to create blindlets, but can provide more context for developers interested in integration.

## Module Loading

The Flashbang module is loaded into the same process as the implant through shellcode execution. Certain Windows API functions are used to facilitate communication between the implant and the module. This means that the C2 platform must expose a capability to execute arbitrary code on targets. A couple examples include [Beacon Object Files](https://cobaltstrike.com/help-beacon-object-files) in Cobalt Strike, and custom [Tasks](https://github.com/cobbr/Covenant/wiki/Building-Tasks) in Covenant.

## Implant ⥄ Module Communications

Upon loading, the implant binding and the module [register](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-registerclassexw) handlers for [WM_COPYDATA](https://docs.microsoft.com/en-us/windows/win32/dataxchg/using-data-copy) messages. This allows both components to send serialized messages to each other using the [SendMessage](#) function. These handlers serve as separate send and receive (Tx/Rx) channels.

Each handler is identified by a "class name" string, which by default is a pseudorandom GUID seeded by the handler's type, PID, and a salt. The first two factors ensure that the class names are unique whether the implant and Flashbang are in the same process or in separate processes. The salt factor serves as a shared secret between both components to ensure that other processes cannot easily determine the correct GUID values.

As shown in the diagram below, the implant binding first initializes its message handler, then loads Flashbang shellcode into local or remote process memory, and then Flashbang initializes its handler. From there, both components will be able to identify their counterpart handlers and send messages.

![](http://www.plantuml.com/plantuml/png/hPHlRzCm4CRVvrFSEpO1WzTedTPb0ofH4PLD5H8IkiRNr9fZXxEzjNnwphAI5gfCsiIR_d4kltlkkIUvz04vnDOK6wq8riw5bTiM_bxR3XbRk7BR2fNIje7tVnr7gv1tLuuLSUcCOxWoMDyVr5H2rhNkq8RZic8oWPdLGQFHlp5ePs4oAOg6NUnWGOtsWHYMyv_br-hxzVHs2f_HAaCC9mkgIJ-8M7OjV3JeDokqpMa1C6ix8ztWxKN_ymDy8r8mWJ8oarogPjVmUc3TxZg2a_FLnO1PaE-SzNJ-RdLnUZR2lFC4Fd63WzgrNGm4dQUe70jOL7swapPhnr0BruF5biPO9Hep35ocyfij20LA9Woqr09yb2DfnRqjcop_h22hUi--E5HxDs3kL3GqqdDgZAi53YsrZdUWBQG30uw1vIYDmwucxyTKBra7EcZdDsHCxHJ1AgxNn4bkXZ_b8MsfZcdtqawtEgIjfZcZtGFpaCyEHjz8Oh6NAGBsVruGZL-HVCZ96CM-B1tZlF-Na3bvZmt1pRQMomsD1J9BtYleOGTouUHeeCE7vSXd4SlPlQJoc1ozjarlblh75vlooBhPYBX5faYf3QYj1oziubSUqlRGmxAQBgPpU40JwGtyiCcdWDnG69g5j5dqCcYtkyQ0P-enVULxdbf4bPy9QBEHuwM3YRqXpx3maDZVFbYOtqqGt15urC4SWKlvYAtv0m00)

## States and Message Types

Flashbang operates as a state machine which means that it can only be in one of a finite number of states at any given time. This helps avoid buggy scenarios that can arise from requests that are sent out-of-order.

It can receive messages to move from one state to another. The types of messages are listed below:

- **GET_HEARTBEAT**: Sends a heartbeat request to verify the Flashbang module has loaded.
- **LOAD_CAPABILITY**: Loads general settings and buffers for drivers if required.
- **LOAD_BLINDLET**: Loads the serialized blindlet into memory.
- **APPLY_BLINDLET**: Executes the sequence of actions specified in the blindlet.
- **REVERT_BLINDLET**: Reverts the actions performed by the blindlet.
- **UNLOAD_BLINDLET**: Removes the blindlet from memory. Required to load another blindlet, or to unload the module.
- **UNLOAD_CAPABILITY**: Terminates Flashbang threads and removes the module from memory.

The diagram below depicts legal transitions between states:

<p align="center">
  <img src="pages/developers/internals-states.png" />
</p>

## Message Serialization

[FlatBuffers](https://google.github.io/flatbuffers/index.html#flatbuffers_overview) are used for message serialization. This library was chosen for several reasons:

- As a binary format, it adds a layer of obfuscation to increase difficulty for defenders (cf. JSON).
- Minimal network traffic compared to human-readable formats.
- Smaller library than other tested serialization formats, keeping the shellcode lighter.
- Supports languages that common implants are written in (C/C++, Go, C#, Python, etc.).

## Driver Loading

If any kernel-mode actions are specified in a blindlet, Flashbang will load its driver to facilitate execution. It's important to load the driver as stealthily as possible to avoid getting caught. Telemetry visibility and reporting verbosity will vary from product to product, so there isn't a one-size-fits-all way to load the driver.

Flashbang offers a number of configuration options to alter the reporting of events related to driver loading. The diagram below depicts the driver loading flow based on the options in the `driver_settings` section of a blindlet.

![](http://www.plantuml.com/plantuml/svg/xLHDRnCn4BtxLpo9ItAWFo2eK6ZAgHLI8i7PZTzchNZjnPuDh13_7NkxIhCAXP0pnvLdthoFsRb9egDqZLDfPtshesvm6xonOeE_ysOjlIDgPpsL6cgLhJ1R1YRahoq5YN5lDuHsaThqu7URvDqSKjDZrZFD5LqYPZxC5V3gtcvhMMlN1B-upIt20uJrM76RssEFRrPgf9ORMraQUDtmweq2V10Y5ep5KN6qGd1FBrUAcNwb1fxtGHlhjtYWrC7W-XeVhUEKN1OjWzpX3JyKiBZp-j4HbTDRI40JCcAFJStDBdKDPXaxycjd8yqyiphjw1BX-3JS12dAMYtrQUVKZsKCBKoqUyQ2OcpQdOWkBAU2CGLPKn17PuQfP_hsKmr1_TwtzSE7bspBJvUwzgcbXmiRxP1g7NbIeu32PtKivtahfem0uB1HVUEits4M9vsltuvpojSJXx-P1ma7j8EPvzW97DR-p36HJH3-yvIoxdyExmiVb-y_lv3V-7ffX6l9G6E68rBvdHalp_9_bkC9cumfiOwt8ICUepq4qpd2bO8HQh6aet2iHsKTSzDYbUDW4XWEzxOSLLGnDC-uAaberGszwHht2m00)

## Module ⥄ Driver Communications

The module primarily communicates with the driver using exposed IOCTL functions. The [DeviceIoControl](https://docs.microsoft.com/en-us/windows/win32/api/ioapiset/nf-ioapiset-deviceiocontrol) function is used to call IOCTL functions and to do this, it requires a handle associated with the driver's device. Creating this handle requires the device to have a name.

Using a _constant string_ for the device name (e.g. `\Device\FlashbangDriver`) introduces a tool-specific artifact which can pose an OPSEC risk. Using a _dynamic string_ appears to pose a chicken vs. egg problem because the user-mode module cannot receive the string from an IOCTL function if it needs it to make the DeviceIoControl call.

This problem is resolved by registering a temporary callback in the driver via [IoRegisterContainerNotification](https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-ioregistercontainernotification). An undocumented Native API function ([NtNotifyChangeSession](https://processhacker.sourceforge.io/doc/ntioapi_8h.html#a23719c37bc09be8bb0b4e3d296ce3fd6)) is called from the user-mode module to trigger the callback and receive the symbolic link string. After the first and only call, the callback is unregistered to reduce potential artifacts/exposure. With the symbolic link string now available to the user-mode module, it can proceed to create a handle to the device, and make DeviceIoControl calls for blindlet-specific actions.

Four IOCTL functions are currently available for the user-mode module to call:

- `IOCTL_GET_MODULES`: Returns a list of loaded kernel-mode modules.
- `IOCTL_GET_CALLBACKS`: Returns a list of kernel-mode callback functions.
- `IOCTL_GET_QWORD`: Read QWORD primitive (to get original callback bytes).
- `IOCTL_SET_QWORD`: Write QWORD primitive (to patch callbacks).

## Creating a New Action

TBD