<style type="text/css">
  .markdown-section h4, .markdown-section h5 {
    margin-bottom: -0.5em;
  }
</style>

# Blindlet Reference Documentation

## YAML Structure

?> _TBD_

## Metadata

### General Attributes

?> _TBD_

### Compatibility Attributes

?> _TBD_

### OPSEC Attributes

?> _TBD_

## Payload Actions

Actions are prefixed with `Um` (user-mode) and `Km` (kernel-mode) which indicate where they will occur.

| Type | Action | Description | Status |
|:-----|:-------|:------------|:-------|
| Enumeration | [KmEnumerateCallbacks](#KmEnumerateCallbacks) | Enumerate kernel-mode callbacks. | :soon: |
| Enumeration | [UmEnumerateHooks](#UmEnumerateHooks) | Enumerate user-mode function hooks for only the host process. | :soon: |
| Enumeration | [UmEnumerateEtw](#UmEnumerateEtw) | Enumerate ETW sessions and providers. | :soon: |
| Enumeration | [UmEnumerateFilters](#UmEnumerateFilters) | Enumerate filter drivers via Filter Manager. | :soon: |
| Telemetry Suppression | [KmSuppressCallbacks](#KmSuppressCallbacks) | Suppress kernel-mode callbacks. | :heavy_check_mark: |
| Telemetry Suppression | [UmSuppressHooks](#UmSuppressHooks) | Suppress user-mode function hooks for only the host process. | :heavy_check_mark: |
| Telemetry Suppression | [UmSuppressEtw](#UmSuppressEtw) | Suppress ETW providers within a given session. | :heavy_check_mark: |
| Telemetry Suppression | [UmSuppressFilter](#UmSuppressFilter) | Suppress a filter driver by unloading it via Filter Manager. | :heavy_check_mark: |
| Code Execution | [UmRunCommand](#UmRunCommand) | Run a command. | :heavy_check_mark: |
| Code Execution | [UmRunLua](#UmRunLua) | Run Lua code. | :heavy_check_mark: |
| Code Execution | [UmRunShellcode](#UmRunShellcode) | Run shellcode from user-mode. | :soon: |
| Code Execution | [KmRunShellcode](#KmRunShellcode) | Run shellcode from kernel-mode. | :soon: |
| Memory Patching | [UmPatchMemory](#UmPatchMemory) | Patch memory from user-mode. | :soon: |
| Memory Patching | [KmPatchMemory](#KmPatchMemory) | Patch memory from kernel-mode. | :heavy_check_mark: |
| Utility | [UmCheckFileVersion](#UmCheckFileVersion) | Check file version against allowlist. | :heavy_check_mark: |
| Utility | [UmBlockConnection](#UmBlockConnection) | Block specific network connections. | :heavy_check_mark: |
| Utility | [UmSetSystemTime](#UmSetSystemTime) | Set system time. | :soon: |

### Enumeration

#### KmEnumerateCallbacks

?> _TBD_

#### UmEnumerateHooks

?> _TBD_

#### UmEnumerateEtw

?> _TBD_

#### UmEnumerateFilters

?> _TBD_

### Telemetry Suppression

#### KmSuppressCallbacks

Suppress kernel-mode callbacks.

##### Parameters

- **module_name** (`str`): The name of the loaded module for which callbacks will be suppressed.
- **types** (`str[]`): One or more of: `IMAGE`, `PROCESS`, `THREAD`, `REGISTRY`, `OBJECT`, `FILE`.

##### Example

```yaml
payload:
  actions:
    - type: KmSuppressCallbacks
      note: Suppress EDRAgent driver callbacks.
      conf:
        module_name: edragent.sys
        types: [IMAGE, PROCESS, THREAD, REGISTRY, OBJECT, FILE]
```

##### Remarks

Flashbang will load its driver to facilitate this action. Please refer to [Driver Settings](#driver-settings) so that the driver is loaded in an OPSEC-safe manner.

#### UmSuppressHooks

Suppress user-mode function hooks for only the host process.

##### Parameters

- **functions** (`str[]`): A list of function names in `ModuleName.dll!FunctionName` format.
  - An empty list will suppress all hooked functions.

##### Example

```yaml
payload:
  actions:
    - type: UmSuppressHooks
      note: Suppress hooks on user-mode APIs (this process only).
      conf:
        functions:
		  - ntdll.dll!NtCreateUserProcess
		  - ntdll.dll!NtWriteVirtualMemory
```

#### UmSuppressEtw

Suppress ETW providers within a given session. This is programmatically equivalent to running `logman update trace SESSION_NAME --p PROVIDER_NAME -ets`, without the process creation footprint.

##### Parameters

- **session** (`str`): The name of the session whose providers shall be suppressed.
- **providers** (`str[]`): A list of providers to suppress for the given session name.

##### Example

```yaml
payload:
  actions:
    - type: UmSuppressEtw
      note: Disable provider in Avast-IDP trace session.
      conf:
        session: Avast-IDP
        providers:
          - Microsoft-Windows-WMI-Activity
```

##### Remarks

!> Some ETW sessions can only be manipulated if Flashbang's host process is running as SYSTEM. This action cannot be used to disable the `Microsoft-Windows-Threat-Intelligence` provider.

#### UmSuppressFilter

Suppress a filter driver by unloading it via Filter Manager.

##### Parameters

- **filter_name** (`str`): The name of the filter.
- **filter_altitude** (`int`): The altitude of the filter (overrides **filter_name** if defined).

##### Example

```yaml
payload:
  actions:
    - type: UmSuppressFilter
      note: Disable SysmonDrv filter.
      conf:
        filter_name: SysmonDrv
        filter_altitude: 385201
```

##### Remarks

!> Unloading a filter driver may cause undefined behaviour in the programs that interact with it and **it may not be possible to revert this action back to an operational state**. Success can vary heavily between product implementations. Some requests may return an unsuccessful status code (e.g. `STATUS_FLT_DO_NOT_DETACH`), whereas others may appear to be successful but cannot be effectively reverted back to the original state. **Always test in a lab environment before executing on live targets.**

### Code Execution

#### UmRunCommand

Run a command.

##### Parameters

- **command** (`str`): The command line to execute.

##### Example

```yaml
payload:
  actions:
    - type: UmRunCommand
      note: Open a file in Notepad.
      conf:
        command: 'notepad C:\Windows\System32\WindowsCodecsRaw.txt'
```

#### UmRunLua

Run Lua code.

##### Parameters

- **code** (`str`): The Lua code to execute.

##### Example

```yaml
payload:
  actions:
    - type: UmRunLua
      note: Call MessageBoxA from the Windows API.
      conf:
        code: |
          ffi = require('ffi')
          user32 = ffi.load('user32.dll')
		  ffi.cdef[[ int MessageBoxA(void* w, const char* txt, const char* cap, int type); ]]
		  user32.MessageBoxA(nil, "Hello, world!", "Flashbang", 0);
```

#### UmRunShellcode

Run shellcode from user-mode.

?> _TBD_

#### KmRunShellcode

Run shellcode from kernel-mode.

?> _TBD_

### Memory Patching

#### UmPatchMemory

Patch memory from user-mode.

?> _TBD_

#### KmPatchMemory

Patch memory from kernel-mode.

##### Parameters

- **module_name** (`str`): The name of the loaded module to be patched.
- **search_pattern** (`byte[]`): Optional array of bytes to seek in module.
- **offset** (`int`): Offset in bytes from the start of the module.
  - If search_pattern is provided, then this is offset from the start of the first match.
- **old_bytes** (`byte[]`): Optional array of old bytes to compare against at the offset.
  - If set, patching will continue only if the targeted bytes match with those specified in this array.
- **new_bytes** (`byte[]`): Array of bytes the old bytes will be replaced with.

##### Example

```yaml
payload:
  actions:
    - type: KmPatchMemory
      note: Patch token check in EDRAgent driver.
      conf:
        module_name: edragent.sys
        search_pattern: [0x46, 0x37, 0x79, 0x22, 0x75, 0x3A]
        offset: 4
        old_bytes: [0x75, 0x3A] # JNZ
        new_bytes: [0x66, 0x90] # NOP
```

##### Remarks

Flashbang will load its driver to facilitate this action. Please refer to [Driver Settings](#driver-settings) so that the driver is loaded in an OPSEC-safe manner.

### Utility

#### UmCheckFileVersion

Check file version against allowlist.

##### Parameters

- **file_path** (`str`): The path of the file.
- **supported_versions** (`str[]`): A sequence of supported version strings.

##### Example

```yaml
payload:
  actions:
    - type: UmCheckFileVersion
      name: Validate version of EDRAgent.sys driver.
      conf:
        file_path: '%SystemRoot%\System32\drivers\edragent.sys'
        supported_versions: [1.23.44404.0, 1.23.44606.0]
```

##### Remarks

This action can serve as an execution guardrail to ensure that the blindlet only executes on systems with tested product versions.

#### UmBlockConnection

?> _TBD_

#### UmSetSystemTime

?> _TBD_

## Payload Settings

### General Settings

?> _TBD_

### Driver Settings

#### loading_method (`str`)

Sets the method for loading the driver.

- "FILE" → Drops the driver file to a temporary location, or path specified in `destination_path`.
- "SMB" → Drops the driver file to a temporary location and creates a share for it.

##### Guidance

Use SMB to delete the driver file immediately after loading. This may prevent some (but not all) EDRs from obtaining signature or hash metadata. Using SMB mode may also cause some EDRs from reporting driver loads because of a `STATUS_OBJECT_PATH_SYNTAX_BAD` error (YMMV). A remote `destination_path` can also be provided with the SMB option, to avoid having to drop the driver file to disk.

#### service_name (`str`)

Sets the service name of the driver.

- If the service does not exist, a new one will be created and deleted after loading.
- If the service already exists and is not running, the driver will masquerade as it by replacing the ImagePath value.
  - The ImagePath value will be reverted back right after the driver has loaded.
- If the service already exists and is running, then an error will be raised.

##### Guidance

Use the name of an existing driver service (e.g. Modem) if the EDR reports service creations but not updates. Do not use the name of an already running service, and do not use the name of a service that is not a driver.

#### destination_path (`str`)

Sets the absolute path of the driver file. If a local path is specified, the driver will be dropped to disk here.

##### Guidance

TBD

##### Examples

```yaml
destination_path: '' # Defaults to dropping the driver to %TEMP%.
destination_path: 'C:\Windows\win32modem.sys' # Drops the driver to C:\Windows\win32modem.sys.
destination_path: '%SystemRoot%\win32modem.sys' # Expands environment variables.
destination_path: '%SystemRoot%\New\Folders\win32modem.sys' # Creates new folders if they do not exist.
```

#### registry_image_path (`str`)

Sets the ImagePath value of the service in the Registry.

- If the "FILE" loading method is used, then the file will be created at the specified path.
- If the "SMB" or "WEBDAV" loading methods are used, a symlink will be created.

#### use_random_hash (`bool`)

Modifies the PE checksum so the computed hash will be different each time (Windows 10 ONLY).

- Prevents reliable hash-based signaturing of the driver file.
- More details via [@FuzzySec](https://twitter.com/FuzzySec/status/1298655331451637760)

#### use_leading_dot (`bool`)

Uses a leading dot (U+2024) for the filename extension separator instead of a full stop (U+002E).

- This may evade detection on some file filters looking for '.sys' suffixes.
- However, not using a full stop can be suspicious too.

<!--

#### use_hyperv_drop (`bool`)

Will create the driver file or symlink as the System process (PID=4) if Hyper-V is enabled.

-->

<!-- for opsec, try abusing blind spot, or blending in, then tampering. -->