# OPSEC Considerations

Flashbang's purpose is to covertly blind AV/EDR products, so it's _critically important_ that its presence does not get detected&mdash;especially during its bootstrapping process before an operator has the chance to enable a blindlet. The sections below go over the choices we have made to reduce Flashbang's footprint as well as options you can consider to modify potential artifacts.

## Language Choice

Flashbang is written in an unmanaged language (C++) which immediately reduces some of the introspection capabilities that security products can use&mdash;compared to using managed languages that are compiled or interpreted (such as C# or PowerShell):

0. **Bypassing AMSI is not an immediate concern.** AMSI requires cooperation from both security vendors _and_ application developers. Flashbang and supporting runtime libraries do not call the relevant scanning functions from the AMSI API, so developer cooperation does not occur.

0. **ETW logging of .NET assemblies is not applicable.** Explain what it is and why it's not relevant because we're not using .NET. Pellentesque arcu ante, sagittis at iaculis eu, tempus sit amet odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce vitae ipsum ut odio dictum molestie id et nisi.

0. **Anomalous CLR loading patterns do not occur.** Suspendisse faucibus, purus dapibus facilisis egestas, nisl sem ullamcorper nibh, consequat elementum odio dolor quis lorem. In accumsan consequat neque id accumsan. Sed ac placerat mauris. Sed condimentum odio sit amet ante varius, eu semper justo semper.

0. **Script-block logging does not occur.** Sed commodo dolor sapien, eu tempor erat rhoncus et. Nulla dictum enim in auctor ultrices. Donec quis eleifend nisi. Pellentesque ut nulla nec risus tristique gravida in at massa. Vivamus pharetra ligula ipsum, pellentesque sodales sem pellentesque nec.

While C++ doesn't have an easy learning curve and isn't the fastest language for prototyping, there are some additional perks of using the language: There are already bindings available to the Windows API and there's greater flexibility in obfuscation techniques which makes it more resilient to reverse engineering. Knowing how to code in C++ is not required to use Flashbang as an operator, or even to develop blindlets (as demonstrated in the [tutorial](developers/tutorial)).

## Module Loading

?> ToDo: use of Donut, plus validation against Hollows Hunter (TBD).

## Driver Loading

?> ToDo: overview of driver loading options.

## Implant ⥄ Module Communications

?> ToDo: WM_COPYDATA/SendMessage implementation + use of FlatBuffers for serialization.

## Module ⥄ Driver Communications

?> ToDo: Uses IOCTL function with a random (configurable) device name on driver load. Device name is retrieved using an undocumented Native API function.
