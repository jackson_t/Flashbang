![logo](https://static.thenounproject.com/png/3918-200.png)

# Flashbang

> A modular capability to evade AV/EDR.

[Repository](https://github.com/jthuraisamy/Flashbang)
[Get Started](#introduction)