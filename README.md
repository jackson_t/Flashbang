# Flashbang

## Build Instructions

### Code

1. Install [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/).
2. Switch to Windows containers (via tray icon context menu).
3. Enter the lines below in a Command Prompt window:

```batch
git clone "https://gitlab.com/jackson_t/Flashbang.git"
cd Flashbang\
docker build -t "flashbang-buildtools:latest" -m 2GB build\
:: Building the Docker image takes 30-45 min; consider taking a coffee break.
docker run --rm -v "%CD%:C:\Flashbang" flashbang-buildtools "C:\Flashbang\build\build.ps1"
```

### Documentation

Documentation can be served with a simple HTTP server, for example:

1. Set current working directory to `docs/`.
2. Run `python3 -m http.server`.
3. Navigate to [http://localhost:8000](http://localhost:8000).

Live rendering is possible by using the [docsify](https://docsify.js.org/) command:

1. Install docsify via `npm i docsify-cli -g`.
2. Set current working directory to the root of the repository.
3. Run `docsify serve docs`.
4. Navigate to [http://localhost:3000](http://localhost:3000).
