# Start timer.
$StopWatch = New-Object -TypeName System.Diagnostics.Stopwatch 
$StopWatch.Start()

# Build donut build-dependency.
Set-Location C:\BuildTools\donut\
nmake /NOLOGO /F .\Makefile.msvc | Out-Null

# Create dist/ directory.
New-Item -Path C:\Flashbang\dist -ItemType Directory -Force | Out-Null

# Compile FlatBuffers schema for C++.
Write-Output "Building FlatBuffers schema files..."
Set-Location -Path C:\Flashbang\src\Schemas
C:\BuildTools\vcpkg\buildtrees\flatbuffers\x64-windows-static-rel\flatc.exe --cpp --gen-mutable .\flashbang.fbs
Move-Item -Path .\flashbang_generated.h -Destination C:\Flashbang\src\FlashbangDll\Serialization.h -Force

# Compile FlatBuffers schema for Python.
Remove-Item -Path C:\Flashbang\src\MessageSerialization\Python\Flashbang -Recurse -Force
C:\BuildTools\vcpkg\buildtrees\flatbuffers\x64-windows-static-rel\flatc.exe --python --gen-mutable .\flashbang.fbs
Move-Item -Path .\Flashbang -Destination C:\Flashbang\src\MessageSerialization\Python\. -Force

# Compile FlatBuffers schema for Java.
Remove-Item -Path C:\Flashbang\src\MessageSerialization\Java\src\Flashbang\Serialization -Recurse -Force
C:\BuildTools\vcpkg\buildtrees\flatbuffers\x64-windows-static-rel\flatc.exe --java --gen-mutable .\flashbang.fbs
Move-Item -Path .\Flashbang\Serialization -Destination C:\Flashbang\src\MessageSerialization\Java\src\Flashbang\. -Force
Remove-Item -Path .\Flashbang -Recurse -Force
# Flatbuffers has a bug in Java compilation where long values are missing the 'L' suffix.
(Get-Content -Path C:\Flashbang\src\MessageSerialization\Java\src\Flashbang\Serialization\Status.java -Raw) -Replace ';', 'L;' | Set-Content -Path C:\Flashbang\src\MessageSerialization\Java\src\Flashbang\Serialization\Status.java
(Get-Content -Path C:\Flashbang\src\MessageSerialization\Java\src\Flashbang\Serialization\Status.java -Raw) -Replace 'Flashbang.SerializationL', 'Flashbang.Serialization' | Set-Content -Path C:\Flashbang\src\MessageSerialization\Java\src\Flashbang\Serialization\Status.java

# Compile SysWhispers files.
Remove-Item -Path C:\Flashbang\src\FlashbangDll\Syscalls.h -Force
Remove-Item -Path C:\Flashbang\src\FlashbangDll\Syscalls.cpp -Force
Remove-Item -Path C:\Flashbang\src\FlashbangDll\SyscallsStubs.asm -Force
Set-Location -Path C:\BuildTools\SysWhispers2
python .\syswhispers.py --functions NtOpenProcessToken,NtAdjustPrivilegesToken,NtDeviceIoControlFile,NtOpenSession,NtNotifyChangeSession --out-file C:\Flashbang\src\FlashbangDll\Syscalls
Rename-Item -Path C:\Flashbang\src\FlashbangDll\Syscalls.c -NewName C:\Flashbang\src\FlashbangDll\Syscalls.cpp

# Make Flashbang-specific changes to SysWhispers output.
(Get-Content -Path C:\Flashbang\src\FlashbangDll\Syscalls.cpp -Raw) -Replace '#include "Syscalls.h"', "#include ""pch.h""`r`n#include ""Syscalls.h""" | Set-Content -Path C:\Flashbang\src\FlashbangDll\Syscalls.cpp
(Get-Content -Path C:\Flashbang\src\FlashbangDll\Syscalls.h -Raw) -Replace '// Typedefs are prefixed to avoid pollution.', "typedef ULONG NTSTATUS;`r`n`r`n// Typedefs are prefixed to avoid pollution." | Set-Content -Path C:\Flashbang\src\FlashbangDll\Syscalls.h

# Build core module, driver, and harness.
Set-Location -Path C:\Flashbang\src
Write-Output "`nCleaning solution..."
MSBuild Flashbang.sln /nologo /verbosity:quiet /property:Configuration=Release /target:Clean
Write-Output "Building FlashbangDll..."
MSBuild Flashbang.sln /nologo /verbosity:quiet /property:Configuration=Release /target:FlashbangDll
Write-Output "Building FlashbangDriver..."
MSBuild Flashbang.sln /nologo /verbosity:quiet /property:Configuration=Debug /target:FlashbangDriver
Write-Output "Building FlashbangHarness..."
MSBuild Flashbang.sln /nologo /verbosity:quiet /property:Configuration=Release /target:FlashbangHarness

# Sign the driver with an expired cert if the system's date is set to before March 31, 2013.
if ( $(Get-Date).ToFileTimeUtc() -lt 130091616000000000 )
{
	# Disable SSL/TLS verification.
	Add-Type "
		using System.Net;
		using System.Security.Cryptography.X509Certificates;
		public class TrustAllCertsPolicy : ICertificatePolicy {
			public bool CheckValidationResult(ServicePoint srvPoint, X509Certificate certificate, WebRequest request, int certificateProblem) {
				return true;
			}
		}"
	[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
	
	Invoke-WebRequest -Uri "https://web.archive.org/web/20201205052447if_/https://duo.com/assets/files/DellCertificates.zip" -OutFile C:\TEMP\DellCertificates.zip
	Invoke-WebRequest -Uri "https://web.archive.org/web/20151201040017if_/http://www.myssl.cn/download/MSCV-VSClass3.cer" -OutFile C:\TEMP\MSCV-VSClass3.cer
	Expand-Archive -Path C:\TEMP\DellCertificates.zip -DestinationPath C:\TEMP\
	$Password = $(Get-Content C:\TEMP\Verisign.pass).Trim()
	signtool sign /f C:\TEMP\Verisign.pfx /ac C:\TEMP\MSCV-VSClass3.cer /p $Password C:\Flashbang\src\x64\Release\FlashbangDriver.sys
}

# Create directories within dist/.
New-Item -Path C:\Flashbang\dist\CobaltStrike -ItemType Directory -Force | Out-Null
New-Item -Path C:\Flashbang\dist\Standalone -ItemType Directory -Force | Out-Null

# Build serialization libraries.
Write-Output "Building Python-based serializer..."
Set-Location -Path C:\Flashbang\src\MessageSerialization\Python
Copy-Item -Path requirements.txt -Destination C:\Flashbang\dist\Standalone\requirements.txt -Force
Copy-Item -Path message_serialization.py -Destination C:\Flashbang\dist\Standalone\message_serialization.py -Force
7z a -tZip -mx9 C:\Flashbang\dist\Standalone\message_serialization_flatbuffers.zip .\Flashbang\ | Out-Null
Write-Output "Building Java-based serializer..."
New-Item -Path C:\Flashbang\src\MessageSerialization\Java\out -ItemType Directory -Force | Out-Null
Set-Location -Path C:\Flashbang\src\MessageSerialization\Java\lib
javac -d ..\out -cp $($(Get-ChildItem *.jar) -join ';') ..\src\Flashbang\Serialization\*.java ..\src\Flashbang\*.java
Set-Location -Path C:\Flashbang\src\MessageSerialization\Java\out
foreach ($lib in $(Get-ChildItem C:\Flashbang\src\MessageSerialization\Java\lib | % { $_.FullName })) { jar xf $lib; }
7z a -tZip -mx9 C:\Flashbang\dist\CobaltStrike\flashbang_serializer.jar . | Out-Null
Set-Location -Path C:\Flashbang\src\MessageSerialization\Java
Remove-Item -Path C:\Flashbang\src\MessageSerialization\Java\out -Recurse -Force

# Build Cobalt Strike integration.
Copy-Item -Path C:\Flashbang\src\x64\Debug\FlashbangDriver.sys -Destination C:\Flashbang\dist\CobaltStrike\flashbang_driver.sys -Force
Copy-Item -Path C:\Flashbang\src\CobaltStrike\flashbang.cna -Destination C:\Flashbang\dist\CobaltStrike\flashbang.cna -Force
Copy-Item -Path C:\Flashbang\src\Schemas\flashbang_schema.json -Destination C:\Flashbang\dist\CobaltStrike\flashbang_schema.json -Force
Set-Location -Path C:\Flashbang\src\CobaltStrike
Write-Output "Building Cobalt Strike BOF..."
cl.exe /nologo /c /GS- flashbang_bof.c /FoC:\Flashbang\dist\CobaltStrike\flashbang_bof.o | Out-Null
Write-Output "Packaging FlashbangDll as compressed shellcode..."
C:\BuildTools\donut\donut.exe -a 2 -b 1 -z 2 -m Initialize -o C:\Flashbang\dist\CobaltStrike\flashbang_core.bin C:\Flashbang\src\x64\Release\FlashbangDll.dll

# Move harness binaries to dist/Standalone.
Copy-Item -Path C:\Flashbang\src\x64\Release\*.pdb -Destination C:\Flashbang\dist\Standalone\ -Force
Copy-Item -Path C:\Flashbang\src\x64\Release\FlashbangHarness.exe -Destination C:\Flashbang\dist\Standalone\FlashbangHarness.exe -Force
Copy-Item -Path C:\Flashbang\src\x64\Release\FlashbangDll.dll -Destination C:\Flashbang\dist\Standalone\FlashbangDll.dll -Force
Copy-Item -Path C:\Flashbang\src\x64\Debug\FlashbangDriver.sys -Destination C:\Flashbang\dist\Standalone\FlashbangDriver.sys -Force
Copy-Item -Path C:\Flashbang\src\Schemas\flashbang_schema.json -Destination C:\Flashbang\dist\Standalone\flashbang_schema.json -Force
Copy-Item -Path C:\Flashbang\dist\CobaltStrike\flashbang_core.bin -Destination C:\Flashbang\dist\Standalone\flashbang_core.bin -Force

# Reset path.
Set-Location -Path C:\Flashbang

# Print summary.
$StopWatch.Stop()
$ElapsedTime = $StopWatch.Elapsed.ToString()
Write-Output "`nBuild complete! Duration: $ElapsedTime."